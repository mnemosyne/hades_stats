"""
For glm fiitings
"""


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
from numbers import Number
from collections.abc import Iterable
import time

import numpy
import pandas
import scipy.optimize

# ~ from hades_stats.math_functions import LinearExpr #Expr, InterceptExpr
# ~ from hades_stats.math_exprs import LinearCombination, MathExpr
from hades_stats.covar_sample import CovarSample, OBSNAME
from hades_stats.covar_distr import CovarDistribution, VarDistribution
# ~ from hades_stats.r2py import robj_to_py, obj_in_r

# ~ try:
    # ~ from rpy2 import robjects
    # ~ from rpy2.robjects.packages import importr
    # ~ from rpy2.robjects import pandas2ri
    
    # ~ importr('ismev')
# ~ except:
    # ~ print("CANNOT IMPORT ISMEVs")
    
# ~ from japet_misc import todo_msg
# ~ print(todo_msg("_", __file__))

##======================================================================================================================##
##                EXTREME FT WITH COVARAITES                                                                            ##
##======================================================================================================================##

class FitEVT_glm:
    """ Class doc
    >>> a = pandas.DataFrame(numpy.arange(10).reshape(5,2), index = numpy.arange(15,20), columns = ['a', 'b'])
    >>> s = pandas.DataFrame([[1,2], [3,4]], index = ["cov1", "cov2"], columns = ['a', 'b'])
    >>> t = pandas.DataFrame(numpy.arange(100,110).reshape(5,2), index = numpy.arange(15,20), columns = ["cov3", "cov4"])
    >>> a
        a  b
    15  0  1
    16  2  3
    17  4  5
    18  6  7
    19  8  9
    >>> s
          a  b
    cov1  1  2
    cov2  3  4
    >>> t
        cov3  cov4
    15   100   101
    16   102   103
    17   104   105
    18   106   107
    19   108   109
    
    >>> b = CovarSample(a, spa_covariates = s, time_covariates = t)
    >>> b.merged_dataframe()
        obs  cov1  cov2  cov3  cov4
    15    0     1     3   100   101
    16    2     1     3   102   103
    17    4     1     3   104   105
    18    6     1     3   106   107
    19    8     1     3   108   109
    15    1     2     4   100   101
    16    3     2     4   102   103
    17    5     2     4   104   105
    18    7     2     4   106   107
    19    9     2     4   108   109
    
    >>> f = FitEVT_glm(b, "gev", mul = ['cov1'], sigl = "cov2")
    >>> obj_in_r()
    >>> cd = f.to_covardistribution()
    >>> abs(cd.nllh(b) - f.nllh) < 1e-8
    True
    """
    
    def __init__(self, data, model, **kwarg):
        """ Class initialiser """
        assert isinstance(data, CovarSample)
        
        self.data = data.merged_dataframe()
        self.model = model
        
        for k, v in kwarg.items():
            setattr(self, k, v)
            
        fitres = self._fit(**kwarg)
        
        for k, v in fitres.items():
            if k not in ("data", "model"):
                setattr(self, k, v)
            
        self.pypars = self._return_py_params()
        
    def return_colnumbs(self, parcovs):
        """Return the colnumbs of covnames"""
        if isinstance(parcovs, str):
            parcovs = [parcovs]
        elif isinstance(parcovs, Iterable):
            pass            
        else:
            raise ValueError("Exp str or iterable, got {0}".format(parcovs))
            
        cols = list(self.data.columns)
        for icov in parcovs:
            assert icov in cols, "{0} is not in {1}".format(icov, cols)
            
        res = [cols.index(cn) for cn in parcovs]
            
        return res
        
        
    def _fit(self, **kwarg):
        """Fit within R"""
        xobs = self.data.iloc[:, 0]

        pandas2ri.activate()
        robjects.r.assign('x_obs', xobs)
        robjects.r.assign('ydat', self.data)
        pandas2ri.deactivate()
        
        rtext = 'rfit=ismev::{0}.fit(c(x_obs), ydat = as.matrix(ydat)'.format(self.model)
        rtext = [rtext]
        
        if self.model in ['gum', 'gev']:
            pass            
        elif self.model in ('pp', 'gpd'):
            threshold = xobs.min()
            rtext.append('threshold = {0}'.format(threshold))
        else:
            assert 0, "Not Implemented"
        
        for ikwrg in ("mul", "sigl", 'shl'):
            if ikwrg in kwarg:
                covs = kwarg.pop(ikwrg)
                nbs = self.return_colnumbs(covs)
                nbs = "c({0})".format(",".join(["{0}".format(i+1) for i in nbs]))
                txt = "{0} = {1}".format(ikwrg, nbs)
                rtext.append(txt)
        
        for ikw, kval in kwarg.items():
            if isinstance(kval, Number):
                txt = "{0} = {1}".format(ikw, kval)
            else:
                txt = "{0} = '{1}'".format(ikw, kval)
            rtext.append(txt)
            
        rtext = ",".join(rtext)
        rtext += ")"
        #~ print(rtext)
        
        # ~ print(0)
        robjects.r(rtext)
        # ~ print(1)
        
        res = robj_to_py('rfit', dont_get_attr=('data', 'vals', 'cov'))
        #~ print(1)
        robjects.r("rm(rfit, x_obs, ydat)")
        
        return res
        
    def _return_py_params(self):
        """
        Return py params
        """
        res = {}
        ipar = 0
        for key, ikwrg in zip(["loc", "scale", "c"], ["mul", "sigl", 'shl']):
            res[key] = {}
            pars = ["intercept"]
            if hasattr(self, ikwrg):
                covs = getattr(self, ikwrg)
                if isinstance(covs, str):
                    covs = [covs]
                for icov, cov in enumerate(covs):
                    #~ par = ikwrg.replace("l", str(icov + 1))
                    pars.append(cov)
            
            for par in pars:
                val = self.mle[ipar]
                if self.model == "gev" and key == "c":
                    val = -val
    
                res[key][par] = val
                ipar += 1
                
        assert len(self.mle) == ipar
        #~ print(res)
        return res
        
    def return_lincomb(self):
        """
        Return a linear comb for each param : dict
        """
        res = {key : LinearCombination(dic1) for key, dic1 in self.pypars.items()}
        return res

    def to_covardistribution(self):
        """
        Return the fitted covar ditribution
        """
        dicpar = self.return_lincomb()
        res = CovarDistribution(self.model, **dicpar)
        return res


##======================================================================================================================##
##                HOME-MADE CLASS                                                                                       ##
##======================================================================================================================##


class GLM_Fit:
    """ Class doc
    >>> lexpr = LinearExpr([0.1, 0])
    >>> vnorm = VarDistribution("norm", loc=lexpr, scale=1)
    >>> vgev = VarDistribution("gev", loc=lexpr, scale=1, c=-0.1)
    
    >>> x = pandas.DataFrame({'x':numpy.arange(1000)})
    
    >>> normds = vnorm(x)
    >>> normobs = normds.apply(lambda x: float(x.random(1)))
    >>> normobs = CovarSample(normobs, time_covariates=x)
    >>> normglm = GLM_Fit(normobs, vnorm, method="L-BFGS-B", bounds=[(None, None), (None, None), (1e-8, None)])
    >>> diff = normglm.distribution.params() - vnorm.params()
    >>> diff.abs() < 0.1
    loc    a    True
           b    True
    scale  a    True
    dtype: bool
    
    >>> gevds = vgev(x)
    >>> gevobs = gevds.apply(lambda x: float(x.random(1)))
    >>> gevobs = CovarSample(gevobs, time_covariates=x)
    >>> gevglm = GLM_Fit(gevobs, vgev, method="L-BFGS-B", bounds=[(-0.5, 0.5),(None, None), (None, None), (1e-8, None)])
    >>> diff = gevglm.distribution.params() - vgev.params()
    >>> diff.abs() <0.1
    c      a    True
    loc    a    True
           b    True
    scale  a    True
    dtype: bool
    
    """
    
    def __init__(self, data, vardist, **kwargs):
        """ Class initialiser """
        assert isinstance(vardist, VarDistribution)
        if isinstance(data, CovarSample):
            data = data.merged_dataframe()
        else:
            assert OBSNAME in data
        
        
            
        self.data = data
        self.dist_init = vardist
        
        self.fit_results = self._fit(**kwargs)
        
        if self.fit_results.success:
        
            self.distribution = self.dist_init.new(params=self.fit_results.x)
            
            nllh = self.nllh()
            nllhfun = self._nllh(self.fit_results.x)
            nllhopt = self.fit_results.fun
            
            assert abs(nllh - nllhfun) < 1e-4 and abs(nllh - nllhopt) <= 1e-4, "Pb nllh:{0:.1f}, {1:.1f}, {2:.1f}".format(nllh, nllhfun, nllhopt)
            
        
    def nllh(self):
        """Negative loglikelihood of the fitted GLM"""
        res = self.distribution._nllh(self.data)
        return res

    def _nllh(self, params):
        """Neg log likelihood. Function to optimize"""
        # ~ print(params)
        res = self.dist_init.new(params=params)._nllh(data=self.data)
        # ~ res = self.dist_init._new(params=params).nllh(data=self.data)
        return res
        
    def _fit(self, **kwargs):
        """Fit method"""
        optim_args = dict(method="BFGS")
        optim_args.update(**kwargs)
        
        x0 = self.dist_init.params().values
        # ~ print(x0)
        res = scipy.optimize.minimize(self._nllh, x0=x0, **optim_args)
        return res




##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='Samle module')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--rfit', default=False, action='store_true')
    parser.add_argument('--glmfit', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        import doctest
        test_result = doctest.testmod()
        print(test_result)

    #+++++++++++++++++++++++++++++++#
    #    EXAMPLES                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.rfit:
        print(obj_in_r())
        
        a = pandas.DataFrame(numpy.arange(10).reshape(5, 2), index=numpy.arange(15, 20), columns=['a', 'b'])
        s = pandas.DataFrame([[1, 2], [3, 4]], index=["cov1", "cov2"], columns=['a', 'b'])
        t = pandas.DataFrame(numpy.arange(100, 110).reshape(5, 2), index=numpy.arange(15, 20), columns=["cov3", "cov4"])
        
        b = CovarSample(a, spa_covariates=s, time_covariates=t)
        
        f = FitEVT_glm(b, "gev", mul=['cov1'], sigl="cov2")
        #~ f = FitEVT_glm(b, "gev", mul = ['cov1', "cov2"])
        print(obj_in_r())
        cd = f.to_covardistribution()

        a = pandas.DataFrame(numpy.arange(10).reshape(5, 2), index=numpy.arange(15, 20), columns=['a', 'b'])
        s = pandas.DataFrame([[1, 2], [3, 4]], index=["x", "y"], columns=['a', 'b'])
        b = CovarSample(a, spa_covariates=s)
        cd = CovarDistribution('norm', loc=MathExpr("x * y"), scale=1)
        cd.nllh(data=b)

    if opts.glmfit:
        #~ pass
        a = pandas.DataFrame(numpy.arange(10).reshape(5, 2), index=numpy.arange(15, 20), columns=['a', 'b'])
        s = pandas.DataFrame([[1, 2], [3, 4]], index=["x", "y"], columns=['a', 'b'])
        b = CovarSample(a, spa_covariates=s)


        lexpr = LinearExpr([0.1, 0])
        vdistr = VarDistribution("gev", loc=lexpr, scale=1, c=-0.1)
        x = pandas.DataFrame({'x':numpy.arange(10000)})
        ds = vdistr(x)

        # ~ ds.apply(lambda x: x['loc'])

        obs = ds.apply(lambda x: float(x.random(1)))
        # ~ obs = CovarSample(obs, time_covariates=x)
        obs = CovarSample(obs, time_covariates=x)

        vdistr.nllh(obs)


        vdistr2 = vdistr.new([-0.05, 0.2, 1, 2])
        vdistr2.nllh(obs)

        print("start fit 1")

        t1 = time.time()
        glm = GLM_Fit(b, vdistr2, method="L-BFGS-B", bounds=[(-1, 1), (None, None), (None, None), (1e-8, None)])
        
        t2 = time.time()
        
        print("Fit with n={0}\n{1:.1f}secondes".format(b.merged_dataframe().index.size, t2-t1))

        
        glm = GLM_Fit(obs, vdistr2, method="L-BFGS-B", bounds=[(None, None), (None, None), (None, None), (1e-8, None)])


        t3 = time.time()
        
        print("Fit with n={0}\n{1:.1f}secondes".format(x.size, t3-t2))
