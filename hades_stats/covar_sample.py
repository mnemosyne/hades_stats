"""
For covar samples, eventually GLM fitting
"""
 
##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import numpy
import pandas


##======================================================================================================================##
##                CST                                                                                                   ##
##======================================================================================================================##

OBSNAME = "obs"

##======================================================================================================================##
##                DATA with COVARIATES                                                                                  ##
##======================================================================================================================##

class CovarSample(pandas.DataFrame):
    """ Class doc
    >>> a = pandas.DataFrame(numpy.arange(10).reshape(5,2), index = numpy.arange(15,20), columns = ['a', 'b'])
    >>> a
        a  b
    15  0  1
    16  2  3
    17  4  5
    18  6  7
    19  8  9
    >>> s = pandas.DataFrame([[1,2], [3,4]], index = ["c1", "c2"], columns = ['a', 'b'])
    >>> s
        a  b
    c1  1  2
    c2  3  4
    >>> t = pandas.DataFrame(numpy.arange(50,60).reshape(5,2), index = numpy.arange(15,20), columns = ["c3", "c4"])
    >>> t
        c3  c4
    15  50  51
    16  52  53
    17  54  55
    18  56  57
    19  58  59
    >>> b = CovarSample(a, spa_covariates = s, time_covariates = t)
    >>> b.merged_dataframe()
        obs  c1  c2  c3  c4
    15    0   1   3  50  51
    16    2   1   3  52  53
    17    4   1   3  54  55
    18    6   1   3  56  57
    19    8   1   3  58  59
    15    1   2   4  50  51
    16    3   2   4  52  53
    17    5   2   4  54  55
    18    7   2   4  56  57
    19    9   2   4  58  59
    >>> b.list_covariates()
    ['c1', 'c2', 'c3', 'c4']
    
    
    >>> s = pandas.DataFrame([[1,2], [3,4]], index = ["c1", "c2"], columns = ['b', 'a'])
    >>> s
        b  a
    c1  1  2
    c2  3  4

    >>> t = pandas.DataFrame(numpy.arange(50,60).reshape(5,2), index = numpy.arange(15,20), columns = ["c3", "c4"])
    >>> t
        c3  c4
    15  50  51
    16  52  53
    17  54  55
    18  56  57
    19  58  59
    >>> b = CovarSample(a, spa_covariates = s, time_covariates = t)
    >>> b.merged_dataframe()
        obs  c1  c2  c3  c4
    15    0   2   4  50  51
    16    2   2   4  52  53
    17    4   2   4  54  55
    18    6   2   4  56  57
    19    8   2   4  58  59
    15    1   1   3  50  51
    16    3   1   3  52  53
    17    5   1   3  54  55
    18    7   1   3  56  57
    19    9   1   3  58  59
    >>> b.list_covariates()
    ['c1', 'c2', 'c3', 'c4']
    """
    
    def __init__(self, data, spa_covariates=None, time_covariates=None):
        """ Class initialiser """
        pandas.DataFrame.__init__(self, data)
        
        # ~ assert isinstance(name, str)
        
        # ~ self.name = name
        self.spa_cov = spa_covariates
        self.time_cov = time_covariates
        
        if self.spa_cov is not None:
            assert isinstance(self.spa_cov, pandas.DataFrame)
            assert all(self.spa_cov.columns.sort_values() == self.columns.sort_values())
            assert all([isinstance(i, str) for i in self.spa_cov.index]), "Cov must be name\n{0}".format(self.spa_cov)
        
        if self.time_cov is not None:
            assert isinstance(self.time_cov, pandas.DataFrame)
            assert all(self.time_cov.index == self.index)
            assert all([isinstance(i, str) for i in self.time_cov.columns]), "Cov must be name\n{0}".format(self.spa_cov)
        
        #07/12/2023 FLC
        #Suppression de ce test pour éviter de sortir en erreur si aucune covariable n'est spécifiée dans le constructeur
        #if self.time_cov is None and self.spa_cov is None:
        #    raise ValueError("no need to this class if there is no covs")
    
    #07/12/2023 FLC : l'initialisation de spa_covariates et time_covariates dans le constructeur ne se passe pas bien pour pands
    # message warning => UserWarning: Pandas doesn't allow columns to be created via a new attribute name
    #L'initialisation par des méthodes en dehors du construteur est OK => A privilégier donc !
    
    def SetTimeCov(self, time_covariates):
        self.time_cov = time_covariates
    
    def SetSpaCov(self, spa_covariates):
        self.spa_cov = spa_covariates
        
    def list_covariates(self):
        """
        List the covarites
        See main doctest
        """
        res = []
        if self.time_cov is not None:
            res += list(self.time_cov.columns)
        if self.spa_cov is not None:
            res += list(self.spa_cov.index)
            
        assert len(set(res)) == len(res)
        res = sorted(res)
        return res
        
    def merged_dataframe(self, dropna=True):
        """
        Return a complte dataframe with data and cavariate values
        See main doctest
        """
        cols = self.columns
        ncol = len(cols)
        nrow = self.index.size
        
        res = [self[i] for i in cols]
        res = pandas.concat(res)
        # ~ print(res)
        res = pandas.DataFrame({OBSNAME:res})#, columns=[OBSNAME])
        # ~ print(res)
        if self.spa_cov is not None:
            sdata = []
            for isample in cols:
                icovs = self.spa_cov[isample]
                icovs = pandas.DataFrame(icovs).transpose()
                sdata += [icovs] * nrow
            sdata = pandas.concat(sdata)
            sdata.index = res.index
            #~ print(sdata)
            #~ assert 0, 'check'
            res = pandas.concat([res, sdata], axis=1)
        
        if self.time_cov is not None:
            # ~ print(ncol)
            tdata = [self.time_cov for isample in range(ncol)]
            # ~ print(tdata)
            tdata = pandas.concat(tdata, axis=0)
            # ~ print(tdata)
            res = pandas.concat([res, tdata], axis=1)
        
        if dropna:
            res = res.dropna(axis=0)
            
        return res



##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='Samle module')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        import doctest
        test_result = doctest.testmod()
        print(test_result)

    #+++++++++++++++++++++++++++++++#
    #    EXAMPLES                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.examples:
        
        a = pandas.DataFrame(numpy.arange(10).reshape(5, 2), index=numpy.arange(15, 20), columns=['a', 'b'])
        s = pandas.DataFrame([[1, 2], [3, 4]], index=["cov1", "cov2"], columns=['a', 'b'])
        t = pandas.DataFrame(numpy.arange(100, 110).reshape(5, 2), index=numpy.arange(15, 20), columns=["cov3", "cov4"])
        
        b = CovarSample(a, spa_covariates=s, time_covariates=t)


        x = numpy.arange(100)
        t = 2 * x
        cs = CovarSample(x, time_covariates=pandas.DataFrame({'t':t}))
