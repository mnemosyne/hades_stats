"""
Python module for theorical distribution analysis
"""


##======================================================================================================================##
##                                PACKAGES IMPORT                                                                       ##
##======================================================================================================================##

#packages python
import math
import os
import argparse
import doctest
from collections.abc import Iterable
from numbers import Number

import numpy
import pandas
import scipy.stats
import lmoments3.distr
from matplotlib import pyplot
import dill
#~ import pickle

from hades_stats.utils import neg_log_likelihood

##======================================================================================================================##
##                                CONSTANTS                                                                             ##
##======================================================================================================================##

DIST_CONTINU = [d for d in dir(scipy.stats) if isinstance(getattr(scipy.stats, d), scipy.stats.rv_continuous)]
DIST_DISCRETE = [d for d in dir(scipy.stats) if isinstance(getattr(scipy.stats, d), scipy.stats.rv_discrete)]
SP_DISTS = DIST_CONTINU + DIST_DISCRETE

LM_DISTS = [d for d in dir(lmoments3.distr) if isinstance(getattr(lmoments3.distr, d), scipy.stats.rv_continuous)]

_alias = {'exp':'expon', 'pois':'poisson', "binomial":'binom', "lnorm":'lognorm', "unif":'uniform', "pe3":'pearson3', 'gam':"gamma", "glo":'genlogistic', 'nor':"norm", "gno":"gennorm"}
_alias2 = {v : k for k, v in _alias.items()}
assert len(_alias) == len(_alias2)


GEVS = ['gev', 'genextreme', 'gum', 'gumbel']
GPDS = ['gpd', 'genpareto', "gpa"]

_sp_nm = "sp"
_lm_nm = "lmom"



_keys = sorted(set(SP_DISTS + LM_DISTS + sorted(_alias.keys()) + GEVS + GPDS))

ALIAS = pandas.DataFrame({}, columns=(_sp_nm, _lm_nm), index=_keys)

for k1 in _keys:
    if k1 in _alias: 
        k2 = _alias[k1]
    elif k1 in _alias2:
        k2 = _alias2[k1]
    else:
        k2 = None
    
    if k1 in SP_DISTS:
        ALIAS.loc[k1,_sp_nm] = k1
    elif k2 in SP_DISTS:
        ALIAS.loc[k1,_sp_nm] = k2
    else:
        pass
        
    if k1 in LM_DISTS:
        ALIAS.loc[k1,_lm_nm] = k1
    elif k2 in LM_DISTS:
        ALIAS.loc[k1,_lm_nm] = k2
    else:
        pass
        
for ind in GEVS:
    ALIAS.loc[ind,_sp_nm] = "genextreme"
    ALIAS.loc[ind,_lm_nm] = "gev"
for ind in GPDS:
    ALIAS.loc[ind,_sp_nm] = "genpareto"
    ALIAS.loc[ind,_lm_nm] = "gpa"
        
##======================================================================================================================##
##                                FUNCTIONS                                                                             ##
##======================================================================================================================##

KWARG_KYS = ("loc", "scale")


def sp_dist(name):
    """
    Return a scipy distribution
    >>> norm = sp_dist("norm")
    """
    res = getattr(scipy.stats, name)
    return res

def lm_dist(name):
    """
    Return a scipy distribution
    >>> norm = sp_dist("norm")
    """
    res = getattr(lmoments3.distr, name)
    return res
    
def sp_name(name):
    """
    >>> sp_name("GEV")
    'genextreme'
    """
    key = name.lower()
    res = ALIAS.loc[key][_sp_nm]
    assert res in SP_DISTS, f"{key} not in {SP_DISTS}"
    return res
    
def lm_name(name):
    """
    Function doc
    >>> lm_name('expon')
    'exp'
    """
    key = name.lower()
    res = ALIAS.loc[key][_lm_nm]
    assert res in LM_DISTS, f"{key} not in {LM_DISTS}"
    return res

def get_parnms(spdist):
    """
    >>> get_parnms(scipy.stats.norm)
    ['loc', 'scale']
    >>> get_parnms(scipy.stats.poisson)
    ['mu', 'loc', 'scale']
    >>> get_parnms(scipy.stats.genextreme)
    ['c', 'loc', 'scale']
    """
    res = ["loc", "scale"]
    nargs = spdist.numargs
    if nargs > 0:
        res = spdist.shapes.split(', ') + res
    
    return res
    
def get_parvals_dict(spdist, *args, **kwargs):
    """
    >>> get_parvals_dict(scipy.stats.genextreme, c=1)
    {'c': 1, 'loc': 0, 'scale': 1}
    >>> get_parvals_dict(scipy.stats.genextreme, c=1, loc=1)
    {'c': 1, 'loc': 1, 'scale': 1}
    >>> get_parvals_dict(scipy.stats.genextreme, c=1, loc=1, scale=2)
    {'c': 1, 'loc': 1, 'scale': 2}
    >>> get_parvals_dict(scipy.stats.poisson)
    Traceback (most recent call last):
        ...
    TypeError: _parse_args() missing 1 required positional argument: 'mu'
    >>> get_parvals_dict(scipy.stats.poisson, mu=1)
    {'mu': 1, 'loc': 0, 'scale': 1}
    """
    nargs = spdist.numargs

    #treat parvalues
    parvls = spdist._parse_args(*args, **kwargs)
    kwvals = parvls[1:]
    agvals = parvls[0]
    parvls = list(agvals) + list(kwvals)

    assert len(kwvals) == 2 and len(agvals) == nargs, "pb code"
        
    #treat param names
    parnms = get_parnms(spdist=spdist)

    #to dict
    assert len(parvls) == len(parnms), "pb code"
    res = dict(zip(parnms, parvls))
    return res
    

##======================================================================================================================##
##                                CLASSES                                                                               ##
##======================================================================================================================##

class Distribution(scipy.stats._distn_infrastructure.rv_continuous_frozen,scipy.stats._distn_infrastructure.rv_discrete_frozen):
    """ Class doc
    Classe permettant de realiser quelques stats sur une distribution, et deux trois graphs
    Entree:
    - scipy_func    :   Une fonction du module scipy

    >>> d = Distribution('norm', loc=0., scale=1.)
    
    >>> sorted(Distribution('norm', loc=0, scale=1).__dict__.keys())
    ['a', 'args', 'b', 'dist', 'kwds', 'name']
    
    >>> Distribution('norm', loc=0, scale=1).name
    'norm'
    >>> Distribution('exp', loc=0, scale=1).name
    'expon'
    >>> Distribution('gev', c= 0.1, loc=10, scale=5).name
    'genextreme'
    >>> Distribution('gpd', c=0.1, loc=0, scale=5).name
    'genpareto'
    >>> Distribution('gamma', a=1, loc=0, scale=5).name
    'gamma'
    >>> Distribution('gamma', a=0, loc=0, scale=5).name
    'gamma'
    >>> Distribution('gev', c= 0, loc=0, scale=1).name
    'genextreme'
    >>> "{0:.3f}".format(Distribution('norm',loc= 0, scale = 1).cdf(1))
    '0.841'
    >>> "{0:.3f}".format(Distribution('norm',loc= 0, scale = 1).quantile(0.5))
    '0.000'
    >>> "{0:.3f}".format(Distribution('pois', mu=1).pmf(0))
    '0.368'
    >>> "{0:.3f}".format(Distribution('pois',mu=1).pmf(1))
    '0.368'
    
    >>> print(Distribution('norm', loc=0., scale=1.))
    name     norm
    a        -inf
    b         inf
    loc         0
    scale       1

    
    >>> Distribution("pois", mu=10).iscontinuous()
    False
    >>> Distribution("norm", loc = 0, scale = 1 ).iscontinuous()
    True
    
    >>> gev = Distribution("gev", c=0.1, loc = 10, scale = 2)
    >>> pp = gev.to_pp(threshold = 10)
    >>> gpd1 = pp["gpd"]
    >>> pois1 = pp['pois']
    >>> gev1 = gpd1.to_gev(pois1['mu'])
    >>> gev1 == gev
    True
    >>> pp = gev.to_pp(npot_py = 2)
    >>> gpd2 = pp["gpd"]
    >>> gev2 = gpd2.to_gev(2)
    >>> gev2 == gev
    True
    >>> fig = pyplot.figure()
    >>> l=gev.return_level_plot(lw=5)
    >>> l=gev1.return_level_plot(lw = 4, ls = "--")
    >>> l=gev2.return_level_plot(lw = 3, ls = ":")
    >>> l=gpd1.return_level_plot(npy = pois1['mu'], lw = 2)
    >>> l=gpd2.return_level_plot(npy = 2,lw = 1, ls = "--")
    >>> fig.show()
    """
    
    ## old init 
    def __init__(self, name, *args, **kwargs):
        """
        Init object
        """
        self.name = sp_name(name)
        dist = sp_dist(name=self.name)
        params = get_parvals_dict(dist, *args, **kwargs)
        assert all(isinstance(v, Number) for v in params.values())
        # ~ print("check that each param is not iterable")
        # ~ print("use import numbers numbers.Number")
        
        #rv discrete do not need scale, error else
        #from https://docs.scipy.org/doc/scipy/reference/tutorial/stats.html
        #no estimation methods, such as fit, are available, and scale is not a valid keyword parameter. The location parameter, keyword loc can still be used to shift the distribution.
        if isinstance(dist, scipy.stats.rv_discrete): 
            params.pop("scale") #scipy scipy.stats.poisson.__init__ () line where _construct_argparser is called
            scipy.stats._distn_infrastructure.rv_discrete_frozen.__init__(self, dist, **params)
        else:
            # ~ print("in init")
            scipy.stats._distn_infrastructure.rv_continuous_frozen.__init__(self, dist, **params)
    
    ## new method since sicpy.1.9
    # ~ def __new__(cls, name, *args, **kwargs):
        # ~ print("in new")
        # ~ name = sp_name(name)
        # ~ dist = sp_dist(name=name)
        
        # ~ params = get_parvals_dict(dist, *args, **kwargs)

        # ~ #rv discrete do not need scale, error else
        # ~ #from https://docs.scipy.org/doc/scipy/reference/tutorial/stats.html
        # ~ #no estimation methods, such as fit, are available, and scale is not a valid keyword parameter. The location parameter, keyword loc can still be used to shift the distribution.
        # ~ if isinstance(dist, scipy.stats.rv_discrete): 
            # ~ params.pop("scale") #scipy scipy.stats.poisson.__init__ () line where _construct_argparser is called

        
        # ~ obj = scipy.stats._distn_infrastructure.rv_frozen(dist, *args, **kwargs)
        # ~ obj = super(dist, *args, **kwargs) #same result as the line before but fail sometime
        
        # ~ obj = dist.freeze(*args, **kwargs)
        
        # ~ obj.name = name
        
        # ~ return obj
    
    def __str__(self):
        """Object respresentation
        >>> print(Distribution("gev", c=-0.0736606, loc = 43.5634, scale = 12.8102))
        name     genextreme
        a          -13.5758
        b               inf
        c        -0.0736606
        loc         43.5634
        scale       12.8102
        """
        nm = "name"
        ks = ("a", "b")
        df0 = pandas.Series({nm: self[nm]})
        df1 = pandas.Series({k: self[k] for k in ks}, index=ks, dtype=float)
        df2 = self.params()
        df = pandas.concat([df0, df1, df2])
        #~ res = str(df)
        #~ res = df.to_string() #float_format='{0:.5g}'.format)
        res = df.to_string(float_format='{0:g}'.format)
        return res

    def __eq__(self, other):
        """
        >>> d1=Distribution("gev", c=0, loc = 1, scale = 2)
        >>> d2=Distribution("gev",c= 0,loc = 1, scale = 2)
        >>> d1 == d2
        True
        >>> d2=Distribution("gev",c= 0, loc = 1, scale = 2)
        >>> d1 == d2
        True
        >>> d2=Distribution("gev", c=0, loc = 1., scale = 2)
        >>> d1 == d2
        True
        >>> d1 == Distribution("gev", c=0, loc = 1.05, scale = 2)
        False
        >>> d1 == Distribution("gpd",c= 0, loc = 1., scale = 2.1)
        False
        >>> d1 == Distribution("gev",c= 0.1, loc = 1.0, scale = 2)
        False
        """
        _num_approx = 1e-6
        if isinstance(other, Distribution):
            d1 = self.params()
            d2 = other.params()
            dif = (d1 - d2).abs()
            # ~ print(dif)
            res = (dif <= _num_approx).all()
        else:
            res = False
                
        return res
        
        
    def __getitem__(self, key):
        """
        >>> d = Distribution('gev', c=0., loc = 0., scale = 1.)
        >>> d['name']
        'genextreme'
        >>> d['loc']
        0.0
        >>> d['scale']
        1.0
        >>> d['c']
        0.0
        >>> "{0:.2f}".format(d['RL10'])
        '2.25'
        >>> "{0:.2f}".format(d['rl_100'])
        '4.60'
        >>> "{0:.3f}".format(d["mean"])
        '0.577'
        >>> Distribution('norm', loc = 0, scale = 1)["mean"]
        0.0
        >>> d = Distribution('gev', c=-0.1, loc = 0., scale = 1.)
        >>> d['c']
        -0.1
        >>> d['-c']
        0.1
        """
        if key[0] == "-":
            negative = True
            key = key[1:]
        else:
            negative = False
        
        if key in self.kwds:
            res = self.kwds[key]
            
        elif key in self.__dict__:
            res = self.__dict__[key]
            
                
        elif "RL" in key.upper():
            rl = key.upper().replace("RL", "").replace("_", "")
            rl = float(rl)
            res = self.return_level(rl, npy=1)
        else:
            #~ lmom = "l_" in key
            summary = self.create_summary()
            assert key in summary, f"{key} not available in \n {summary.keys()}"
            res = summary[key]

        if negative:
            res = - res
            
        return res

    def params(self):
        """
        >>> d= Distribution("gev",c=0.,loc=1, scale=2)
        >>> d.params()
        c        0.0
        loc      1.0
        scale    2.0
        dtype: float64


        >>> Distribution("gev", c=0.1, loc = 1, scale = 2).params() 
        c        0.1
        loc      1.0
        scale    2.0
        dtype: float64
        >>> Distribution("gev", c=0.1,  loc=1, scale=2).params() 
        c        0.1
        loc      1.0
        scale    2.0
        dtype: float64
        >>> Distribution("pois",mu=0.,loc=1).params()
        loc    1.0
        mu     0.0
        dtype: float64
        
        >>> Distribution("norm").params()
        loc      0
        scale    1
        dtype: int64

        """
        parnms = sorted(self.kwds.copy())
        res = pandas.Series(self.kwds, index=parnms)
        return res
        
    def parargs(self):
        """
        Function doc
        >>> Distribution("gev",c=0.,loc=1, scale=2).parargs()
        ((0.0,), 1, 2)
        >>> Distribution("pois",mu=0.,loc=1).parargs()
        ((0.0,), 1, 1)
        >>> Distribution("norm",loc=1).parargs()
        ((), 1, 1)
        """
        res = self.dist._parse_args(**self.kwds)
        return res

    def n_par(self):
        """
        Renvoie le nombre de parametre de la distribution
        >>> Distribution('exp',loc = 0, scale = 1).n_par()
        2
        """
        res = len(self.args) + len(self.kwds)
        return res
        

    def quantile(self, f):
        """
        Renvoie le quantile correspondant a la frequence de non depassement f donne en entree : F-1(f)
        """
        res = self.ppf(f)
        return res

    def random(self, n, seed=None):
        """
        >>> d = Distribution('norm', loc = 0, scale = 1)
        >>> s = d.random(500)
        >>> s.min() > -5 and s.min() < -1
        True
        >>> s.max() < 5 and s.max() > 1
        True
        >>> s.mean() < 0.5 and s.mean() > -0.5
        True
        """
        if seed is not None:
            numpy.random.seed(seed)
            
        res = self.rvs(n)
        res = numpy.array(res, dtype=float)
        return res

    def nllh(self, values):
        """
        Neg log likelihhod
        >>> d = Distribution('norm', loc = 0, scale = 1)
        >>> vals = d.random(500)
        >>> nllh = d.nllh(vals)
        >>> nllh > 600 and nllh < 800
        True
        >>> "{0:.3f}".format(Distribution('norm',loc = 0, scale = 1).nllh([0,0,0]))
        '2.757'
        """
        values = numpy.array(values, dtype=float)
        probs = self.pdf(values)
        res = neg_log_likelihood(probs)
        return res
        
        
    def iscontinuous(self):
        """Test if the dist is continuous"""
        res = self.name in DIST_CONTINU
        return res

    def create_summary(self):
        """
        Realise des stats sur la fonction
        >>> [Distribution('norm', loc=0., scale=1.).create_summary()[i] for i in ['mean', 'var', 'skew', 'kurtosis']]
        [0.0, 1.0, 0.0, 3.0]
        >>> [Distribution('norm', loc=10, scale=5).create_summary()[i] for i in ['mean', 'var', 'skew', 'kurtosis']]
        [10.0, 25.0, 0.0, 3.0]
        >>> [Distribution('exp', loc=0, scale=1).create_summary()[i] for i in ['mean', 'var', 'skew', 'kurtosis']]
        [1.0, 1.0, 2.0, 9.0]
        >>> [Distribution('exp', loc=1, scale=1).create_summary()[i] for i in ['mean', 'var', 'skew', 'kurtosis']]
        [2.0, 1.0, 2.0, 9.0]
        >>> [Distribution('exp', loc=1, scale=2.).create_summary()[i] for i in ['mean', 'var', 'skew', 'kurtosis']]
        [3.0, 4.0, 2.0, 9.0]
       
        #~ >>> lnames = ["l_skew", "l_kurtosis"]
        #~ >>> ["{0:.2f}".format(Distribution('exp', loc=1, scale=2.).create_summary()[i]) for i in lnames]
        #~ ['0.33', '0.17']
        #~ >>> ["{0:.2f}".format(Distribution('norm', loc=1, scale=2.).create_summary()[i]) for i in lnames]
        #~ ['0.00', '0.12']
        #~ >>> ["{0:.2f}".format(Distribution("gev",c= 0, loc=1, scale=2.).create_summary()[i]) for i in lnames]
        #~ ['0.17', '0.15']
        #~ >>> ["{0:.2f}".format(Distribution('gev',c= -0.1, loc=1, scale=2.).create_summary()[i]) for i in lnames]
        #~ ['0.11', '0.13']
        """
        res = {}

        dic = {m:i for i, m in enumerate(['mean', 'var', 'skew', 'kurtosis'])}
        moments = self.stats('mkvs')

        #moments
        for moment, i in dic.items():
            value = float(moments[i])

            if moment == 'kurtosis':
                value += 3.

            res[moment] = value


        return res

    def return_level(self, t, npy=None):
        """Return the return level of the given return period or return event
        >>> test_distr = Distribution('gev',c=0, loc=0, scale=1)
        >>> round(test_distr.return_level(10, 1), 2)
        2.25
        >>> all(test_distr.return_level((10, 100), 1) - numpy.array([2.25036733, 4.60014923]) < 1e-8)
        True
        >>> all(test_distr.return_level((5, 50), 2) - numpy.array([2.25036733, 4.60014923]) < 1e-8)
        True
        """
        t = numpy.array(t, dtype=float)

        if npy is not None:
            assert isinstance(npy, (int, float)), "faut n obs par ans pr passer de n evt de retour a periode de retour"
            t = t * npy

        cdf = 1 - 1 / t
        res = self.ppf(cdf)
        return res

    def return_event(self, rl):
        """ Renvoie la periode de retour estime de la valeur passee en entree
        >>> test_distr = Distribution("gev",c= 0,loc=0, scale=1)
        >>> round(test_distr.return_event(scipy.stats.genextreme(0).ppf(0.9)),0)
        10.0
        >>> (test_distr.return_event((scipy.stats.genextreme(0).ppf(0.8), scipy.stats.genextreme(0).ppf(0.95))) - numpy.array([ 5., 20.]) < 1e-8).all()
        True
        """
        rl = numpy.array(rl, dtype=float)
        cdf = self.cdf(rl)
        res = 1 / (1-cdf)
        return res

    def return_period(self, rl, npy):
        """ Renvoie la periode de retour estime de la valeur passee en entree
        >>> test_distr = Distribution('gev',c=0, loc=0, scale=1)
        >>> round(test_distr.return_period(scipy.stats.genextreme(0).ppf(0.9), npy=2.),0)
        5.0
        >>> (test_distr.return_period((scipy.stats.genextreme(0).ppf(0.9), scipy.stats.genextreme(0).ppf(0.99)), npy=2.) - numpy.array([ 5., 50.]) < 1e-8).all()
        True
        """
        assert isinstance(npy, (int, float)), "npy is required"
        res = self.return_event(rl) / npy
        return res

    def cdf_plot(self, rank=False, **kwargs):
        """Graph la fonction de repartition
        >>> test_distr = Distribution('norm', loc=0., scale=1.)
        >>> f=pyplot.figure()
        >>> l= test_distr.cdf_plot()
        >>> l.set_label('r')
        >>> l=pyplot.legend()
        >>> test_distr = Distribution('poisson', mu=10)
        >>> f.show()
        
        >>> f=pyplot.figure()
        >>> l= test_distr.cdf_plot()
        >>> l.set_label('r')
        >>> l=pyplot.legend()
        >>> f.show()
        """
        assert rank is False or isinstance(rank, (int, float)), f"pb input : {rank}"
        
        #~ if xlim == None:
        xlim = self.quantile((0.01, 0.99))

        if self.iscontinuous():
            argdic = {"ls" : "-", "color" : "k", "marker" : ""}
            x_model = numpy.linspace(xlim[0], xlim[-1], num=1000, endpoint=True)
        else:
            argdic = {"ls" : "", "color" : "k", "marker" : "o"}
            x_model = numpy.arange(xlim[0], xlim[-1] + 1, dtype=int)

        argdic.update(kwargs)


        y_model = self.cdf(x_model)

        if rank:
            y_model = y_model * rank

        line = pyplot.plot(x_model, y_model, **argdic)[0]

        if not self.iscontinuous():
            pyplot.vlines(x=x_model, ymin=0, ymax=y_model, color=line.get_color())

        #~ pyplot.title("CDF")
        pyplot.xlabel("x")
        pyplot.ylabel("CDF - F(x)")


        return line

    def pdf_plot(self, log_axis=0, xlim=None, fill=False, **kwargs):
        """Graph la fonction de densite
        >>> test_distr = Distribution('norm', loc=0., scale=1.)
        >>> f=pyplot.figure()
        >>> l= test_distr.pdf_plot()
        >>> l.set_color('r')
        >>> l=pyplot.legend()
        >>> l= test_distr.pdf_plot(fill = True)
        >>> l.set_color('r')
        >>> l=pyplot.legend()
        >>> l = Distribution('pois',mu=1, loc = 1).pdf_plot()
        >>> f.show()
        """
        if xlim is None:
            xlim = self.quantile((0.001, 0.999))

        if self.iscontinuous():
            x_model = numpy.linspace(xlim[0], xlim[-1], num=1000, endpoint=True)
        else:
            x_model = numpy.arange(xlim[0], xlim[-1] + 1, dtype=int)

        if self.iscontinuous():
            y_model = self.pdf(x_model)
        else:
            x_model = numpy.array(x_model, dtype=int)
            y_model = self.pmf(x_model)

        if self.iscontinuous():
            if fill:
                res = pyplot.fill_between(x_model, y1=y_model, y2=0, color='0.7')
            else:
                res = pyplot.plot(x_model, y_model, **kwargs)[0]

        else:
            assert not fill, f'pb entree demande "False" got {fill.__class__} : {fill}'
            line = pyplot.plot(x_model, y_model)[0]
            line.set_ls('')
            line.set_marker('o')
            vlines = pyplot.vlines(x=x_model, ymin=0, ymax=y_model, color=line.get_color())
            vlines.set_label('')

            res = {'line':line, 'vlines':vlines}

        pyplot.grid(1)
        pyplot.title("PDF")
        pyplot.xlabel("$x$")
        pyplot.ylabel("$f(x)$")

        if log_axis:
            pyplot.semilogx()

        return res

    def return_level_plot(self, xlim=None, plot_axes=1, npy=None, **kwargs):
        """Graph le periode de retour ou le nombre d'evenement de retour
        >>> test_distr = Distribution('norm', loc=0., scale=1.)
        >>> l= test_distr.return_level_plot()
        >>> test_distr = Distribution('gev',c=0, loc=0, scale=1)
        >>> f=pyplot.figure()
        >>> l= test_distr.return_level_plot(npy=2.)
        >>> l.set_label('r')
        >>> f=pyplot.figure()
        >>> l= test_distr.return_level_plot()
        >>> l.set_color('r')
        >>> l=pyplot.legend()
        >>> f.show()
        """
        if xlim is None:
            xlim = (1.1, 500)

        x_model = numpy.linspace(xlim[0], xlim[-1], num=5000, endpoint=True)
        y_model = self.return_level(x_model, npy=npy)

        if npy:
            title = "N events Return Level"
        else:
            title = "N years Return Level"

        #~ label = "{0}; $\lambda = {1:.1f}.an^{{-1}}$".format(self.return_latex_params(sep='; '), self.npy)
        line = pyplot.plot(x_model, y_model, **kwargs)[0]

        pyplot.title(title)
        pyplot.xlabel("$T_{x}$")
        pyplot.ylabel("$x$")

        if plot_axes:
            return_period = [1, 2, 5, 10, 20, 50, 100, 200, 500]
            x_ticks = numpy.array(return_period, dtype=int)
            fig = pyplot.gcf()
            ax = fig.gca()
            ax.semilogx()
            ax.set_xticks(x_ticks)
            ax.set_xticklabels([str(i) for i in x_ticks])
            ax.grid(1)

        pyplot.xlim(xlim)

        return line



    def to_pp(self, threshold=None, npot_py=None):
        """From a gev or a gumbel law ---->>>> exp or gpd / poisson : it requires the number of expected 
        >>> Distribution('gev', c=0.1, loc =0, scale = 1).to_pp()
        Traceback (most recent call last):
                ...
        ValueError: need threshold or npot_py
        >>> Distribution('gev',  c=0.1,loc =0,scale =1).to_pp(npot_py = 1)["gpd"]['loc']
        0.0
        >>> Distribution('gev', c=0.1,loc =0,scale =1).to_pp(threshold = 0)["pois"]["mu"]
        1.0
        >>> Distribution('gev',c=0,loc =0,scale =1).to_pp(2)["gpd"].name
        'genpareto'

        >>> Distribution('gev',c=0.1,loc =0,scale =1).to_pp(2)["gpd"].name
        'genpareto'
        
        
        >>> gev = Distribution('gev',c=0.1,loc =0,scale =1)
        >>> gpd = gev.to_pp(2)['gpd']
        >>> pois = gev.to_pp(2)['pois']

        >>> fig = pyplot.figure()
        >>> l=gev.return_level_plot()
        >>> l=gpd.return_level_plot( npy=pois["mu"] )

        >>> fig.show()

        """
        assert self.name == 'genextreme', 'require a gev'

        name = 'gpd'

        if self["c"] == 0:
            #~ name = 'exp'

            scale = self['scale']
            arg = 0

            if isinstance(threshold, (float, int)):
                loc = threshold
                npot_py = math.exp((self['loc'] - threshold) / scale)
            elif isinstance(npot_py, (float, int)):
                loc = self['loc'] - scale * math.log(npot_py)
            else:
                raise ValueError("need threshold or npot_py")

        else:
            arg = - self["c"]
            if isinstance(threshold, (float, int)):
                loc = threshold
                npot_py = (self['scale'] / (arg*(- self['loc'] + threshold + self['scale'] / arg)))**(1/arg)
                scale = self['scale'] / npot_py ** arg
            elif isinstance(npot_py, (float, int)):
                scale = self['scale'] / npot_py ** arg
                loc = self['loc'] + (scale / arg) * (1 - npot_py ** arg)
                #~ loc = (self['scale'] / (npot_py ** arg)) / arg + self['loc'] - self['scale'] / arg
            else:
                raise ValueError("need threshold or npot_py")

        gpd = Distribution(name, c=arg, loc=loc, scale=scale)
        pois = Distribution("pois", mu=npot_py)
        
        res = {"gpd" : gpd, "pois" : pois}    
        
        
        return res
        
    def to_gev(self, npy):
        """
        Renvoie la gev qui a le meme comportement asymptotique que la distribution: 
        necessite une exp ou une gpd et un nb d evt par an cf Madsen()
        >>> pp=Distribution('gpd',c=0,loc = 0, scale = 1).to_gev(1)
        >>> Distribution('norm', loc = 0, scale = 1, ).to_gev(npy = 365)
        Traceback (most recent call last):
                ...
        AssertionError: need exp or gpd to get gev
        >>> Distribution('gpd',c=0,loc = 0, scale = 1).to_gev(npy = 2).name
        'genextreme'
        >>> Distribution('gpd',c=0, loc = 0, scale = 1).to_gev(npy = 2)["c"]
        0.0
        >>> Distribution('gpd', c=0.1, loc = 0, scale = 1).to_gev(npy = 3).name
        'genextreme'
        """

        assert self.name in ('expon', 'genpareto'), 'need exp or gpd to get gev'
        assert isinstance(npy, (float, int)), "pb input"

        name = 'gev'

        if self.name == 'expon' or self["c"] == 0:
            loc = self['loc'] + self['scale'] * math.log(npy)
            sca = self['scale']
            arg = 0.
        else:
            arg = - self["c"]
            loc = self['loc'] - (self['scale'] / self["c"]) * (1 - npy ** (self["c"]))
            sca = self['scale'] * npy ** self["c"]
            

        res = Distribution(name, c=arg, loc=loc, scale=sca)
        return res
            
            
    def scale_distr(self, ratio, par2scale=("loc", "scale")):
        """
        Function doc
        >>> d0 = Distribution("norm", loc = 1, scale = 2)
        >>> print(d0.scale_distr(ratio = 2))
        name     norm
        a        -inf
        b         inf
        loc         2
        scale       4
        >>> print(d0.scale_distr(ratio = 0.1))
        name     norm
        a        -inf
        b         inf
        loc       0.1
        scale     0.2
        
        >>> d0 = Distribution("gumbel", c=0, loc = 1, scale = 2)
        >>> print(d0.scale_distr(ratio = 2))
        name     genextreme
        a              -inf
        b               inf
        c                 0
        loc               2
        scale             4
        >>> print(d0.scale_distr(ratio = 0.1))
        name     genextreme
        a              -inf
        b               inf
        c                 0
        loc             0.1
        scale           0.2

        >>> d0 = Distribution("gev", c=0.1, loc = 1, scale = 2)
        >>> print(d0.scale_distr(ratio = 2))
        name     genextreme
        a              -inf
        b                10
        c               0.1
        loc               2
        scale             4
                
        >>> d0 = Distribution("gev", c=-0.1, loc = 1, scale = 2)
        >>> print(d0.scale_distr(ratio = 2))
        name     genextreme
        a               -10
        b               inf
        c              -0.1
        loc               2
        scale             4
        
        >>> print(d0.scale_distr(ratio = 0.1))
        name     genextreme
        a               -10
        b               inf
        c              -0.1
        loc             0.1
        scale           0.2
        >>> print(d0.scale_distr(ratio = 4, par2scale = "loc"))
        name     genextreme
        a               -10
        b               inf
        c              -0.1
        loc               4
        scale             2
        >>> print(d0.scale_distr(ratio = 4, par2scale = "scale"))
        name     genextreme
        a               -10
        b               inf
        c              -0.1
        loc               1
        scale             8
        >>> print(d0.scale_distr(ratio = 4,par2scale = ["scale", 'c']))
        name     genextreme
        a              -2.5
        b               inf
        c              -0.4
        loc               1
        scale             8
        
        >>> print(d0.scale_distr(ratio = 4, par2scale = ()))
        name     genextreme
        a               -10
        b               inf
        c              -0.1
        loc               1
        scale             2
        >>> print(d0.scale_distr(ratio = 4, par2scale = None))
        name     genextreme
        a               -10
        b               inf
        c              -0.1
        loc               1
        scale             2
        """
        if isinstance(par2scale, str):
            par2scale = [par2scale]
        elif isinstance(par2scale, Iterable):
            par2scale = list(par2scale)
        elif par2scale is None:
            par2scale = []
        else:
            raise ValueError(f"dont know what to do with {par2scale}")
        
        
        #~ print(par2scale)
        
        pars = self.params()
        
        others = [i for i in pars.index if i not in par2scale]
        #~ print(others)
        
        others = pars.loc[others]
        news = pars.loc[par2scale] * ratio
        
        newpars = pandas.concat([others, news])
        newpars = dict(newpars)
        #~ print(newpars)
        res = Distribution(name=self.name, **newpars)
        return res
        
        
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##


if __name__ == '__main__':
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#

    
    parser = argparse.ArgumentParser(description='Samle module')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--pickle', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)

    #+++++++++++++++++++++++++++++++#
    #    TESTS PICKLE               #
    #+++++++++++++++++++++++++++++++#
    
    if opts.pickle:
        print("pikle")

        _testname = os.path.join(os.path.dirname(__file__), "dist.pic")
                
        d = Distribution("norm", loc=0., scale=1.)

        
        with open(_testname, "wb") as pcw:
            dill.dump(obj=d, file=pcw)


        with open(_testname, "rb") as pcr:
            dp = dill.load(file=pcr)

        assert dp == d
        os.remove(_testname)
