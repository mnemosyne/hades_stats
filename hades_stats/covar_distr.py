"""
Create distribution which can depend on covariates
"""

 


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest
from numbers import Number
import time

import numpy
import pandas


from hades_stats.utils import neg_log_likelihood
from hades_stats.distribution import Distribution, sp_dist, sp_name
from themis_maths.exprs import MathExpr, LinearCombination, Expr, LinearExpr, InterceptExpr
# ~ from hades_stats.math_exprs import MathExpr, LinearCombination
# ~ from hades_stats.math_functions import Expr, LinearExpr, InterceptExpr
from hades_stats.covar_sample import CovarSample, OBSNAME


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

# ~ _param_sep = "--"


##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

# ~ def ():
    # ~ """
    # ~ Function doc
    # ~ >>> 'afarie'
    # ~ """
    

##======================================================================================================================##
##                VAR DISTRIBUTION CLASS                                                                                ##
##======================================================================================================================##

class VarDistribution:
    """ Class doc
    >>> lexpr = LinearExpr([0.1, 0])
    >>> vdistr = VarDistribution("norm", loc=lexpr, scale=1)
    >>> vdistr2 = vdistr.new([0.2, 1, 2])
    >>> vdistr.params()
    loc    a    0.1
           b    0.0
    scale  a    1.0
    dtype: float64

    >>> vdistr2.params()
    loc    a    0.2
           b    1.0
    scale  a    2.0
    dtype: float64

    
    >>> x = numpy.arange(1990,2010)
    >>> x = pandas.DataFrame({"x":x}, index=x)
    >>> ds = vdistr(x)
    >>> ds2 = vdistr2(x)

    >>> vdistr.expand_params(x)
            loc  scale
    1990  199.0    1.0
    1991  199.1    1.0
    1992  199.2    1.0
    1993  199.3    1.0
    1994  199.4    1.0
    1995  199.5    1.0
    1996  199.6    1.0
    1997  199.7    1.0
    1998  199.8    1.0
    1999  199.9    1.0
    2000  200.0    1.0
    2001  200.1    1.0
    2002  200.2    1.0
    2003  200.3    1.0
    2004  200.4    1.0
    2005  200.5    1.0
    2006  200.6    1.0
    2007  200.7    1.0
    2008  200.8    1.0
    2009  200.9    1.0

    >>> vdistr2.expand_params(x)
            loc  scale
    1990  399.0    2.0
    1991  399.2    2.0
    1992  399.4    2.0
    1993  399.6    2.0
    1994  399.8    2.0
    1995  400.0    2.0
    1996  400.2    2.0
    1997  400.4    2.0
    1998  400.6    2.0
    1999  400.8    2.0
    2000  401.0    2.0
    2001  401.2    2.0
    2002  401.4    2.0
    2003  401.6    2.0
    2004  401.8    2.0
    2005  402.0    2.0
    2006  402.2    2.0
    2007  402.4    2.0
    2008  402.6    2.0
    2009  402.8    2.0

    >>> ds.apply(lambda x: x['loc'])
    1990    199.0
    1991    199.1
    1992    199.2
    1993    199.3
    1994    199.4
    1995    199.5
    1996    199.6
    1997    199.7
    1998    199.8
    1999    199.9
    2000    200.0
    2001    200.1
    2002    200.2
    2003    200.3
    2004    200.4
    2005    200.5
    2006    200.6
    2007    200.7
    2008    200.8
    2009    200.9
    dtype: float64
    >>> ds2.apply(lambda x: x['loc'])
    1990    399.0
    1991    399.2
    1992    399.4
    1993    399.6
    1994    399.8
    1995    400.0
    1996    400.2
    1997    400.4
    1998    400.6
    1999    400.8
    2000    401.0
    2001    401.2
    2002    401.4
    2003    401.6
    2004    401.8
    2005    402.0
    2006    402.2
    2007    402.4
    2008    402.6
    2009    402.8
    dtype: float64
    >>> (ds.apply(lambda x: x['scale']) == 1.0).all()
    True
    >>> (ds2.apply(lambda x: x['scale']) == 2.0).all()
    True

    >>> obs = ds.apply(lambda x: float(x.random(1)))
    >>> obs = pandas.DataFrame({OBSNAME:obs.values, "x": obs.index})
    >>> obs
               obs     x
    0   197.686135  1990
    1   199.984622  1991
    2   200.081318  1992
    3   201.009573  1993
    4   199.450034  1994
    5   199.095323  1995
    6   199.054640  1996
    7   198.153523  1997
    8   200.782367  1998
    9   198.798932  1999
    10  198.814953  2000
    11  199.894350  2001
    12  201.686148  2002
    13  200.536716  2003
    14  199.376215  2004
    15  199.787007  2005
    16  201.225245  2006
    17  200.539487  2007
    18  200.031164  2008
    19  200.669969  2009

    >>> nllh = vdistr.nllh(obs)
    >>> nllh2 = vdistr2.nllh(obs) 
    >>> 0 < nllh  < 50
    True
    >>> 4000 < nllh2 < 5000
    True
    
    """
    
    def __init__(self, name, **kwargs):
        """ Class initialiser """
        self.kwds = {}
        for kw, kexpr in kwargs.items():
            if isinstance(kexpr, (str, Number)):
                kexpr = InterceptExpr(kexpr)
            elif isinstance(kexpr, (Expr)):
                pass
            else:
                raise ValueError("exp expr or number")
            self.kwds[kw] = kexpr
        
        self.name = sp_name(name)
        
        
    def expand_params(self, data):
        """Create DataFrame with parameter values
        Index correspond to the index of the object given
        See main doctest"""
        dicpars = {par : expr(**data) for par, expr in self.kwds.items()}
        res = pandas.DataFrame(dicpars, index=data.index)
        return res

    def __call__(self, data):
        """Return a list of distribution. See main doctest"""
        kwgdf = self.expand_params(data) #pandas.DataFrame(dicpars)
        
        res = kwgdf.apply(lambda x: Distribution(self.name, **x), axis='columns')
        
        if res.index.size == 1:
            res = res.iloc[0]
        else:
            res = pandas.Series(res, index=data.index)
        # ~ res=None
        return res

    def nllh(self, data):
        """Negative log likelihood. See main doctest"""
        if isinstance(data, CovarSample):
            data = data.merged_dataframe()
            
        xval = data[OBSNAME]
        distrs = self(data)
        assert (xval.index == distrs.index).all()
        
        # ~ print(set([type(distrs.loc[ind]) for ind in xval.index]))
        probs = [distrs.iloc[ind].pdf(xval.iloc[ind]) for ind in range(xval.index.size)]
        
        res = neg_log_likelihood(probs)
        # ~ res = None
        return res
       
       
    def _nllh(self, data):
        """Fast computation of nllh. See main doctest"""
        dist = sp_dist(self.name)
        if isinstance(data, CovarSample):
            data = data.merged_dataframe()
            
        xval = data[OBSNAME]
        
        kwgdf = self.expand_params(data) #pandas.DataFrame(dicpars)
        
        # ~ assert 0, "use vector in .pdf"
        
        # ~ probs = [dist.pdf(xval.iloc[ind], **kwgdf.iloc[ind]) for ind in range(xval.index.size)]
        probs = dist.pdf(xval.values, **kwgdf)
        res = neg_log_likelihood(probs)
        # ~ res = None
        return res
        
       
    def params(self):
        """Return sorted parameter in a dataframe. See main doctest"""
        alldic = {(par, a): v for par, expr in self.kwds.items() for a, v in expr.params().items()}
        res = pandas.Series(alldic).sort_index()
        return res

    def new(self, params):
        """Create new with paramters given. See main doctest"""
        parindex = self.params().index
        newpars = pandas.Series(params, index=parindex)
        # ~ print(newpars)
        newexpr = {kpar : parexpr.new(newpars.loc[kpar].values) for kpar, parexpr in self.kwds.items()}
        # ~ print(newexpr)
        res = self.__class__(name=self.name, **newexpr)
        return res

    
        
    def npar(self):
        """Number of parameters. See main doctest"""
        res = self.params().size
        assert 0, "use method of lexpr"
        return res
        
        

##======================================================================================================================##
##                COVAR DISTRIBUTION CLASS                                                                              ##
##======================================================================================================================##

class CovarDistribution:
    """
    cree une distribution dont les parametre peuvent dependre de covariable
    >>> cd = CovarDistribution('gev', c=0,loc = MathExpr('x * y'), scale =5)
    >>> cd.__class__ == CovarDistribution
    True
    >>> sorted(CovarDistribution('gev', c=0, loc = MathExpr('x * y'), scale =5).__dict__.keys())
    ['kwds', 'name']
    """

    def __init__(self, name, **kwargs):
        """Init object"""
        self.kwds = {}
        for kw, kexpr in kwargs.items():
            if isinstance(kexpr, (str, Number)):
                kexpr = MathExpr(str(kexpr))
            elif isinstance(kexpr, (MathExpr, LinearCombination)):
                pass
            else:
                raise ValueError("exp expr or number")
            self.kwds[kw] = kexpr
        
        self.name = name


    def __call__(self, df):
        """
        >>> cd = CovarDistribution('gev', c=0, loc = MathExpr('2. * x * y'), scale =5)
        >>> cd2 = cd(pandas.DataFrame({'y':5., 'x' : 1}, index = [1]))
        >>> cd2.__class__ == Distribution
        True
        >>> cd2['loc']
        10.0
        >>> cd3 = cd(pandas.DataFrame({'x':5,'y':5}, index = [1]))
        >>> cd3.__class__ == Distribution
        True
        >>> cd3['loc']
        50.0

        >>> cd = CovarDistribution('gev',c= 0., loc = MathExpr('x * y'), scale =5)
        
        >>> cd1s= cd(pandas.DataFrame({"x" : numpy.array([1, 2, 3]), "y" : 5}))
        >>> for i in cd1s: print(i['loc'], i['scale'], i['c'])
        5.0 5.0 0.0
        10.0 5.0 0.0
        15.0 5.0 0.0
        >>> cd1s= cd(pandas.DataFrame({"x" : numpy.array([1, 2, 3]), "y" : numpy.array([5, 10, 15])}))
        >>> for i in cd1s: print(i['loc'], i['scale'], i['c'])
        5.0 5.0 0.0
        20.0 5.0 0.0
        45.0 5.0 0.0
        """
        kwargs = {ky: mexp(df) for ky, mexp in self.kwds.items()}
        #~ print(kwargs)
        
        kwgdf = pandas.DataFrame(kwargs)
        #~ print(kwgdf)
        
        fun = lambda x: Distribution(self.name, **dict(x))
        res = kwgdf.apply(fun, axis='columns')
        
        if res.index.size == 1:
            res = res.iloc[0]
        
        return res


    def nllh(self, data):
        """
        Compute the neg log likelihood of th data
        >>> ex = MathExpr('x')
        >>> cd = CovarDistribution('norm', loc = ex, scale =1)

        >>> vals0 = Distribution('norm', loc = 0, scale = 1).random(10, seed = 2)
        >>> vals1 = Distribution('norm', loc = 1, scale = 1).random(10, seed = 1)
        >>> vals2 = Distribution('norm', loc = 2, scale = 1).random(10, seed = 3)
        >>> xobs = pandas.DataFrame({"x0" : vals0, "x1" : vals1, "x2" : vals2})
        >>> s = pandas.DataFrame({"x0" : [0], "x1" : [1], "x2": [2]}, index = ["x"])
        >>> s
           x0  x1  x2
        x   0   1   2
        >>> xobs
                 x0        x1        x2
        0 -0.416758  2.624345  3.788628
        1 -0.056267  0.388244  2.436510
        2 -2.136196  0.471828  2.096497
        3  1.640271 -0.072969  0.136507
        4 -1.793436  1.865408  1.722612
        5 -0.841747 -1.301539  1.645241
        6  0.502881  2.744812  1.917259
        7 -1.245288  0.238793  1.372999
        8 -1.057952  1.319039  1.956182
        9 -0.909008  0.750630  1.522782
        >>> dataf = CovarSample(xobs, spa_covariates = s)
        >>> dataf.merged_dataframe()
                obs  x
        0 -0.416758  0
        1 -0.056267  0
        2 -2.136196  0
        3  1.640271  0
        4 -1.793436  0
        5 -0.841747  0
        6  0.502881  0
        7 -1.245288  0
        8 -1.057952  0
        9 -0.909008  0
        0  2.624345  1
        1  0.388244  1
        2  0.471828  1
        3 -0.072969  1
        4  1.865408  1
        5 -1.301539  1
        6  2.744812  1
        7  0.238793  1
        8  1.319039  1
        9  0.750630  1
        0  3.788628  2
        1  2.436510  2
        2  2.096497  2
        3  0.136507  2
        4  1.722612  2
        5  1.645241  2
        6  1.917259  2
        7  1.372999  2
        8  1.956182  2
        9  1.522782  2
        >>> nllh= cd.nllh(dataf)
        >>> nllh > 40 and nllh < 50
        True

        """
        if isinstance(data, CovarSample):
            data = data.merged_dataframe()
        
        xval = data[OBSNAME]
        distrs = self(data)
        probs = [d.pdf(x) for x, d in zip(xval, distrs)]
        res = neg_log_likelihood(probs)
        return res





##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='Samle module')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)

    #+++++++++++++++++++++++++++++++#
    #    EXAMPLES                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.examples:
        #~ pass
        a = pandas.DataFrame(numpy.arange(10).reshape(5, 2), index=numpy.arange(15, 20), columns=['a', 'b'])
        s = pandas.DataFrame([[1, 2], [3, 4]], index=["x", "y"], columns=['a', 'b'])
        b = CovarSample(a, spa_covariates=s)
        cd = CovarDistribution('norm', loc=MathExpr("x * y"), scale=1)
        cd.nllh(data=b)


        lexpr = LinearExpr([0.1, 0])
        vdistr = VarDistribution("norm", loc=lexpr, scale=1)
        x = pandas.DataFrame({'x':numpy.arange(10000)})
        ds = vdistr(x)

        ds.apply(lambda x: x['loc'])

        obs = ds.apply(lambda x: float(x.random(1)))
        # ~ obs = CovarSample(obs, time_covariates=x)
        obs = CovarSample(obs, time_covariates=x)

        t1 = time.time()
        
        
        vdistr2 = vdistr.new([0.2, 1, 2])
        vdistr3 = vdistr.new([0.15, 0, 2])
        vdistr4 = vdistr.new([0.1, 0, 1.1])
        
        nllh1 = vdistr._nllh(obs)
        nllh2 = vdistr2._nllh(obs)
        nllh3 = vdistr3._nllh(obs)
        nllh4 = vdistr4._nllh(obs)

        for iin, nllh in enumerate([nllh1, nllh2, nllh3, nllh4]):
            print(iin, "{0:.1f}".format(nllh))

        t2 = time.time()
        print("Time for computing 4 nllh with n={0}\n{1:.1f}secondes".format(x.size, t2-t1))
