"""
Module contenant differents tests statistiques:
- Tendance
- Rupture
- Racine Unitaire
- GOF
- Comparaison d'echantillon
"""

##======================================================================================================================##
##                                PACKAGES IMPORT                                                                       ##
##======================================================================================================================##

#packages python
import math
import random
import argparse
from numbers import Number
#~ from collections import Iterable
import doctest
#packages perso

import numpy
import scipy.stats
import scipy.interpolate


from hades_stats.utils import corfun
from hades_stats.distribution import Distribution

numpy.seterr(all='ignore')

##======================================================================================================================##
##                                                CONSTANTES                                                            ##
##======================================================================================================================##

TEST_TYPES = ['bp', 'trend', 'unit_root', 'bpvar', 'poisson']

##====================================================================================================================##
##                                              FONCTION                                                              ##
##====================================================================================================================##

def example_test_time_serie(test_type):
    """
    Renvoi une time serie correspondant au critere:
    ex : test_type='bp' et h0 = True renvoie une serie ne presentant pas de rupture
    """
    assert test_type in TEST_TYPES, 'choisir un type de test dans {0}'.format(TEST_TYPES)
    res = {'h0' : {}, 'h1' : {}}

    x_iid_norm = numpy.array([
        2.17, -1.29, -0.39, 0.43, 0.83, -0.13, 1.20, -0.60, -1.21, -0.20, 0.29, 0.96, 0.26, -0.29, 1.91, -2.78, -1.06, -0.06, 1.75, -0.76, -1.70, 
        1.40, -2.21, 1.13, 0.19, -0.35, -0.99, -1.38, -0.97, 1.27, -0.68, -0.58, 1.53, 0.33, -0.20, 0.93, -0.38, 0.23, -1.19, -0.13, 0.48, 0.01, 
        0.93, 1.45, 0.32, -0.74, -0.33, -1.10, 0.32, 1.00, 1.97, -0.35, 0.16, -0.69, 0.13, -0.54, -0.57, -0.76, -0.29, -0.82, 1.74, -1.20, -0.68, 
        1.86, 0.12, 2.10, 0.99, 0.75, 1.76, 0.65, -0.06, 0.02, 0.34, -1.06, 0.29, -1.91, -0.13, -0.13, 1.10, -1.16, -0.34, -1.11, 0.52, -0.08, 
        1.10, 0.46, 0.81, -0.37, -0.83, 0.13, -0.12, -0.50, 0.72, -0.89, 0.62, 0.52, -1.09, 0.29, -0.40, -0.58])


    x_iid_gum = numpy.array([
        0.40, -0.21, 3.39, 1.66, 0.16, 1.23, 0.29, -0.42, 2.10, 0.76, 0.51, 2.84, -0.77, 0.75, 1.38, 0.36, -0.48, -1.15, -1.16, 3.14, 1.25, 2.07, 
        2.44, 0.83, 0.43, 0.18, -0.60, 0.51, 0.70, -1.11, -1.38, -0.25, -1.58, 0.20, -0.28, 0.58, -1.19, 2.68, 1.30, 2.20, 2.83, 0.39,
        -0.34, 2.06, 0.42, 0.58, -0.11, 1.11, 0.58, 3.84, 0.72, -0.74, -0.41, -1.10, 2.87, 0.26, 0.23, 0.40, -1.17, 1.22, 1.52, 2.34, 1.75, 2.21, 
        -1.09, 2.06, 0.41, -0.34, 1.88, 0.73, -0.23, 0.45, 1.52, -0.14, 0.21, -0.79, -0.82, 1.91, 1.01, 0.33, 1.19, -0.25, 1.22, -0.78,
        -0.50, -0.77, 1.61, -0.28, 0.19, -1.51, -0.17, -1.17, 1.89, 0.26, 0.80, 2.63, 2.93, -0.74, 1.47, 3.36])

    x_iid_gev = numpy.array([
        6.22, -0.89, 3.99, -1.53, -1.10, 1.83, 1.15, 3.23, 0.31, -0.68, -0.14, 1.03, -1.16, 1.21, 1.40, 1.50, -0.12, 0.37, -1.25, 0.78, -0.18, 1.20, 
        0.40, 0.68, -0.68, 1.89, 0.70, 0.07, -0.21, -0.06, -0.15, 0.47, -1.14, 0.45, -0.28, -0.58, -0.28, 0.46, -0.25, 10.55, 1.91, -0.23, 1.72, 
        0.79, -0.41, -0.75, -0.72, -0.37, -0.20, 0.34, 1.25, 1.86, 2.41, 2.05, -0.56, 2.20, -0.79, -1.05, -1.00, -0.09, 4.00, -0.53, 0.76, 0.09, 
        0.27, 1.95, 3.50, -0.31, -0.23, -0.36, -0.54, -0.03, 0.10, 2.11, 0.55, 0.99, -0.52, 1.52, -0.46, 0.69, 0.99, 0.45, 3.79, -0.59, 2.63, 
        -1.14, -0.91, -0.11, 1.83, 0.09, -0.73, -0.67, -0.32, 0.34, 0.75, 0.36, -0.32, 1.09, 0.01, -0.30])

    x_iid_poisson = numpy.array([
        5, 6, 7, 7, 6, 5, 3, 8, 4, 7, 7, 8, 4, 3, 6, 8, 4, 5, 2, 7, 4, 2, 4, 6, 2, 3, 6, 6, 6, 4, 9, 3, 2, 5, 6, 
        4, 9, 2, 4, 3, 5, 3, 1, 9, 6, 3, 3, 8, 8, 8])

    x_iid_binom = numpy.array([8, 9, 9, 9, 9, 9, 10, 6, 10, 10, 10, 10, 8, 10, 9, 8, 9, 9, 8, 8, 9, 8, 8, 9, 9, 9, 9, 8, 8, 10, 10, 9, 8, 8, 8, 6, 10, 10, 9, 10, 10, 9, 9, 9, 9, 8, 8, 10, 8, 10])


    if test_type == 'bp':

        break_index = 49
        break_value = 1

        res['h0']['pettitt'] = numpy.array([7.1, 8.1, 8.2, 11.1, 6.6, 4.9, 4.0, 17.7, 6.5, 4.6, 8.8, 11.6, 6.8, 7.5, 6.9, 8.1, 9.3, 7.5, 10.0, 8.7, 9.1, 8.9, 9.1, 9.6, 8.1, 9.8, 8.2])

        res['h0']['test1'] = numpy.array([
            0.16, -1.23, 1.8, -0.12, 0.39, 1.43, 0.59, -2.53, -0.1, -0.85, 1.3, -0.52, 1.4, 0.47, -0.26, -0.07, 0.79, -0.14, 0.48, -1.68, 0.23, 0.96,
            0.01, 1.36, 1.0, -1.51, 1.5, -1.86, 1.52, -0.96, 0.42, 0.88, 0.37, 0.05, 0.01, 1.44, 1.17, -0.05, -1.6, 0.62, 1.3, -0.66, -1.52, 0.75,
            0.26, 0.73, -1.44, -1.5, 1.79, 0.21])

        res['h0']['norm'] = x_iid_norm
        res['h0']['gum'] = x_iid_gum
        res['h0']['gev'] = x_iid_gev

        res['h1']['pettitt'] = numpy.array([
            -1.05, 0.96, 1.22, 0.58, -0.98, -0.03, -1.54, -0.71, -0.35, 0.66, 0.44, 0.91, -0.02, -1.42, 1.26, -1.02, -0.81, 1.66, 1.05, 0.97,
            2.14, 1.22, -0.24, 1.60, 0.72, -0.12, 0.44, 0.03, 0.66, 0.56, 1.37, 1.66, 0.10, 0.80, 1.29, 0.49, -0.07, 1.18, 3.29, 1.84])

        res['h1']['test1'] = numpy.array([
            -1.48, -0.29, -1.25, -0.76, 1.04, 1.66, 0.69, 0.19, -1.15, 0.21, -1.19, -1.95, -2.19, -3.89, -2.75, -1.09, -1.62, -1.71, -3.77, -1.67])

        res['h1']['norm'] = numpy.append(x_iid_norm[:break_index], x_iid_norm[break_index:] + break_value)
        res['h1']['gum'] = numpy.append(x_iid_gum[:break_index], x_iid_gum[break_index:] + break_value)
        res['h1']['gev'] = numpy.append(x_iid_gev[:break_index], x_iid_gev[break_index:] + break_value)


    elif test_type == 'trend':

        trend = 0.1 * numpy.arange(len(x_iid_norm))

        res['h0']['norm'] = x_iid_norm
        res['h0']['gum'] = x_iid_gum
        res['h0']['gev'] = x_iid_gev
        res['h1']['norm'] = x_iid_norm + trend
        res['h1']['gum'] = x_iid_gum + trend
        res['h1']['gev'] = x_iid_gev + trend

    elif test_type == 'unit_root':

        res['h0']['norm'] = numpy.cumsum(x_iid_norm)
        res['h0']['gum'] = numpy.cumsum(x_iid_gum)
        res['h0']['gev'] = numpy.cumsum(x_iid_gev)
        res['h1']['norm'] = x_iid_norm
        res['h1']['gum'] = x_iid_gum
        res['h1']['gev'] = x_iid_gev

    elif test_type == 'bpvar':
        break_index = 49
        coef_var = 2

        res['h0']['norm'] = x_iid_norm
        res['h0']['gum'] = x_iid_gum
        res['h0']['gev'] = x_iid_gev
        res['h1']['norm'] = numpy.append(x_iid_norm[:break_index], x_iid_norm[break_index:]*coef_var)
        res['h1']['gum'] = numpy.append(x_iid_gum[:break_index], x_iid_gum[break_index:] * coef_var)
        res['h1']['gev'] = numpy.append(x_iid_gev[:break_index], x_iid_gev[break_index:] * coef_var)


    elif test_type == 'poisson':
        res['h0'] = x_iid_poisson
        res['h1'] = x_iid_binom

    else:
        assert 0, 'pb code source'

    return res


def pval2star(pval, limits=(0.01, 0.05, 0.1)):
    """
    Function doc
    >>> pval2star(0.2)
    ''
    >>> pval2star(0.09)
    '*'
    >>> pval2star(0.02)
    '**'
    >>> pval2star(0.005)
    '***'
    """
    res = ""
    for val in limits:
        if pval <= val:
            res += "*"
            
    return res
    
    

##====================================================================================================================##
##                              Class TEST PARENTE                                                                    ##
##====================================================================================================================##

class Test:
    """
    Classe parente
    """
    H0 = 'Null hypothesis'
    H1 = 'Alternative hypothesis'
    NAME = 'TEST'
    INFO = 'info test'
    TYPE = 'type de test'
    
    ATTRIBUTES = ("statistic", "pvalue")
    ADD_ATTRS = ("break_index",)

    def __init__(self, alpha_risk):
        """
        >>> sorted(Test(0.05).__dict__.keys())
        TEST
        ['alpha_risk', 'h0', 'pvalue', 'statistic']
        """
        self.alpha_risk = alpha_risk
        self.pvalue = None
        self.statistic = None
        
        results = self._compute_test()
        
        assert isinstance(results, dict), 'pb exp dict got {0}'.format(results)

        # get statistic and pvalue
        for attr in self.ATTRIBUTES:
            val = results.pop(attr)
            setattr(self, attr, val)
        
        for attr in self.ADD_ATTRS:
            if attr in results:
                val = results.pop(attr)
                setattr(self, attr, val)
        
        assert not results, "Dont know what to do with {0}".format(results)
        
        self.h0 = self.return_h0(alpharisk=self.alpha_risk)
        

    def return_h0(self, alpharisk):
        """
        Renvoie l'acceptation ou le rejet de l'hypothese nulle en fonction du risque de premiere espece passe en entree
        """
        assert isinstance(alpharisk, Number)
        res = self.pvalue >= alpharisk
        return res

    def __str__(self):
        """
        Representation de l'objet
        """
        bgtxt = '{0:^70}\n'.format(self.NAME).upper()
        res = [bgtxt]
        res.append('Infos test : {0}\n'.format(self.INFO))
        res.append('{0:^70}'.format('HYPOTHESIS'))
        res.append('Test the null hypothesis    :    H0= {0}'.format(self.H0))
        res.append('{0:^70}'.format('VS'))
        res.append('The alternative hypothesis  :    H1= {0}\n'.format(self.H1))
        res.append('{0:^70}'.format('RESULT'))
        res.append('Test statistic        :      {0}'.format(self.statistic))
        if 'break_index' in self.__dict__:
            res.append('Break index           :      {0}'.format(self['break_index']))
        res.append('Associated P VALUE    :      {0}'.format(self.pvalue))
        res.append('The test              :      {0} the null hypothesis H0'.format(self.accept_h0()))
        res.append('With a aplha risk     :      {0}'.format(self.alpha_risk))
        res = '\n'.join(res)

        return res

    def _compute_test(self):
        """
        Methode qui sera apppele par toute les classe filles
        """
        res = dict(pvalue=1, statistic=1)
        print(self.NAME)
        return res
    
    def __getitem__(self, key):
        """
        Get the key
        """
        if key in self.__dict__:
            res = self.__dict__[key]
        else:
            assert 0, 'pb {0} not available in {1}'.format(key, self.__dict__.keys())
                
        return res
    
    def accept_h0(self):
        """
        Function doc
        >>> t = Test(0.05)
        TEST
        >>> t.h0
        True
        >>> t.accept_h0()
        'ACCEPT'
        >>> t.h0 = False
        >>> t.accept_h0()
        'REJECT'
        """
        res = ('reject', 'accept')[int(self.h0)].upper()
        return res

##====================================================================================================================##
##                              TEST SPECIAUX                                                                         ##
##====================================================================================================================##

class Test_PoissonDisperionRatio(Test):
    """
    Test si les evenement passe en entree sont issus d'une loi de poisson.
    >>> h0 = Test_PoissonDisperionRatio(example_test_time_serie('poisson')['h0'])
    >>> h0.h0
    True
    >>> '{0.statistic:.2f}, {0.pvalue:.2f}'.format(h0)
    '0.92, 0.82'
    >>> h1 = Test_PoissonDisperionRatio(example_test_time_serie('poisson')['h1'])
    >>> h1.h0
    False
    >>> '{0.statistic:.2f}, {0.pvalue:.2f}'.format(h1)
    '0.11, 0.00'
    """
    H0 = 'Counts are poisson distributed'
    H1 = "Counts aren't poisson distributed"
    NAME = 'Poisson dispersion Index Test'
    INFO = "Comparison between observed and poisson distributed counts"
    TYPE = "Poisson Dispersion Index"

    def __init__(self, n_evts, alpha_risk=0.05):
        """
        >>> sorted(Test_PoissonDisperionRatio(example_test_time_serie('poisson')['h0']).__dict__.keys())
        ['alpha_risk', 'h0', 'n_evts', 'pvalue', 'statistic']
        """

        self.n_evts = numpy.array(n_evts, dtype=int)
        assert self.n_evts.size > 1, 'il faut une list de plus de 1 element got : {0}'.format(self.n_evts.size)
        Test.__init__(self, alpha_risk=alpha_risk)

    def _compute_test(self):
        """
        Calcul de la statistique du test et la pvalue associee
        """
        mean = self.n_evts.mean()
        var = self.n_evts.std(ddof=1) ** 2
        size = self.n_evts.size
        d_index = var / mean
        statistic = size * var / mean
        df = size - 1
        chicdf = scipy.stats.chi2(df).cdf(statistic)
        pvalue = 1 - abs(0.5 - chicdf)*2
        res = {'statistic'     :       d_index, 'pvalue'       :       pvalue}
        return res

class Test_LikelihoodRatio(Test):
    """
    Test s'il faut contraindre ou relache un parametre de deux modeles emboites
    >>> '{0.pvalue:.2f}'.format(Test_LikelihoodRatio(null_loglik = -10, alt_loglik = -9, df_diff = 1))
    '0.16'
    >>> '{0.pvalue:.2f}'.format(Test_LikelihoodRatio(null_loglik = -10, alt_loglik = -8, df_diff = 1))
    '0.05'
    >>> '{0.pvalue:.2f}'.format(Test_LikelihoodRatio(null_loglik = -10, alt_loglik = -8, df_diff = 2))
    '0.14'
    >>> '{0.pvalue:.2f}'.format(Test_LikelihoodRatio(null_loglik = -10, alt_loglik = -5, df_diff = 2))
    '0.01'
    >>> '{0.pvalue:.2f}'.format(Test_LikelihoodRatio(2, df_diff = 2))
    '0.14'
    >>> '{0.pvalue:.2f}'.format(Test_LikelihoodRatio(-5, df_diff = 2))
    '0.01'
    """
    H0 = "M0 (embeded in M1) is true"
    H1 = "alternative model M1 is True"
    NAME = "Likelihood Ratio Test"
    INFO = "Likelihood comparison of two models: M0 is a particular case of M1"
    TYPE = "Likelihood ratio"

    def __init__(self, *nllh_diff, df_diff, alpha_risk=0.05, **kwarg):
        """
        >>> sorted(Test_LikelihoodRatio(null_loglik = -10, alt_loglik = -9, df_diff = 1).__dict__.keys())
        ['alpha_risk', 'df_diff', 'h0', 'loglik_dif', 'pvalue', 'statistic']
        """
        if len(kwarg) == 2:
            null_loglik = kwarg.pop('null_loglik')
            alt_loglik = kwarg.pop('alt_loglik')
            assert null_loglik < 0, "log lik must be < 0"
            assert alt_loglik < 0, "log lik must be < 0"
            assert null_loglik <= alt_loglik, "M0 must be <= M1"
            loglik_dif = alt_loglik - null_loglik
        elif len(nllh_diff) == 1:
            nllh_diff = list(nllh_diff)
            loglik_dif = abs(nllh_diff.pop(0))
        else:
            raise ValueError("Dont knwo what to do with :\n{0}\n{1}".format(nllh_diff, kwarg))

        assert not kwarg, "pb dont know what to do whth : {0}".format(kwarg)
        assert not nllh_diff, "pb dont know what to do whth : {0}".format(nllh_diff)
        assert df_diff > 0, "le model alternatif doit avoir plus de parametre"

        self.loglik_dif = loglik_dif
        self.df_diff = df_diff
        Test.__init__(self, alpha_risk=alpha_risk)

    def _compute_test(self):
        """
        Calcul de la statistique du test et la pvalue associee
        """
        statistic = 2 * self.loglik_dif
        assert statistic >= 0, 'pb source'
        chi2 = Distribution("chi2", self.df_diff)
        pvalue = 1 - chi2.cdf(statistic)

        res = {"statistic":statistic, "pvalue":pvalue}
        return res


##====================================================================================================================##
##                              TEST BREAK POINT                                                                      ##
##====================================================================================================================##

class Test_BP_Pettitt(Test):
    """
    Test de Pettitt (1979) le valeurs testes sont issue de l'article
    >>> h0 = Test_BP_Pettitt(example_test_time_serie('bp')['h0']['pettitt'])
    >>> "{0:.3f}".format(h0.pvalue)
    '0.092'
    >>> h0.statistic
    90
    >>> h0.break_index
    15
    >>> h0.h0
    True
    >>> h1 = Test_BP_Pettitt(example_test_time_serie('bp')['h1']['pettitt'])
    >>> "{0:.3f}".format(h1.pvalue)
    '0.007'
    >>> h1.statistic
    232
    >>> h1.break_index
    16
    >>> h1.h0
    False
    """
    H0 = "La serie ne persente pas de rupture"
    H1 = "Il y a une rupture dans la serie"
    NAME = "Test de Pettitt"
    INFO = "Test non parametrique sur les rang"
    TYPE = "Break Point"


    def __init__(self, x_obs, alpha_risk=0.05):
        """
        >>> sorted(Test_BP_Pettitt(example_test_time_serie('bp')['h0']['pettitt']).__dict__.keys())
        ['alpha_risk', 'break_index', 'h0', 'pvalue', 'statistic', 'x_obs']
        """
        self.x_obs = numpy.array(x_obs, dtype=float)
        assert self.x_obs.size > 1, 'il faut une list de plus de 1 element got : {0}'.format(self.x_obs.size)

        Test.__init__(self, alpha_risk=alpha_risk)

    def _compute_test(self):
        """
        Calcul de la statistique du test et la pvalue associee
        """
        def pettitt_p_value(k_statistic, n_x_obs):
            """
            calcul la pvalue du test
            """
            num = -6. * k_statistic ** 2
            den = n_x_obs ** 3 + n_x_obs **2
            frac = num / float(den)
            #~ res = 2 * math.exp(frac) # valeur two sidded
            res = math.exp(frac) # one sidded

            return res

        u_stats = numpy.array([self._pettitt_u_stat(i) for i in range(1, self.x_obs.size+1)])
        abs_u_stats = abs(u_stats)
        k_statistic = abs_u_stats.max()
        index_k_stat = abs_u_stats.argmax()
        statistic = u_stats[index_k_stat]

        pvalue = pettitt_p_value(k_statistic, self.x_obs.size)

        if pvalue > 1:
            pvalue = 1
        res = {"pvalue"  :   pvalue, "statistic"     :   statistic, 'break_index':index_k_stat}

        return res

    def _pettitt_u_stat(self, t):
        """
        >>> t=Test_BP_Pettitt(example_test_time_serie('bp')['h0']['pettitt'])
        >>> t._pettitt_u_stat(1)
        12
        >>> t._pettitt_u_stat(16)
        90
        >>> t=Test_BP_Pettitt(example_test_time_serie('bp')['h1']['pettitt'])
        >>> t._pettitt_u_stat(1)
        35
        >>> t._pettitt_u_stat(17)
        232
        """
        def cmp(a, b):
            """
            cmp method
            """
            return int(a > b) - int(a < b)

        x1 = self.x_obs[:t]
        x2 = self.x_obs[t:]
        #~ print(x1)
        #~ print(x2)
        res = [cmp(xj, xi) for xj in x2 for xi in x1]
        #~ print(res)
        res = sum(res)
        return res

class Test_BP_KF(Test):
    """
    >>> x = example_test_time_serie('bp')['h0']['norm']
    >>> "{0.break_index} - {0.statistic[0]:.5f}, {0.statistic[99]:.5f}".format(Test_BP_KF(x,model = 1, k0 = 10, lam=0.4 ))
    '[62 69] - 0.55430, 0.53576'
    >>> "{0.break_index} - {0.statistic[0]:.5f}, {0.statistic[99]:.5f}".format(Test_BP_KF(x,model = 2, k0 = 10, lam=0.4 ))
    '[62 69] - 0.55430, 0.53576'
    >>> "{0.break_index} - {0.statistic[0]:.5f}, {0.statistic[99]:.5f}".format(Test_BP_KF(x,model = 3, k0 = 10, lam=0.4 ))
    '[62 69] - 0.55430, 0.53576'
    >>> "{0.break_index} - {0.statistic[0]:.5f}, {0.statistic[99]:.5f}".format(Test_BP_KF(x,model = 4, k0 = 10, lam=0.4 ))
    '[62 69] - 0.55430, 0.53576'
    >>> x = example_test_time_serie('bp')['h1']['norm']
    >>> all(["{0.break_index} - {0.statistic[0]:.5f}, {0.statistic[99]:.5f}".format(Test_BP_KF(x,model = i, k0 = 10, lam = 0.4 ))== '[48] - 0.46800, 0.65450' for i in range(1,5)])
    True
    >>> x = example_test_time_serie('bp')['h0']['gev']
    >>> all(["{0.break_index} - {0.statistic[0]:.5f}, {0.statistic[99]:.5f}".format(Test_BP_KF(x,model = i, k0 = 10, lam = 0.4 ))== 'None - 0.17497, 0.17497' for i in range(1,4)])
    True
    >>> "{0.break_index} - {0.statistic[0]:.5f}, {0.statistic[99]:.5f}".format(Test_BP_KF(x,model = 4, k0 = 10, lam=0.4 ))
    '[38 39] - 0.16626, 0.16687'
    >>> x = example_test_time_serie('bp')['h1']['gev']
    >>> all(["{0.break_index} - {0.statistic[0]:.5f}, {0.statistic[99]:.5f}".format(Test_BP_KF(x,model = i, k0 = 10, lam = 0.4 ))== '[38] - 0.16626, 0.24974' for i in range(1,4)])
    True
    >>> "{0.break_index} - {0.statistic[0]:.5f}, {0.statistic[99]:.5f}".format(Test_BP_KF(x,model = 4, k0 = 10, lam=0.4 ))
    '[38 39 48] - 0.16626, 0.25393'
    >>> x = example_test_time_serie('bp')['h0']['pettitt']
    >>> all(["{0.break_index} - {0.statistic[0]:.5f}, {0.statistic[26]:.5f}".format(Test_BP_KF(x,model = i, k0 = 10, lam = 0.4 ))== '[6 7] - 0.22941, 0.31925' for i in range(1,4)])
    True
    >>> "{0.break_index} - {0.statistic[0]:.5f}, {0.statistic[26]:.5f}".format(Test_BP_KF(x,model = 4, k0 = 10, lam=0.4 ))
    '[3 6 7] - 0.33759, 0.31925'
    >>> x = example_test_time_serie('bp')['h1']['pettitt']
    >>> all(["{0.break_index} - {0.statistic[0]:.5f}, {0.statistic[39]:.5f}".format(Test_BP_KF(x,model = i, k0 = 10, lam = 0.4 ))== '[ 3 16] - 0.40735, 0.52264' for i in (1,3)])
    True
    >>> "{0.break_index} - {0.statistic[0]:.5f}, {0.statistic[39]:.5f}".format(Test_BP_KF(x,model = 2, k0 = 10, lam=0.4 ))
    '[16 37] - 0.29570, 0.84990'
    >>> "{0.break_index} - {0.statistic[0]:.5f}, {0.statistic[39]:.5f}".format(Test_BP_KF(x,model = 4, k0 = 10, lam=0.4 ))
    '[16] - 0.29570, 0.52264'
    """
    H0 = "La serie ne persente pas de rupture"
    H1 = "Il y a une rupture dans la serie"
    NAME = "Test de Kehagias et Fortin"
    INFO = "Test tres sensible"
    TYPE = "Break Point"

    def __init__(self, x_obs, model=1, k0=10, lam=0.4, imax=10, alpha_risk=0.05):
        """
        >>> sorted(Test_BP_KF(numpy.arange(10)).__dict__.keys())
        ['alpha_risk', 'break_index', 'h0', 'imax', 'k0', 'lam', 'model', 'pvalue', 'statistic', 'x_obs']
        """
        self.x_obs = numpy.array(x_obs, dtype=float)
        assert self.x_obs.size > 1, 'il faut une list de plus de 1 element got : {0}'.format(self.x_obs.size)

        self.model = model
        self.lam = lam
        self.k0 = k0
        self.imax = imax

        Test.__init__(self, alpha_risk=alpha_risk)

    def _compute_test(self):
        """
        Calcul de la statistique du test et la pvalue associee
        """
        def ff02a(T, K):
            """
            # initialize breaks
            """
            res = [0]
            for k in range(1, K):
                prec = res[k-1]
                add = math.floor(T/float(K)) - 1
                res.append(prec + add)
            res.append(T)
            res = numpy.array(res, dtype=int)
            return res


        def ff06a(b, T):
            """
            # compute state-stay prob
            """
            res = (T - b.size +1) / float(T)
            return res

        def ff03(model, x, be):
            """
            Fonction qui choisi entre les differente ff03
            """

            if model in (1, 2):
                res = ff03a(x, be)                  #% estimation of means
            elif model in (3, 4):#% model == 3 | model == 4
                res = ff03b(x, be)                    # % estimation of means
            else:
                assert 0, 'pb code source'
            return res


        def ff03a(x, be):
            """
            % function function mue=ff03a(x,be)
            % compute means
            """
            res = []
            for k in range(1, be.size):
                ind0 = be[k-1]
                ind1 = be[k]
                ind = numpy.arange(ind0, ind1, dtype=int)
                mean = x[ind].mean()
                res.append(mean)

            res = numpy.sort(res)
            return res

        def ff03b(x, be):
            """
            #~ % function function mue=ff03a(x,be)
            #~ % compute means
            """
            res = []
            for k in range(1, be.size):
                ind0 = be[k-1]
                ind1 = be[k]
                ind = numpy.arange(ind0, ind1, dtype=int)
                mean = x[ind].mean()
                res.append(mean)

            res = numpy.sort(res)
            res = numpy.append(res, x.mean())
            return res

        def ff04(model, ppe, mue, sgmue):
            """
            fonction qui choisi entre les differentes fonction ff04
            """

            if model == 1:
                res = ff04a(ppe, mue, sgmue)             # estimation of transition matrix
            elif model == 2:
                res = ff04b(ppe, mue, sgmue)             # estimation of transition matrix
            elif model == 3:
                res = ff04c(ppe, mue, sgmue)             # estimation of transition matrix
            elif model == 4:
                res = ff04d(ppe, mue, sgmue)             # estimation of transition matrix
            else:
                assert 0, 'pb code source'

            return res

        def ff04a(ppe, mue, sgmue):
            """
            #~ % function Pe=ff04a(ppe,mue,sgmue)
            #~ % compute transition matrix
            """
            size = mue.size
            res = []

            for k1 in range(size):
                row = []
                for k2 in range(size):
                    num = (mue[k1] - mue[k2]) ** 2
                    den = float(2 * sgmue ** 2)
                    exp = math.exp(- num / den)
                    row.append(exp)
                row = numpy.array(row)
                row[k1] = 0
                row = row / (0.000000001+ row.sum())
                row = (1 - ppe) * row
                row[k1] = ppe

                res.append(row)

            res = numpy.array(res)

            return res

        def ff04b(ppe, mue, sgmue):
            """
            % function Pe=ff04b(ppe,mue,sgmue)
            % compute transition matrix
            """
            size = mue.size

            # order the states by their mu's
            #~ [q,ii]=sort(mue)
            ii = numpy.argsort(mue)

            if size < 2:
                res = 1
                K = 0

            else:
                res = []
                Q = numpy.zeros(shape=(size, size))
                R = numpy.eye(size)

                for k1 in range(size):

                    q2 = (mue[ii[1]] + mue[ii[0]]) /2.
                    q2 = (q2 - mue[k1])/ sgmue

                    Q[k1, ii[0]] = scipy.stats.norm(loc=0, scale=1).cdf(q2)

                    for k2 in range(1, size -1):
                        q1 = (mue[ii[k2]]+mue[ii[k2-1]])/2.
                        q1 = (q1-mue[k1])/sgmue
                        q2 = (mue[ii[k2+1]]+mue[ii[k2]])/2.
                        q2 = (q2-mue[k1])/sgmue

                        Q[k1, ii[k2]] = scipy.stats.norm(loc=0, scale=1).cdf(q2)-scipy.stats.norm(loc=0, scale=1).cdf(q1)

                    q1 = (mue[ii[size -1]] + mue[ii[size-2]])/2
                    q1 = (q1-mue[k1]) / sgmue

                    Q[k1, ii[size - 1]] = 1 - scipy.stats.norm(loc=0, scale=1).cdf(q1)

                    result = (1-ppe) * Q[k1, :] + ppe * R[k1, :]
                    res.append(result)

            res = numpy.array(res, dtype=float)
            return res

        def ff04c(ppe, mue, sgmue):
            """
            % function Pe=ff04c(ppe,mue,sgmue)
            % compute transition matrix
            """
            size = mue.size -1
            res = []

            for k1 in range(size):
                row = []
                for k2 in range(size):
                    num = (mue[-1] - mue[k2]) ** 2
                    den = float(2 * sgmue ** 2)
                    exp = math.exp(- num / den)
                    row.append(exp)
                row = numpy.array(row)
                row[k1] = 0
                row = row / (0.000000001 + row.sum())
                row = (1 - ppe) * row
                row[k1] = ppe

                res.append(row)

            res = numpy.array(res)
            return res


        def ff04d(ppe, mue, sgmue):
            """
            % function Pe=ff04b(ppe,mue,sgmue)
            % compute transition matrix
            """
            size = mue.size -1

            # order the states by their mu's
            #~ [q,ii]=sort(mue)
            ii = numpy.argsort(mue[0:size])

            if size < 2:
                res = 1
                K = 0

            else:
                res = []
                Q = numpy.zeros(shape=(size, size))
                R = numpy.eye(size)

                for k1 in range(size):

                    q2 = (mue[ii[1]] + mue[ii[0]]) /2.
                    q2 = (q2 - mue[-1])/ sgmue

                    Q[k1, ii[0]] = scipy.stats.norm(loc=0, scale=1).cdf(q2)

                    for k2 in range(1, size -1):
                        q1 = (mue[ii[k2]]+mue[ii[k2-1]])/2.
                        q1 = (q1-mue[-1])/sgmue
                        q2 = (mue[ii[k2+1]]+mue[ii[k2]])/2.
                        q2 = (q2-mue[-1])/sgmue

                        Q[k1, ii[k2]] = scipy.stats.norm(loc=0, scale=1).cdf(q2) -scipy.stats.norm(loc=0, scale=1).cdf(q1)

                    q1 = (mue[ii[size -1]] + mue[ii[size-2]])/2
                    q1 = (q1-mue[-1]) / sgmue

                    Q[k1, ii[size - 1]] = 1 - scipy.stats.norm(loc=0, scale=1).cdf(q1)

                    result = (1-ppe) * Q[k1, :] + ppe * R[k1, :]
                    res.append(result)

                res = numpy.array(res, dtype=float)
            return res

        def ff05(model, x, mue, sgepe, Pe):
            """
            fonctiopn qui choisi entre ff05a et ff05b
            """

            if model in (1, 2):
                res = ff05a(x, mue, sgepe, Pe)       #estimation of breaks, state
            elif model in (3, 4):
                res = ff05b(x, mue, sgepe, Pe)   # estimation of breaks, state
            else:
                assert 0, 'pb code source'

            return res

        def ff05a(x, mue, sgepe, Pe):
            """
            % function [be,ze,xe,p,d]=ff05a(x,mue,sgepe,Pe)
            % compute breaks, state, prob
            """

            K = mue.size
            T = x.size

            p = numpy.ones(shape=(1, K)) / float(K)
            d = p
            r = numpy.zeros((1, K))

            for t in range(1, T):

                e1 = (x[t] - mue) / (6.28 ** 0.5 * sgepe)
                e1 = e1 ** 2
                q = numpy.exp(-e1)
                result = numpy.dot(p[t-1], Pe)
                result = result * q
                result = result / float(result.sum())

                p = numpy.append(p, [result], axis=0)
                drow = []
                rrow = []

                if K == 1 and Pe.size == 1:
                    vec = d[t-1] * Pe * q[0]
                    vec = vec.flatten()
                    if len(vec) == 1:
                        vec = vec[0]
                        
                    r = numpy.append(r, [[1]], axis=0)
                    d = numpy.append(d, [[vec]], axis=0)

                else:
                    for k in range(K):

                        vec = d[t-1] * Pe[:, k].transpose() * q[k]
                        k1 = numpy.argmax(vec)
                        d1 = vec[k1]
                        rrow.append(k1+1) #indexage ala matab +1
                        drow.append(d1)

                    drow = numpy.array(drow)
                    drow = drow / float(drow.sum())

                    r = numpy.append(r, [rrow], axis=0)
                    d = numpy.append(d, [drow], axis=0)

            vec = d[-1]
            k1 = numpy.argmax(vec)
            d1 = vec[k1]

            ze = numpy.array([k1 +1])

            for t in range(T-1, 0, -1):
                add = r[t, int(ze[0] - 1)]
                ze = numpy.append([add], ze)
            be = numpy.where(numpy.diff(ze) != 0)[0]
            be = [-1] + [i for i in be] + [T-1]
            K = len(be) - 1
            if K == 1:
                xe = [x.mean() for i in range(T)]
            else:
                xe = []
                for k in range(0, K):
                    ind0 = be[k] + 1
                    ind1 = be[k+1]

                    xerow = x[ind0:ind1+1].mean()
                    for i in range(ind0, ind1+1):
                        xe.append(xerow)
                xe = numpy.array(xe)
            be = numpy.array(be) + 1

            res = [be, ze, xe, p, d]
            return res


        def ff05b(x, mue, sgepe, Pe):
            """
            % function [be,ze,xe,p,d]=ff05b(x,mue,sgepe,Pe)
            % compute breaks, state, prob
            """


            K = mue.size - 1
            T = x.size
            p = numpy.ones(shape=(1, K)) / float(K)
            d = p
            r = numpy.zeros((1, K))

            for t in range(1, T):

                e1 = (x[t] - mue[:K]) / (6.28 ** 0.5 * sgepe)
                e1 = e1 ** 2
                q = numpy.exp(-e1)
                result = numpy.dot(p[t-1], Pe)
                result = result * q
                result = result / float(result.sum())

                p = numpy.append(p, [result], axis=0)
                drow = []
                rrow = []

                if K == 1 and Pe.size == 1:
                    vec = d[t-1] * Pe * q[0]
                    vec = vec.flatten()
                    if len(vec) == 1:
                        vec = vec[0]
                        
                    r = numpy.append(r, [[1]], axis=0)
                    d = numpy.append(d, [[vec]], axis=0)

                else:
                    for k in range(K):
                        vec = d[t-1] * Pe[:, k].transpose() * q[k]
                        k1 = numpy.argmax(vec)
                        d1 = vec[k1]
                        rrow.append(k1+1) #indexage ala matab +1
                        drow.append(d1)

                    drow = numpy.array(drow)
                    drow = drow / float(drow.sum())

                    r = numpy.append(r, [rrow], axis=0)
                    d = numpy.append(d, [drow], axis=0)

            vec = d[-1]
            k1 = numpy.argmax(vec)
            d1 = vec[k1]

            ze = numpy.array([k1 +1])

            for t in range(T-1, 0, -1):
                add = r[t, int(ze[0] - 1)]
                ze = numpy.append([add], ze)

            be = numpy.where(numpy.diff(ze) != 0)[0]
            be = [-1] + [i for i in be] + [T-1]

            K = len(be) - 1
            if K == 1:
                xe = [x.mean() for i in range(T)]
            else:
                xe = []
                for k in range(0, K):
                    ind0 = be[k] + 1
                    ind1 = be[k+1]

                    xerow = x[ind0:ind1+1].mean()
                    for i in range(ind0, ind1+1):
                        xe.append(xerow)
                xe = numpy.array(xe)
            be = numpy.array(be) + 1

            res = [be, ze, xe, p, d]
            return res

        ######################## Initialize parameters ############################

        T = self.x_obs.size

        xmin = self.x_obs.min()
        xmax = self.x_obs.max()
        x = (self.x_obs-xmin) / (xmax-xmin)

        lambda2 = self.lam                                                 # factor for sgepe
        lambda1 = (1 - lambda2 ** 2) **0.5                                      # factor for sgmue
        KK = self.k0                                                          # initial est of number of states
        N = self.imax                                                         # max number of EM iterations

        sgmue = lambda1 * x.std(ddof=1)                           # initial means distribution sigma
        sgepe = lambda2 * x.std(ddof=1)                           # initial obs'ns distribution sigma


        be = ff02a(T, KK)                             # initialization of breaks
        pp0 = ff06a(be, T)                              # initial est of state escape probability

        ##############################    EM Cycle    #############################

        test1 = 1
        test2 = 0

        while test1:

            beold = be

            ppe = ff06a(be, T)                           # state stay probability
            mue = ff03(self.model, x, be)
            Pe = ff04(self.model, ppe, mue, sgmue)
            [be, ze, xe, p, d] = ff05(self.model, x, mue, sgepe, Pe)

            if be.size == beold.size:
                if (be == beold).all():
                    test1 = 0
            test2 = test2+1

            if test2 > N:
                test1 = 0

        if be.size > 2:
            break_index = be[1:-1] - 1 #pb matlab
            pval = False
        else:
            break_index = None
            pval = True

        res = {'statistic' : xe, 'pvalue' : pval, 'break_index' : break_index}
        return res

class Test_BP_SZ(Test):
    """
    >>> x = example_test_time_serie('bp')['h0']['norm']
    >>> tn = Test_BP_SZ(x, 1000)
    >>> '{0:.5f}'.format(tn.statistic)
    '9.22490'
    >>> tn.break_index
    72

    >>> x = example_test_time_serie('bp')['h0']['pettitt']
    >>> tp = Test_BP_SZ(x, 1000)
    >>> '{0:.5f}'.format(tp.statistic)
    '9.70000'
    >>> tp.break_index
    6

    >>> x = example_test_time_serie('bp')['h0']['gev']
    >>> tg = Test_BP_SZ(x, 1000)
    >>> '{0:.5f}'.format(tg.statistic)
    '12.88440'
    >>> tg.break_index
    84

    >>> all([i.pvalue > 0.5 for i in (tn,tp,tg)])
    True


    >>> x = example_test_time_serie('bp')['h1']['norm']
    >>> tn = Test_BP_SZ(x, 1000)
    >>> '{0:.5f}'.format(tn.statistic)
    '29.04280'
    >>> tn.break_index
    48

    >>> x = example_test_time_serie('bp')['h1']['gev']
    >>> tg = Test_BP_SZ(x, 1000)
    >>> '{0:.5f}'.format(tg.statistic)
    '30.02960'
    >>> tg.break_index
    38

    >>> x = example_test_time_serie('bp')['h1']['pettitt']
    >>> tp = Test_BP_SZ(x, 1000)
    >>> '{0:.5f}'.format(tp.statistic)
    '10.71450'
    >>> tp.break_index
    16

    >>> all([i.pvalue < 0.1 for i in (tn,tp,tg)])
    True
    """
    H0 = "La serie ne persente pas de rupture"
    H1 = "Il y a une rupture dans la serie"
    NAME = "Test de Smadi et Zghoul"
    INFO = "Test utilisant le bootstrap : la statistique utilise est la somme cumulee des ecart a la moyenne"
    TYPE = "Break Point"

    def __init__(self, x_obs, n_boot=10000, alpha_risk=0.05):
        """
        >>> sorted(Test_BP_SZ(numpy.arange(10),10).__dict__.keys())
        ['alpha_risk', 'break_index', 'h0', 'n_boot', 'pvalue', 'statistic', 'x_obs']
        """
        self.x_obs = numpy.array(x_obs, dtype=float)
        assert self.x_obs.size > 1, 'il faut une list de plus de 1 element got : {0}'.format(self.x_obs.size)
        self.n_boot = n_boot

        Test.__init__(self, alpha_risk=alpha_risk)

    def _compute_test(self):
        """
        Calcul de la statistique du test et la pvalue associee
        """
        def cusum_compute(vec):
            """
            Calcul la somme cumule des ecart a la moyenne
            """
            mean = vec.mean()
            center = vec - mean

            res = [vec[0] - mean]
            for i in range(1, vec.size):
                res.append(res[i-1] + center[i])
            res = numpy.array([0] + res)
            return res

        n = self.x_obs.size
        #~ t = numpy.arange(n) #verifier
        #~ mean= self.x_obs.mean()
        cusum = cusum_compute(self.x_obs)

        abs_cu = numpy.abs(cusum)
        i_shift = numpy.argmax(abs_cu) - 1
        s_diff = cusum.max() - cusum.min()

        s_diff_boot = []
        for i in range(self.n_boot):
            rand = random.sample(range(n), n)
            boot_sample = self.x_obs[rand]
            boot_cusum = cusum_compute(boot_sample)
            add = boot_cusum.max() - boot_cusum.min()
            s_diff_boot.append(add)
        s_diff_boot = numpy.array(s_diff_boot)

        n_boot_sup = numpy.where(s_diff_boot > s_diff)[0].size
        pvalue = n_boot_sup / float(self.n_boot)

        res = {"pvalue" : pvalue, "statistic" : s_diff, 'break_index':i_shift}

        return res

class Test_BP_Lombard(Test):
    """
    >>> x = example_test_time_serie('bp')['h0']['norm']
    >>> tn = Test_BP_Lombard(x)
    >>> '{0:.5f}'.format(tn.statistic)
    '0.00052'
    >>> tn.break_index == None
    True

    >>> x = example_test_time_serie('bp')['h0']['pettitt']
    >>> tp = Test_BP_Lombard(x)
    >>> '{0:.5f}'.format(tp.statistic)
    '0.03567'
    >>> tp.break_index== None
    True

    >>> x = example_test_time_serie('bp')['h0']['gev']
    >>> tg = Test_BP_Lombard(x)
    >>> '{0:.5f}'.format(tg.statistic)
    '0.00180'
    >>> tg.break_index== None
    True

    >>> x = example_test_time_serie('bp')['h1']['norm']
    >>> tn = Test_BP_Lombard(x)
    >>> '{0:.5f}'.format(tn.statistic)
    '0.17380'
    >>> tn.break_index
    [48, 49]

    >>> x = example_test_time_serie('bp')['h1']['gev']
    >>> tg = Test_BP_Lombard(x)
    >>> '{0:.5f}'.format(tg.statistic)
    '0.10627'
    >>> tg.break_index
    [48, 49]

    >>> x = example_test_time_serie('bp')['h1']['pettitt']
    >>> tp = Test_BP_Lombard(x)
    >>> '{0:.5f}'.format(tp.statistic)
    '0.06868'
    >>> tp.break_index
    [16, 17]

    >>> x=example_test_time_serie('bpvar')['h1']['norm']
    >>> tbpvar = Test_BP_Lombard(x)
    >>> '{0:.5f}'.format(tbpvar.statistic)
    '0.00029'
    >>> tbpvar.break_index == None
    True
    >>> tbpvar = Test_BP_Lombard(x,'var')
    >>> '{0:.5f}'.format(tbpvar.statistic)
    '0.04296'
    >>> tbpvar.break_index
    [48, 49]
    """
    H0 = "La serie ne persente pas de rupture"
    H1 = "Il y a une rupture dans la serie"
    NAME = "Test de Lombard"
    INFO = "Test permettant de detecter des rupture non abrupte, assez generaliste"
    TYPE = "Break Point"

    def __init__(self, x_obs, method='mean', alpha_risk=0.05):
        """
        >>> sorted(Test_BP_Lombard(numpy.arange(10)).__dict__.keys())
        ['alpha_risk', 'break_index', 'h0', 'method', 'pvalue', 'statistic', 'x_obs']
        """
        self.x_obs = numpy.array(x_obs, dtype=float)
        assert self.x_obs.size > 1, 'il faut une list de plus de 1 element got : {0}'.format(self.x_obs.size)
        self.method = method

        Test.__init__(self, alpha_risk=alpha_risk)

    def _compute_test(self):
        """
        Calcul de la statistique du test et la pvalue associee
        """
        size = self.x_obs.size
        #~ sort_id = numpy.argsort(self.x_obs)
        #~ ranks = numpy.argsort(sort_id) #verifier

        phi = self._score()
        phim = phi.mean()
        phiet = phi.std(ddof=1)

        phi_std = (phi - phim) / phiet
        m1 = phi_std.cumsum()
        l = numpy.zeros(shape=(size, size))

        for t1 in range(size -1):
            for t2 in range(t1, size):
                ind = numpy.arange(t1+1, t2+1)
                l[t1, t2] = m1[ind].sum()

        l2 = l ** 2
        vn = l2.sum()

        tn = vn / (float(size)  ** 5)

        mat = numpy.zeros(shape=(size, size)) #KD cree la matrice pour determiner t1 et t2

        for x in range(size -1):
            for y in range(x, size):
                u = (x + 1)/ float(size)
                v = (y + 1)/ float(size)
                sig2 = (((1-u) ** 3 * (1 + 3*u))/12.) - ((1-v)**3 * (1 + 3*v))/12. - (((1-v)**2) * (v**2 - u**2))/2.
                sig = sig2 ** 0.5
                val = abs(l[x, y]) / sig
                val = numpy.nan_to_num(val)
                mat[x, y] = val

        vc = [0., 0.0287, 0.0403, 0.0690, 0.1, numpy.inf]
        alphas = [1., 0.1, 0.05, 0.01, 0., 0.]
        pval = scipy.interpolate.interp1d(vc, alphas)(tn)

        if pval < self.alpha_risk:
            idmax = numpy.argmax(mat, axis=0)
            maxs = [mat[y, x] for x, y in enumerate(idmax)]
            t2 = numpy.argmax(maxs)
            #~ maxmax= maxs[t2]
            t1 = idmax[t2] + 1
            t2 += 1

            breaks = [t1, t2]

        else:
            breaks = None

        res = {"pvalue"  :   float(pval), "statistic"     :   tn, 'break_index':breaks}

        return res

    def _score(self):
        """
        Calcul le score pour la moyenne ou la variance
        """
        n = self.x_obs.size
        sort_id = numpy.argsort(self.x_obs)
        ranks = numpy.argsort(sort_id)

        res = 2 * ((ranks +1) / (n+1.)) - 1
        if self.method == 'mean': # Score de Wilcoxon
            pass
        elif self.method == 'var': # Score de Mood
            res = res ** 2
        else:
            assert 0, 'pb code source'
        return res

##====================================================================================================================##
##                              TEST TREND / CORRELATION                                                              ##
##====================================================================================================================##

class Test_Correlation(Test):
    """
    >>> ds = ('norm','gum','gev')
    >>> tests = [Test_Correlation(example_test_time_serie('trend')['h0'][i],typ="spearman") for i in ds]
    >>> all([i.h0 for i in tests])
    True
    >>> ["{0:.2f}".format(i.pvalue) for i in tests]
    ['0.99', '0.76', '0.63']
    >>> tests = [Test_Correlation(example_test_time_serie('trend')['h1'][i],typ="spearman") for i in ds]
    >>> all([not i.h0 for i in tests])
    True
    >>> ["{0:.1g}".format(i.pvalue) for i in tests]
    ['2e-51', '3e-40', '5e-35']
    
    >>> tests = [Test_Correlation(example_test_time_serie('trend')['h0'][i],typ="pearson") for i in ds]
    >>> all([i.h0 for i in tests])
    True
    >>> ["{0:.5f}".format(i.pvalue) for i in tests]
    ['0.99140', '0.84342', '0.36540']
    >>> tests = [Test_Correlation(example_test_time_serie('trend')['h1'][i],typ="pearson") for i in ds]
    >>> all([not i.h0 for i in tests])
    True
    >>> ["{0:.1g}".format(i.pvalue) for i in tests]
    ['7e-50', '4e-40', '1e-29']
    
    >>> tests = [Test_Correlation(example_test_time_serie('trend')['h0'][i], typ = 'kendall') for i in ds]
    >>> all([i.h0 for i in tests])
    True
    >>> ["{0:.2f}".format(i.pvalue) for i in tests]
    ['0.96', '0.73', '0.62']
    >>> tests = [Test_Correlation(example_test_time_serie('trend')['h1'][i], typ = 'kendall') for i in ds]
    >>> all([not i.h0 for i in tests])
    True
    >>> ["{0:.1g}".format(i.pvalue) for i in tests]
    ['9e-32', '1e-27', '3e-27']

    """
    H0 = "Pas de correlation (ou tendance si pas de y)"
    H1 = "Il y a une correlatin (ou tendance)"
    NAME = "Coreelation test : can be spearman, pearson, or mann-kendall"
    INFO = "Test parametrique"
    TYPE = 'Correlation'

    def __init__(self, x_obs, y_obs=None, typ="spearman", alpha_risk=0.05):
        """
        >>> sorted(Test_Correlation(numpy.arange(10)).__dict__.keys())
        ['alpha_risk', 'h0', 'pvalue', 'statistic', 'typ', 'x_obs', 'y_obs']
        """
        self.x_obs = numpy.array(x_obs, dtype=float)
        assert self.x_obs.size > 1, 'il faut une list de plus de 1 element got : {0}'.format(self.x_obs.size)
        self.y_obs = self._return_y(y_obs)
        self.typ = typ

        Test.__init__(self, alpha_risk=alpha_risk)

    def _return_y(self, y):
        """
        >>> Test_Correlation(list(range(10))).y_obs
        array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
        """
        if y is None:
            res = numpy.arange(self.x_obs.size)
        else:
            res = numpy.array(y, dtype=float)
            assert res.size == self.x_obs.size, "uncoherent dim"

        return res
        
        
    def _compute_test(self):
        """
        Calcul de la statistique du test et la pvalue associee
        """
        fun = corfun(self.typ)
        test = fun(self.x_obs, self.y_obs)
        statistic = test[0]
        pvalue = test[1]
        res = {"pvalue" : pvalue, "statistic" : statistic}
        return res




##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
