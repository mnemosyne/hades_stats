"""
Module contenant des clases statistiques basiques (Regression lineaire simple et multiple) et des Score
"""

##======================================================================================================================##
##                                PACKAGES IMPORT                                                                       ##
##======================================================================================================================##


#packages python
import argparse
import numpy
import scipy.stats
import scipy.integrate
import scipy.fftpack
from matplotlib import pyplot

##====================================================================================================================##
##                              functions                                                                             ##
##====================================================================================================================##

def curve_2lr(x, y, bp):
    """
    
    >>> x = numpy.arange(-10,10.1, 0.5)
    >>> y = x ** 2
    
    >>> c2lr = curve_2lr(x, y, 20)
    >>> lr1, lr2 = [c2lr[i] for i in ['lr1', 'lr2']]
    >>> list(set(lr1.x - lr2.x))
    [-10.0]
    >>> abs(lr1.gradient + lr2.gradient) < 1e-8
    True
    >>> abs(lr1.intercept - lr2.intercept) < 1e-8
    True
    >>> fig = pyplot.figure()
    >>> for i in (lr1, lr2): l= i.plot()
    >>> l = pyplot.plot(x,y, ls = '', marker = 'x')
    >>> fig.show()
    """
    assert len(x) == len(y), "pb incoherant dim"
    assert 2 < bp < len(x) - 2, "pb bp"
    
    x1 = x[:bp+1]
    y1 = y[:bp+1]
    x2 = x[bp:]
    y2 = y[bp:]
    
    xbp = x[bp]
    ybp = y[bp]
    
    assert xbp in x1 and xbp in x2 and ybp in y1 and ybp in y1, "pb code"
            
    lr1 = FastLinearRegression(x1, y1)
    lr2 = FastLinearRegression(x2, y2)
    
    sq_resi = sum([ilr.residuals(square=True).sum() for ilr in (lr1, lr2)])
    
            
    res = {"lr1" : lr1, "lr2" : lr2, 'xbp' : xbp, 'ybp': ybp, 'square_residuals' : sq_resi}
    
    return res
    
def segment_curve_2lr(x, y, min_size=5):
    """
    Function doc

    PARAM   :    DESCRIPTION
    RETURN  :    DESCRIPTION
    
    >>> x = numpy.arange(-20,21)
    >>> y = x ** 2
    >>> b2lr = segment_curve_2lr(x,y,5)
    >>> b2lr["xbp"]
    0
    >>> b2lr["ybp"]
    0
    """
    assert len(x) == len(y), "pb incoherant dim"
    
    
    bpstart = min_size - 1
    bpend = len(x) - min_size
    assert bpend > bpstart + 1, "too small vec"
    
    bps = numpy.arange(bpstart, bpend + 1, 1, dtype=int)
    
    c2lrs = [curve_2lr(x=x, y=y, bp=i_bp) for i_bp in bps]
    
    for i_lr in c2lrs:
        assert i_lr["lr1"].x.size >= min_size and i_lr["lr2"].x.size >= min_size, "pb code min size"
    
    sq_res = [ilr['square_residuals'] for ilr in c2lrs]
    indmin = numpy.argmin(sq_res)
    #~ print(indmin)
    
    res = c2lrs[indmin]
    #~ print(res)
    
    assert res["square_residuals"] == min(sq_res), f"pb code {res['square_residuals']} vs {min(sq_res)}"
    
    return res
            

##====================================================================================================================##
##                        creation des classes                                                                        ##
##====================================================================================================================##

class FastLinearRegression:
    """ Class doc
    >>> x = [5.05, 6.75, 3.21, 2.66]
    >>> y = [1.65, 26.5, -5.93, 7.96]
    >>> flr = FastLinearRegression(x, y)
    >>> round(flr['gradient'], 3)
    5.394
    >>> round(flr['intercept'], 2)
    -16.28
    >>> round(flr['rsquared'], 4)
    0.5248
    >>> print(flr)
    LINEAR REGRESSION
     intercept   :   -16.3
      gradient   :   5.39
       r_value   :   0.724
      rsquared   :   0.525

    >>> [f"{i:.3g}" for i in flr(numpy.arange(10))]
    ['-16.3', '-10.9', '-5.49', '-0.1', '5.29', '10.7', '16.1', '21.5', '26.9', '32.3']
    >>> rndm_dics = {i:{i : numpy.random.random(10) for i in ('x', 'y')} for i in range(10)}
    >>> sl_dics = {k:{'x': d['x'], 'y' : d['y'] + d['x'] *  2 + 1} for k, d in rndm_dics.items()}
    >>> d = sl_dics[0]
    >>> lr = FastLinearRegression(d['x'],d['y'])
    """
    
    def __init__(self, x, y):
        """ Class initialiser """
        self.x = x
        self.y = y
        
        gradient, intercept, r_value, p_value, std_err = scipy.stats.linregress(x, y) #scipy.stats.mstats
        
        self.gradient = gradient
        self.intercept = intercept
        self.r_value = r_value
        self.p_value = p_value
        self.std_err = std_err
        
        self.rsquared = r_value ** 2
            
    def __str__(self):
        """
        Print(obj)
        """
        res = ["Linear Regression".upper()]
        for k in ('intercept', 'gradient', 'r_value', 'rsquared'):
            text = f"{k:>10}   :   {self[k]:.3g}"
            res.append(text)
        
        res = "\n".join(res)
        
        return res
            
    def __getitem__(self, key):
        """
        Get item with []
        """
        res = self.__dict__[key]
        return res
    
    def __call__(self, new_x):
        """
        Predict new value from regression, see example in main doctest
        """
        res = self.gradient * new_x + self.intercept
        return res
            
    def residuals(self, square=False):
        """
        Residual of the regression
        """
        yt = self(self.x)
        res = self.y - yt
        
        if square:
            res = res ** 2
        
        return res
            
    def plot(self, x=None, **kwarg):
        """
        Plot the regression
        >>> x = [5.05, 6.75, 3.21, 2.66]
        >>> y = [1.65, 26.5, -5.93, 7.96]
        >>> flr = FastLinearRegression(x, y)
        >>> f=pyplot.figure()
        >>> l=flr.plot()
        >>> f.show()
        """
        argdic = dict(marker="None", ls="-", color="k")
        argdic.update(kwarg)
        
        if x is None:
            x = numpy.array([min(self.x), max(self.x)])
        
        y = self(x)
        res = pyplot.plot(x, y, **argdic)[0]
        return res
        
    def label(self, fmt=".1f"):
        """
        Automatic Label
        >>> x = [5.05, 6.75, 3.21, 2.66]
        >>> y = [1.65, 26.5, -5.93, 7.96]
        >>> flr = FastLinearRegression(x, y)
        >>> flr.label()
        'Y=5.4 X + -16.3 | r=0.7 pval=0.3'
        """
        res = f"Y={self.gradient:{fmt}} X + {self.intercept:{fmt}} | r={self.r_value:{fmt}} pval={self.p_value:{fmt}}"
        return res
            
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--example', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        import doctest
        test_result = doctest.testmod()
        print(test_result)


    #+++++++++++++++++++++++++++++++#
    #    EXAMPLES                   #
    #+++++++++++++++++++++++++++++++#
    

    if opts.example:

        x_var = numpy.random.normal(0, 1, 50)
        y_var = numpy.random.normal(0, 1, 50)
        lr = FastLinearRegression(x_var, y_var)
