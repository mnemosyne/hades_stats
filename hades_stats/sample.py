"""
Simple methods analysing data series
"""


##======================================================================================================================##
##                PACKAGE                                                                                               ##
##======================================================================================================================##

import argparse
import math
import datetime
import random
from collections.abc import Iterable
from numbers import Number
from types import BuiltinFunctionType, MethodType, FunctionType

import numpy
import scipy
import pandas
import lmoments3
import matplotlib
from matplotlib import pyplot

from sklearn.neighbors import KernelDensity

from hades_stats.utils import q_mn_mx_for_ci

##======================================================================================================================##
##                CST                                                                                                   ##
##======================================================================================================================##

EST_KEY = "est"
CI_MN_KY = "ci_min"
CI_MX_KY = "ci_max"

VAL_KEY = "value"
MN_BD_KY = "mn_bd"
MX_BD_KY = "mx_bd"

#~ from functions import todo_msg
#~ print(todo_msg("boot and montecarlo useless : use Series.sample method", __file__))

#~ from functions import todo_msg
#~ print(todo_msg("deal with na values and empirical ranks and stats", __file__))

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

def defmkplot(**kwargs):
    """
    Default plot kwargs
    """
    # ~ res = dict(marker='o', mew=1., ls="", mfc="0.5", mec="k")
    res = dict(marker='o', mew=1., ls="", mec="k")
    res.update(kwargs)
    return res

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def example_sample(n=100):
    """
    Renvoie un objet de type Sample
    >>> ts = example_sample()
    >>> ts.__class__ == Sample
    True
    """
    values = scipy.stats.norm().rvs(n)
    res = Sample(values)
    return res

def minsize_to_compute_quantile(q, coef=2):
    """
    Return the minimal size required to compute quantile without extrapolation
    >>> minsize_to_compute_quantile(0.5,1)
    2
    >>> minsize_to_compute_quantile(0.9,1)
    10
    >>> minsize_to_compute_quantile(0.99,1)
    100
    >>> minsize_to_compute_quantile(0.995,1)
    200
    """
    assert 0. < q < 1., "Needed : 0 < q < 1, got : q = {0}".format(q)
    assert coef >= 1
    dmedian = abs(0.5 - q)
    d1 = 0.5 - dmedian
    res = 1 / d1
    
    #multiply by coef 
    res = res * coef
    res = round(res)
    res = int(res)
    
    return res

def _frequency(allranks, a):
    """Compute frequency given ranks. Used in Sample, see the corresponding doctest"""
    assert isinstance(a, (int, float))
    b = 1. - 2. * a
    res = (allranks - a) / (allranks.size + b)
    return res
    
    
def _sf(freqs):
    """
    Survival frequency
    >>> _sf(0.6)
    0.4
    """
    res = 1 - freqs
    return res
    
def _neg_ln_sf(sfs):
    """
    New log survival function
    >>> round(_neg_ln_sf(0.9), 3)
    0.105
    >>> round(_neg_ln_sf(0.1), 4)
    2.3026
    """
    res = - numpy.log(sfs)
    return res

##======================================================================================================================##
##                SAMPLE CLASS                                                                                          ##
##======================================================================================================================##

class Sample(pandas.Series):
    """
    Some additional graph and statistics for pandas.Series
    
        >>> x = [scipy.stats.norm(loc=10, scale=1).ppf(i) for i in numpy.arange(0.01, 1, 0.01)]
    >>> test_sample = Sample(x)
    >>> f = pyplot.figure()
    >>> l=test_sample.pdf_plot()
    >>> f.show()
    >>> f = pyplot.figure()
    >>> l=test_sample.cdf_plot()
    >>> y = [scipy.stats.genextreme(0, loc=10, scale=1).ppf(i) for i in numpy.arange(0.01, 1, 0.01)]
    >>> f=pyplot.figure()
    >>> l=Sample(y).return_level_plot(npy = 1, a=0.)

    >>> x = numpy.array([scipy.stats.norm(loc=10, scale=1).ppf(i) for i in numpy.arange(0.005, 1, 0.01)], dtype=float)
    >>> s =Sample(x)
    >>> round(float(s.get_statistic('mean')), 1)
    10.0
    >>> round(float(s.get_statistic('median')), 1)
    10.0
    >>> s.get_statistic('size')
    100
    >>> (Sample(x).cum_frequency(a=0.5)[[0, -1]] == numpy.array([0.005, 0.995])).all()
    True
    >>> x = numpy.array([scipy.stats.norm(loc=10, scale=1).ppf(i) for i in numpy.arange(0.01, 1, 0.01)])
    >>> (Sample(x).cum_frequency(a=0.)[[0, -1]] == numpy.array([0.01, 0.99])).all()
    True

    >>> y=numpy.array([scipy.stats.genextreme(0,loc=10,scale=1).ppf(i) for i in numpy.arange(0.005, 1, 0.01)],dtype=float)
    >>> (Sample(y).return_periods(npy = 1, a=0.)[[0, -1]].round(3) == numpy.array([  1.01, 101.]).round(3)).all()
    True
    >>> (Sample(y).return_periods(npy = 2, a=0.)[[0, -1]].round(3) == numpy.array([ 0.505, 50.5]).round(3)).all()
    True
    >>> round(Sample(y).return_periods(a=0.5, npy = 1)[-1],1)
    200.0
    >>> (Sample(y).return_events(a=0)[[0, -1]].round(3) == numpy.array([  1.01, 101.  ]).round(3)).all()
    True
    >>> round(float(Sample(y).mean()), 2)
    10.57
    >>> print(round(float(Sample(y).var()), 1))
    1.6
    
    >>> (Sample([1, 5, 2, 3]).sort_values(ascending=True, inplace=False) == numpy.array([1, 2, 3, 5])).all()
    True

    >>> (numpy.array(Sample([1, 5, 2, 3]).return_events()).round(2) == numpy.array([1.25      , 1.66666667, 2.5       , 5.        ]).round(2)).all()
    True
    >>> (numpy.array(Sample([1, 5, 2, 3]).return_periods(npy = 1)).round(2) == numpy.array([1.25      , 1.66666667, 2.5       , 5.        ]).round(2)).all()
    True
    >>> (numpy.array(Sample([1, 5, 2, 3]).return_periods(npy = 2)).round(2) == numpy.array([0.625     , 0.83333333, 1.25      , 2.5       ]).round(2)).all()
    True
    
    >>> numpy.array(Sample([1, 5, 2, 3]).centred_values())
    array([-1.75,  2.25, -0.75,  0.25])
    >>> ["{0:.2f}".format(i) for i in Sample([1, 5, 2, 3]).std_values()]
    ['-1.02', '1.32', '-0.44', '0.15']
    >>> ["{0:.2f}".format(i) for i in Sample([1, 5, 2, 3]).norm_values()]
    ['0.36', '1.82', '0.73', '1.09']
    
    >>> all(Sample([1, 5, 2, 3]).cum_rank() == numpy.array([1, 2, 3, 4]))
    True
    >>> all(Sample([1, 5, 2, 3]).rank() == numpy.array([1, 4, 2, 3]))
    True
    >>> Sample([1, 5, 2, 3, 5]).rank()
    0    1.0
    1    4.5
    2    2.0
    3    3.0
    4    4.5
    dtype: float64

    >>> all(Sample([1, 5, 2, 3, 4]).rank() == numpy.array([1, 5, 2, 3, 4]))
    True
    >>> all(Sample([2, 1, 5, 3, 4]).rank() == numpy.array([2, 1, 5, 3, 4]))
    True

    >>> all(numpy.array(Sample([1, 5, 2, 3]).cum_frequency()) == numpy.array([ 0.2,  0.4,  0.6,  0.8]))
    True
    >>> all(numpy.array(Sample([1, 5, 2, 3]).emp_frequency()) == numpy.array([ 0.2,  0.8,  0.4,  0.6]))
    True
   
    
    
    
    >>> n = 4
    >>> values = numpy.arange(n, dtype =float)
    >>> values[0] = numpy.nan
    >>> values[-1] = numpy.nan
    >>> s = Sample(values)
    >>> s
    1    1.0
    2    2.0
    dtype: float64

    >>> s = s.dropna()
    >>> s
    1    1.0
    2    2.0
    dtype: float64
    >>> Sample(s)
    1    1.0
    2    2.0
    dtype: float64
    """
    def __init__(self, data, **kwarg):
        """ Class initialiser """
        newdata = pandas.Series(data=data, **kwarg).dropna()
        pandas.Series.__init__(self, data=newdata)

    def cv(self):
        """Coef of variation"""
        res = self.std(ddof=1) / abs(self.mean())
        return res
        
    def centred_values(self):
        """Centred value, see main"""
        res = self - self.mean()
        return res
        
    def std_values(self):
        """Standardized values, see main"""
        res = self.centred_values() / self.std(ddof=1)
        return res
        
    def norm_values(self):
        """Normalized values, see main"""
        res = self / self.mean()
        return res
        
    def cum_rank(self):
        """Cumulative rank of values"""
        res = numpy.arange(self.size)
        res = res + 1
        return res
        
    def cum_frequency(self, a=0):
        """Cummlative frequency
        >>> (Sample(numpy.arange(3)).cum_frequency() == numpy.array([0.25, 0.5 , 0.75])).all()
        True
        >>> (Sample(numpy.arange(3)).cum_frequency(a=1) == numpy.array([0. , 0.5, 1. ])).all()
        True
        """
        res = _frequency(allranks=self.cum_rank(), a=a)
        return res
        
    def return_events(self, **kwarg):
        """Compute the empirical return time of events"""
        res = 1./(1 - self.cum_frequency(**kwarg))
        return res
        
    def return_periods(self, npy, **kwarg):
        """Compute the empirical return periods"""
        assert isinstance(npy, (float, int))
        res = self.return_events(**kwarg) / npy
        return res
        
    def emp_frequency(self, a=0):
        """Empirical frequency of values"""
        res = _frequency(allranks=self.rank(), a=a)
        return res
        
    def emp_cum_sf(self, **kwarg):
        """Survival Function """
        frs = self.cum_frequency(**kwarg)
        res = _sf(freqs=frs)
        return res
        
    def emp_ln_cum_sf(self, **kwarg):
        """Log of survival function"""
        sfs = self.emp_cum_sf(**kwarg)
        res = _neg_ln_sf(sfs=sfs)
        return res

    def pdf(self, q1, q2, condition="g-leq", rank=False, approx=False, fmt=float):
        """
        Return the probability of being between two values        
        >>> x = numpy.arange(10)
        >>> s = Sample(x)
        >>> float(s.pdf(-1, 10))
        1.0
        >>> float(s.pdf(-1, 0))
        0.1
        >>> float(s.pdf(-1, 1))
        0.2
        >>> float( s.pdf(6.9, 9))
        0.3
        >>> s.pdf(6.9, 9, fmt = 'df')
           mn_bd  mx_bd  value
        0    6.9      9    0.3
        """
        if approx:
            print("warn : no recomended method")
            assert condition == "g-leq", "non consistent input"
            res = self.cdf(q2) - self.cdf(q1)
            if rank:
                res = res * self.size
        
        else:
            if q2 < self.min() or q1 > self.max():
                res = 0
            else:
                if condition == "g-leq":
                    cond = (q1 < self.values) & (self.values <= q2)
                elif condition == "geq-l":
                    cond = (q1 <= self.values) & (self.values < q2)
                else:
                    raise ValueError("please chose : geq-l or g-leq")

                res = cond.sum()
            
            if not rank:
                res = res / self.size
                        
        if fmt == float:
            pass
        elif fmt in ('df', 'dataframe'):
            data = [q1, q2, res]
            cols = [MN_BD_KY, MX_BD_KY, VAL_KEY]
            res = pandas.DataFrame([data], columns=cols)
        else:
            raise ValueError('{0} is not implemented'.format(fmt))
                        
        return res
            
    def pdfs(self, classes=None, n_center=None, equi_intervals=True, condition="g-leq", rank=False):
        """
        >>> s = Sample(numpy.arange(10))
        >>> s.pdfs([2,8])
             mn_bd    mx_bd  value
        1 -0.00009  2.00000    0.3
        2  2.00000  8.00000    0.6
        3  8.00000  9.00009    0.1
        """
        delta = self.max() - self.min()
        mnval = self.min() - 1e-5 * delta
        mxval = self.max() + 1e-5 * delta

        if n_center is None and classes is None:
            n_center = int(self.size ** 0.5)

        if n_center is None:
            assert classes is not None, "inconsistent input, n_center and classes == None"
        else:
            assert isinstance(n_center, int), 'pb entree demande {0} got {1.__class__} : {1}'.format("int", n_center)
            
            if equi_intervals:
                classes = numpy.linspace(mnval, mxval, num=n_center+1, endpoint=True)
                classes = classes[1:-1]
            else:
                limrank = numpy.linspace(0, self.size - 1, n_center + 1, endpoint=True)
                sorvals = self.sort_values(ascending=True, inplace=False)
                classes = numpy.array([numpy.mean([sorvals[i-1], sorvals[i]]) for i in limrank[1:-1]])
        
        if all([isinstance(i, (tuple, list)) for i in classes]):
            assert [len(i) == 2 for i in classes], 'Pb input : {0}'.format(classes)
            assert [i[0] < i[1] for i in classes], 'Pb input : {0}'.format(classes)
            assert classes[0][0] <= self.min(), 'Pb input : {0}'.format(classes)
            assert classes[-1][1] >= self.max(), 'Pb input : {0}'.format(classes)
            
            minvals = [i[0] for i in classes]       
            maxvals = [i[-1] for i in classes]
            
            for mn, mx in zip(minvals[1:], maxvals[:-1]):
                assert [mn >= mx], "overlaps\n{0}vs{1}\n{2}\n{3}".format(mn, mx, minvals, maxvals)
        
        else:
            classes = numpy.array(classes, dtype=float)
            classes = numpy.sort(classes)
            classes = list(classes)
            minvals = [mnval] + classes
            maxvals = classes + [mxval]

        res = []
        idx = 0
        #~ bottom = 0
        for minval, maxval in zip(minvals, maxvals):
            idx += 1
            dfpdf = self.pdf(q1=minval, q2=maxval, condition=condition, rank=rank, fmt='df')
            dfpdf.index = [idx]
            res.append(dfpdf)
        
        res = pandas.concat(res)
        
        cumsum = res[VAL_KEY].sum()
        if rank:
            assert cumsum == self.size, "pb cumsum == {0}".format(cumsum)
        else:
            assert abs(cumsum - 1.) < 1e-8, "pb cumsum == {0}".format(cumsum)
            
        return res

    def cdf(self, q, rank=False, **kwarg):
        """
        Renvoie la probalite de non depassement du quantile passe en entree
        >>> s=Sample(numpy.arange(101))
        >>> s.cdf(50)
        0.5
        >>> s.cdf(50,1, a=0.5)
        50.5
        >>> s.cdf(5, a=1)
        0.05
        >>> s.cdf(1, a=1)
        0.01
        >>> s.cdf(95, a=1)
        0.95
        >>> s.cdf(99, a=1)
        0.99
        >>> s.cdf(200, a=1)
        1.0
        >>> s.cdf(-2, a=1)
        0.0
        """
        xp = self.sort_values(ascending=True, inplace=False)
        fp = self.cum_frequency(**kwarg) 

        if rank:
            fp = fp * self.size
        
        res = numpy.interp(q, xp=xp, fp=fp, left=0., right=1.)
        return res
            
    def sf(self, q, **kwarg):
        """
        Survival Function of the quantile given
        >>> s=Sample(numpy.arange(101))
        >>> s.sf(50,a=1.)
        0.5
        >>> s.sf(5,a=1.)
        0.95
        >>> s.sf(1,a=1.)
        0.99
        >>> s.sf(200,a=1.)
        0.0
        >>> s.sf(-2,a=1.)
        1.0
        """
        frs = self.cdf(q=q, rank=False, **kwarg)
        res = _sf(frs)
        return res
        
    def ln_sf(self, q, **kwarg):
        """Log of survaval function for the given value"""
        sfs = self.sf(q=q, **kwarg)
        res = _neg_ln_sf(sfs=sfs)
        return res
        
  
    def quantile(self, f, **kwarg):
        """
        Return the given quantile of the frequency given
        >>> s=Sample(numpy.arange(101))
        >>> s.quantile(0.50,a=1.)
        50.0
        >>> s.quantile(0.05,a=1.)
        5.0
        >>> s.quantile(0.01,a=1.)
        1.0
        >>> s.quantile(0.95,a=1.)
        95.0
        >>> s.quantile(0.99,a=1.)
        99.0
        >>> s.quantile(1)
        100.0
        >>> (s.quantile(numpy.array([0.5,0.6]),a=1.) == numpy.array([50., 60.])).all()
        True
        """
        xp = self.cum_frequency(**kwarg)
        fp = self.sort_values(ascending=True, inplace=False)
        
        mn = self.min()
        mx = self.max()
        
        res = numpy.interp(f, xp=xp, fp=fp, left=mn, right=mx)
        
        return res

    def ppf(self, prob, **kwarg):
        """quantile estimate"""
        res = self.quantile(f=prob, **kwarg)
        return res
        
    def av_years(self):
        """
        av years inn the series
        >>> dates = pandas.date_range('2000-01-01', '2010-01-01', freq='M')
        >>> x = numpy.arange(dates.size)
        >>> Sample(x).av_years() is None
        True
        >>> Sample(x, index=dates).av_years()
        array([2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009])
        """
        if isinstance(self.index, pandas.DatetimeIndex):   #, "must be a datetime index to compute the numbre of years, got {0}".format(type(self.index))
            res = numpy.array(sorted(set(self.index.year)), dtype=int)
        else:
            res = None
        return res
        
    def nyears(self):
        """
        Number of years in the series
        >>> dates = pandas.date_range('2000-01-01', '2010-01-01', freq='M')
        >>> x = numpy.arange(dates.size)
        >>> Sample(x).nyears()
        nan
        >>> Sample(x, index=dates).nyears()
        10
        """
        avyrs = self.av_years()
        if avyrs is None:
            res = numpy.nan
        else:
            res = avyrs.size
        return res
    
    def npy(self):
        """
        Number of observation per year
        >>> dates = pandas.date_range('2000-01-01', '2010-01-01', freq='M')
        >>> x = numpy.arange(dates.size)
        >>> Sample(x, index=dates).npy()
        12.0
        >>> dates = pandas.date_range('2000-01-01', '2010-01-01', freq='a')
        >>> x = numpy.arange(dates.size)
        >>> Sample(x, index=dates).npy()
        1.0
        """
        res = self.size / self.nyears()        
        return res
        
    def return_level(self, t, npy=None, **kwarg):
        """Return the return level of the given return period or return event
        >>> s=Sample(numpy.arange(101))
        >>> s.return_level(2)
        50.0
        >>> s.return_level(10,a=1)
        90.0
        """
        t = numpy.array(t, dtype=float)

        if npy is not None:
            assert isinstance(npy, (int, float)), "faut n obs par ans pr passer de n evt de retour a periode de retour"
            t = t * npy

        cdf = 1 - 1 / t
        res = self.ppf(cdf, **kwarg)
        return res

    def return_event(self, rl, **kwarg):
        """ Return 
        >>> s=Sample(numpy.arange(101))
        >>> s.return_event(50)
        2.0
        >>> s.return_event(60, a=1)
        2.5
        """
        rl = numpy.array(rl, dtype=float)
        cdf = self.cdf(rl, **kwarg)
        res = 1 / (1-cdf)
        return res

    def return_period(self, rl, npy, **kwarg):
        """ 
        >>> s=Sample(numpy.arange(101))
        >>> s.return_period(50,1)
        2.0
        >>> s.return_period(50,1, a=1)
        2.0
        >>> s.return_period(50,2)
        1.0
        """
        assert isinstance(npy, (int, float)), "npy is required"
        res = self.return_event(rl, **kwarg) / npy
        return res
        
    
    def kde(self, npred = 1000, **kwargs):
        """
        Compute kernel density function
        """
        
        x = self.values
        minx = x.min()
        maxx = x.max()
        xdif = maxx - minx
        lim0 = minx - xdif / 10
        lim1 = maxx + xdif / 10
        print(xdif)
        
        x_pred = numpy.linspace(lim0, lim1, npred)
        
        assert x_pred.min() < minx and x_pred.max() > maxx, 'pb code'
        
        x = x.reshape(-1,1)
        x_pred = x_pred.reshape(-1,1)
        
        
        dicargs = dict(kernel="gaussian", bandwidth=xdif/20)
        # ~ print(dicargs)
        dicargs.update(kwargs)
        # ~ print(dicargs)
        kde = KernelDensity(**dicargs).fit(x)

        log_dens = kde.score_samples(x_pred)
        dens = numpy.exp(log_dens)
        
        dens = dens.reshape(-1)
        x_pred = x_pred.reshape(-1)
        
        # ~ print(dens)
        # ~ print(x_pred)
        
        res = pandas.Series(dens, index=x_pred)
        
        # ~ print(res)
        return res
    


    def cdf_plot(self, a=0, rank=False, log_axis=0, **kwarg):
        """Graph la fonction de repartition empirique de l'echntillon
        >>> f = pyplot.figure()
        >>> x = [scipy.stats.norm(loc=10, scale=1).ppf(i) for i in numpy.arange(0.01, 1, 0.01)]
        >>> test_sample = Sample(x)
        >>> l= test_sample.cdf_plot()
        >>> l.set_label('r')
        >>> f.show()
        """
        argdic = defmkplot(label='{0} (Obs : {1})'.format(self.name, self.size))
        argdic.update(kwarg)
        
        x_data = self.sort_values(ascending=True, inplace=False)
        if rank:
            y_data = self.cum_rank()
        else:
            y_data = self.cum_frequency(a=a)
        
        line = pyplot.plot(x_data, y_data, **argdic)[0]

        pyplot.grid(1)
        pyplot.title("CDF")
        pyplot.xlabel("$x$")
        pyplot.ylabel("$F(x)$")

        if log_axis:
            pyplot.semilogx()

        return line

    def pdf_plot(self, true_pdf_plot=True, **kwarg):
        """Graph la densite de probabilite empirique de l'echantillon
        
        >>> f = pyplot.figure()
        >>> x = [scipy.stats.norm(loc=10, scale=1).ppf(i) for i in numpy.arange(0.01, 1, 0.01)]
        >>> test_sample = Sample(x)
        >>> l= test_sample.pdf_plot()
        >>> l[0].set_label('r')
        >>> f.show()
        """

        argpdf = {'classes' : None, 'rank' : False, 'equi_intervals' : True, 'n_center' : None, 'condition' : "g-leq"}
        argplot = {'align' : 'center', 'label' : 'Obs : {0}'.format(self.size), "fc" : "0.75", 'ec' : "k", 'lw' : 2}
        
        kwplot = {}
        kwpdf = {}
        
        for k, v in kwarg.items():
            if k in argpdf:
                kwpdf[k] = v
            else:
                kwplot[k] = v
        
        argpdf.update(kwpdf)
        argplot.update(kwplot)
        
        dataf = self.pdfs(**argpdf)
        bounds = dataf[[MN_BD_KY, MX_BD_KY]]
        y = dataf[VAL_KEY]
        widths = bounds[MX_BD_KY] - bounds[MN_BD_KY]
        x = bounds.mean(axis=1)

        if true_pdf_plot:
            y = y / widths
            if argpdf['rank']:
                print("warn : this kind of graph is not adequat for ranked values")

        res = pyplot.bar(x.values, y.values, width=widths.values, **argplot)

        pyplot.title("PDF")
        pyplot.xlabel('$x$')
        pyplot.ylabel('$f(x)$')

        return res

    def discrete_pdf_plot(self, rank=True, add_marker=True):
        """
        
        >>> f = pyplot.figure()
        >>> s = numpy.concatenate([[i] * i for i in range(10)])
        >>> s = Sample(s)
        >>> dic = s.discrete_pdf_plot()
        >>> f.show()
        """
        x = list(set(self))
        x = numpy.sort(x)
        y = []

        for v in x:
            nvals = numpy.sum(self == v)
            y.append(nvals)
        
        y = numpy.array(y, dtype=int)
        
        assert y.sum() == self.size, "pb code"

        y = numpy.array(y)
        if rank is False or rank in ("F", "f", "normalized"):
            y = y / self.size

        vlines = pyplot.vlines(x, ymin=0, ymax=y, color="k")
        
        if add_marker:
            line = pyplot.plot(x, y, ls="", marker="o", color="k")[0]
        else:
            line = None
        
        pyplot.grid(1)
        pyplot.title("discrete PDF")
        pyplot.xlabel('$x$')
        pyplot.ylabel('$f(x)$')

        res = {"l":line, "vl":vlines}
        return res


    def simple_plot(self, **kwargs):
        """
        Function doc
        >>> test_sample = Sample(numpy.random.normal(10))
        >>> f = pyplot.figure()
        >>> l=test_sample.simple_plot()
        >>> f.show()
        """
        res = pyplot.vlines(numpy.arange(self.size),ymax= self.values, ymin=0, **kwargs)
        return res
        

    def return_level_plot(self, a=0, npy=None, plot_axes=1, **kwarg):
        """Graph la periode de retour empirique de l'echantillon
        >>> x = [scipy.stats.norm(loc=10, scale=1).ppf(i) for i in numpy.arange(0.01, 1, 0.01)]
        >>> test_sample = Sample(x)
        
        >>> f = pyplot.figure()
        >>> l= test_sample.return_level_plot(npy=2.)
        >>> l.set_label('r')
        >>> l= test_sample.return_level_plot(npy = None)
        >>> l.set_label('r')
        >>> f.show()
        
        """
        argdic = defmkplot(label='Obs : {0}'.format(self.size))
        argdic.update(kwarg)
        
        if npy is None or npy is False:
            x_data = self.return_events(a=a)
            title = "N events Return Level"
        else:
            x_data = self.return_periods(a=a, npy=npy)
            title = "N years Return Level"

        y_data = self.sort_values(ascending=True, inplace=False)

        line = pyplot.plot(x_data, y_data, **argdic)[0]
        pyplot.title(title)
        pyplot.xlabel("$T_{x}$")
        pyplot.ylabel("$x$")

        if plot_axes:
            n = int(math.ceil(math.log10(x_data.max())))
            xticks = [i*10**j  for j in range(n) for i in [2, 5, 10]]
            fig = pyplot.gcf()
            ax = fig.gca()
            ax.semilogx()
            ax.set_xticks(xticks)
            ax.set_xticklabels([str(i) for i in xticks])
            ax.set_xlim(1, 1.1*x_data.max())
            ax.grid(1)

        return line

    def neg_ln_sf_plot(self, a=0, **kwarg):
        """
        >>> f = pyplot.figure()
        >>> x = [scipy.stats.norm(loc=10, scale=1).ppf(i) for i in numpy.arange(0.01, 1, 0.01)]
        >>> test_sample = Sample(x)
        >>> l= test_sample.neg_ln_sf_plot()
        >>> l.set_label('r')
        >>> f.show()
        """
        argdic = defmkplot(label="Obs={0}".format(self.size), ms=5., ls="--")
        argdic.update(kwarg)
        
        y_data = self.sort_values(ascending=True, inplace=False)
        x_data = self.emp_ln_cum_sf(a=a)
        
        res = pyplot.plot(x_data, y_data, **argdic)[0]
        
        pyplot.grid(1)
        pyplot.title("CDF")
        pyplot.xlabel("- ln (1 - F(x))")
        pyplot.ylabel("x")
        
        return res
        
    def box_plot(self, pos=0, leg=0, wid=0.25):
        """Graph une box plot
        
        >>> f = pyplot.figure()
        >>> x = scipy.stats.norm(loc=10, scale=1).rvs(100)
        >>> test_sample = Sample(x)
        >>> l= test_sample.box_plot(1, 1)
        >>> f.show()
        """
        dic = {'min' : [wid/2., 'k'], 'max' : [wid/2., 'k'], 'median': [wid, 'r'], 'Q0.25' : [wid, 'b'], 'Q0.75' : [wid, 'b'], 'mean' : [6., 'y']}

        for i, arg in dic.items():
            c = arg[1]

            if i == 'mean':
                x = pos
                y = self.mean()
                pyplot.plot(x, y, 's', ms=arg[0], color=c, label=i)
            else:
                x = [pos-arg[0], pos+arg[0]]
                y = [self.get_statistic(i) for y in range(2)]
                pyplot.plot(x, y, color=c, label=i)

        for i in [['min', 'Q0.25'], ['max', 'Q0.75']]:
            x = [pos for t in range(2)]
            y = [self.get_statistic(t) for t in i]
            pyplot.plot(x, y, ls='--', color='b')

        for i in [pos-wid, pos+wid]:
            x = [i for t in range(2)]
            y = [self.get_statistic(t) for t in ('Q0.25', 'Q0.75')]
            pyplot.plot(x, y, color='b')

        pyplot.grid(1)

        if leg:
            leg = pyplot.legend(numpoints=1)
            pyplot.setp(leg.get_texts(), fontsize='small')

        return None

    def hist_pdf_plot(self, names=None, colors=None, pos=0, wid=0.25, **kwarg):
        """
        Plot a pdf in one column. Names of each classes are expected
        >>> x = [scipy.stats.norm(loc=10, scale=1).ppf(i) for i in numpy.arange(0.01, 1, 0.01)]
        >>> test_sample = Sample(x)
        >>> l= test_sample.hist_pdf_plot()
        """
        argdic = {'classes' : None, 'rank' : False, 'equi_intervals' : True, 'n_center' : None, 'condition' : "g-leq"}                      
        argdic.update(kwarg)

        dataf = self.pdfs(**argdic)
        y = dataf[VAL_KEY]
        nclass = y.size

        if colors is None:
            colors = list(matplotlib.colors.cnames)[:nclass]
        
        if names is None:
            names = ["" for i in range(nclass)]
        
        res = []
        bottom = 0
        for prob, col, lab in zip(y, colors, names):
            top = bottom + prob
            br = pyplot.bar(pos, height=prob, width=wid, align='center', bottom=bottom, color=col, label=lab)
            bottom = top
            res.append(br)
        
        if argdic['rank']:
            assert top == self.size, "pb top == {0}".format(top)
        else:
            assert abs(top - 1.) < 1e-8, "pb top == {0}".format(top)
                
        return res

    def montecarlo(self, **kwargs):
        """
        Function doc
        >>> s = Sample(list(range(10)))
        >>> s1 = s.montecarlo()
        >>> s1.index.isze = s.index.size
        >>> (s1.index == s.index).all()
        False
        >>> (s1.loc[s.index] == s).all()
        True
        """
        argdic = dict(n=self.size, replace=False)
        argdic.update(kwargs)
        res = self.sample(**argdic)
        return res

    def boot(self, **kwargs):
        """
        Bootstrap : sample with replacement. For computing sampling effects
        >>> s = Sample(list(range(10)))
        >>> s1 = s.boot()
        >>> len(set(s1)) < 10
        True
        >>> vals = []
        >>> for i in range(10): vals += list(s.boot(random_state = i))
        >>> (numpy.sort(list(set(vals))) == numpy.arange(10)).all()
        True

        >>> n = 10
        >>> values = numpy.arange(n)
        >>> ts = Sample(values)
        >>> tboot = ts.boot()
        >>> len(set(tboot)) < 10
        True
        
        >>> a= numpy.arange(10)
        >>> boots = [Sample(a).boot() for i in range(100)]
        >>> all([i.size ==10 for i in boots])
        True
        >>> boots = numpy.concatenate(boots)
        >>> boots = set(boots)
        >>> boots = numpy.sort(list(boots))
        >>> (boots == a).all()
        True
        """
        argdic = dict(n=self.size, replace=True)
        argdic.update(kwargs)
        res = self.sample(**argdic)
        res = Sample(res)
        return res

    # ~ def bootold(self, newsize="same", seed=None):
        # ~ """
        # ~ DEPRECATED
        # ~ """
        # ~ inds = bootstrap(size=self.size, newsize=newsize, seed=seed)
        # ~ res = self.iloc[inds]
        # ~ res = Sample(res)
        # ~ return res

    def return_lag_series(self, lag, use_index=False):
        # ~ """
        # ~ Select the appriote method to compute lag series
        # ~ >>> n=100
        # ~ >>> s=example_sample(n)
        # ~ >>> d = s.return_lag_series(1)
        # ~ >>> bool((d['x'] == s[:-1]).all())
        # ~ True
        # ~ >>> bool((d['lag'] == s[1:]).all())
        # ~ True
        
        # ~ >>> s.index = s.index.map(lambda x: int(x/10))
        # ~ >>> d = s.return_lag_series(1)
        # ~ >>> bool((d['x'] == s[:-1]).all())
        # ~ True
        # ~ >>> bool((d['lag'] == s[1:]).all())
        # ~ True
        # ~ >>> d = s.return_lag_series(1)
        # ~ >>> bool((d['x'] == s[:-1]).all())
        # ~ True
        # ~ >>> bool((d['lag'] == s[1:]).all())
        # ~ True
        # ~ >>> d = s.return_lag_series(1, True)
        # ~ >>> d['x'].size
        # ~ 90
        # ~ >>> d['lag'].size
        # ~ 90
        # ~ """
        print("DEPRECATED?? please check that the method shift do not the job?")# else uncomment this line")
        # ~ assert len(set(numpy.diff(self.index))) == 1, "please check index intervals"
        
        # ~ slc0 = slice(None, -lag)
        # ~ slc1 = slice(lag, None)
        
        # ~ if use_index:
            # ~ idx = self.index.drop_duplicates()
            # ~ idx0 = idx[slc0]
            # ~ idx1 = idx[slc1]
            # ~ vals0 = self.loc[idx0]
            # ~ vals1 = self.loc[idx1]
            
        # ~ else:
            # ~ vals0 = self[slc0]
            # ~ vals1 = self[slc1]
            
        # ~ res = {"x" : vals0, "lag" : vals1}
        return None



    def compute_moments(self, moments=range(1, 6), kind_of_moment="raw"):
        """
        Compute the moments

        >>> s = Sample([0, 1, 2, 3, 4])
        >>> s.compute_moments(list(range(1, 4)), "raw") == {'raw_m1': 2.0, 'raw_m2': 6.0, 'raw_m3': 20.0}
        True
        >>> s.compute_moments(list(range(1, 4)), "centred") == {'cen_m1': 0.0, 'cen_m2': 2.0, 'cen_m3': 0.0}
        True
        >>> stdmoms = s.compute_moments(list(range(2, 4)), "standardized")
        >>> for k in sorted(stdmoms.keys()): print(k, stdmoms[k])
        std_m2 0.8
        std_m3 0.0
        """
        assert isinstance(moments, Iterable), 'pb input {0}'.format(moments)
        if kind_of_moment == "raw":
            momname = "raw_m"
        elif kind_of_moment == "centred":
            momname = "cen_m"
        elif kind_of_moment == "standardized":
            momname = "std_m"
        else:
            assert 0, "pb code source"

        res = {}
        for i in moments:
            key = '{0}{1}'.format(momname, i)
            res[key] = self.compute_moment(moment=i, kind_of_moment=kind_of_moment)

        return res

    def compute_moment(self, moment, kind_of_moment="raw"):
        """
        Compute moment
        >>> values = scipy.stats.norm().rvs(500)
        >>> s = Sample(values)
        >>> abs(s.get_statistic('mean') - s.compute_moment(1)) <= 1e-16
        True
        >>> bool(s.get_statistic('var') - s.compute_moment(2, "centred") < 0.01)
        True
        >>> diffskew = s.compute_moment(3, "standardized") - s.skew()
        >>> bool(diffskew < 0.01 and diffskew > -0.01)
        True
        >>> diffkurt = s.kurtosis() + 3 - s.compute_moment(4, "standardized")
        >>> bool(diffkurt < 0.1 and diffkurt > 0.)
        True
        """
        assert isinstance(moment, Number), 'Exp numb got {0.__class__} : {0}'.format(moment)
        assert moment > 0, 'pb entree demande {0} got {1.__class__} : {1}'.format("sup a 0", moment)

        if kind_of_moment == "raw":
            values = self
        elif kind_of_moment == "centred":
            values = self.centred_values()
        elif kind_of_moment == "standardized":
            values = self.std_values()
        else:
            raise ValueError("Exp raw, centred, standardized, got {0}".format(kind_of_moment))

        res = numpy.mean([values ** moment])
        res = float(res)

        return res

    def compute_l_moments(self, nmom=5):
        """Calcul les Lmoments"""
        moms = lmoments3.lmom_ratios(self.values, nmom=nmom)
        inds = ["tau{0}".format(m+1) for m in range(nmom)]
        inds = [i.replace("tau1", "lm1").replace("tau2", "lm2") for i in inds]
        
        res = pandas.Series(moms, index=inds)
        return res
        
    def auto_correlation(self, lag, **kwargs):
        # ~ """
        # ~ Attention on ne rajoute zeroes que les pas de temps suivant une mesure

        # ~ >>> ac_lag1 = example_sample(n = 100).auto_correlation(1) ** 2
        # ~ >>> ac_lag2 = example_sample(n = 100).auto_correlation(2) ** 2
        # ~ >>> bool(ac_lag1 < 0.2 and ac_lag1 > 0)
        # ~ True
        # ~ >>> bool(ac_lag2 < 0.2 and ac_lag2 > 0)
        # ~ True
        # ~ """
        print("DEPRECATED?? please check that the method autocorr do not the job?")# else uncomment this line")
        # ~ fun = corfun(**kwargs)
        # ~ dic = self.return_lag_series(lag=lag)
        
        # ~ x = dic["x"]
        # ~ lag = dic["lag"]
        # ~ if len(x) > 5:
            # ~ res = fun(x, lag)[0]
        # ~ else:
            # ~ res = None
        return None

    def auto_correlation_function(self, lags): # voir si possbile **kwargs):
        """
        Attention on ne rajoute zeroes que les pas de temps suivant une mesure
        Function doc
        >>> acf = example_sample(n = 100).auto_correlation_function([1, 2, 3])
        >>> ac_vals = acf.values ** 2
        >>> bool(max(ac_vals) < 0.2 and min(ac_vals) > 0)
        True
        """
        data = {i_lag : self.autocorr(lag=i_lag) for i_lag in lags} #use kwargs if autocorr permit
        res = pandas.Series(data, index=lags)

        return res

    def plot_acf(self, lags, **kwargs):
        """
        Attention on ne rajoute zeroes que les pas de temps suivant une mesure
        >>> fig = pyplot.figure()
        >>> l = example_sample(n = 100).plot_acf([1, 2, 3])
        >>> fig.show()
        """
        lags = self.auto_correlation_function(lags=lags, **kwargs)
        res = pyplot.plot(lags.index, lags.values)[0]
        return res

    def get_statistic(self, key, **kwarg):
        """
        Get some statistic
        >>> Sample(list(range(10)) * 2).get_statistic('max')
        9
        >>> Sample(list(range(10)) * 2).get_statistic('max-1')
        9
        >>> Sample(list(range(10)) * 2).get_statistic('max-2')
        9
        >>> Sample(list(range(10)) * 2).get_statistic('max-3')
        8
        >>> Sample(list(range(10)) * 2).get_statistic('max-4')
        8
        >>> Sample(list(range(10)) * 2).get_statistic('max-5')
        7
        >>> Sample(list(range(10)) * 2).get_statistic('min')
        0
        >>> Sample(list(range(10)) * 2).get_statistic('min-1')
        0
        >>> Sample(list(range(10)) * 2).get_statistic('min-2')
        0
        >>> Sample(list(range(10)) * 2).get_statistic('min-3')
        1
        >>> Sample(list(range(10)) * 2).get_statistic('min-4')
        1
        >>> Sample(list(range(10)) * 2).get_statistic('min-5')
        2
        
        >>> s = Sample(numpy.arange(1, 100))
        >>> s.get_statistic('lm1')
        50
        >>> s.get_statistic("Q0.75")
        75
        """
        if hasattr(self, key):
            fun = getattr(self, key)
            if isinstance(fun, (FunctionType, MethodType, BuiltinFunctionType)): 
                res = fun(**kwarg)
            elif isinstance(fun, Number):
                res = fun
            else:
                raise ValueError("{0} is not a valid key, please implement".format(key))
        
        elif hasattr(numpy, key):
            fun = getattr(numpy, key)
            res = fun(self)
        elif "-" in key and ('min' in key or "max" in key):
            #~ assert 0, "use n_largest, "
            key2 = key.split("-")
            #~ print(key2
            assert len(key2) == 2, "pb key"
            sel = key2[0]
            nb = int(key2[1])
            
            if sel == "max":
                index = - nb
            elif sel == 'min':
                index = nb -1
            else:
                assert 0, 'pb input, expected {0} but got {1.__class__} : {1}'.format("min or max", key)
                    
            res = self.sort_values(ascending=True, inplace=False).values[index]
        
        elif "Q" in key:
            f = key.replace("Q", "")
            f = float(f)
            res = self.quantile(f=f)
        else:
            moms = self.compute_moments()
            lmoms = self.compute_l_moments()
            if key in moms:
                res = moms[key]
            elif key in lmoms:
                res = lmoms[key]
            else:
                raise ValueError("Dont knwo what to do with {0}".format(key))
        
        res = float(res)
        
        if res.is_integer():
            res = int(res)

        return res
        
    def conf_int(self, statistic, ci=0.9, nboot=100, **kwarg):
        """
        Function doc
        >>> from hades_stats import Distribution
        >>> dnorm = Distribution("norm", loc = 0, scale = 1)
        >>> snorm = Sample(dnorm.random(100, seed = 0))
        >>> ci = 0.9
    
        #thoritical conf int
        >>> qmnmx = q_mn_mx_for_ci(ci = ci)
        >>> th_conf_inf = Distribution('norm', loc = snorm.mean(), scale = snorm.sem())
        >>> th_conf_inf = th_conf_inf.quantile(qmnmx)
        >>> th_conf_int = pandas.Series(th_conf_inf, index = [CI_MN_KY, CI_MX_KY])
        
        #boot interval
        >>> bt_conf_int = snorm.conf_int(statistic = 'mean', ci = ci)
        >>> m=bt_conf_int.pop(EST_KEY)
        >>> (numpy.abs(bt_conf_int - th_conf_int) < 0.1).all()
        True
        
        >>> snorm = Sample(dnorm.random(100, seed = 1))
        >>> ci = 0.6
        
        #thoritical conf int
        >>> qmnmx = q_mn_mx_for_ci(ci = ci)
        >>> th_conf_int = Distribution('norm', loc = snorm.mean(), scale = snorm.get_statistic('sem'))
        >>> th_conf_int = th_conf_int.quantile(qmnmx)
        >>> th_conf_int = pandas.Series(th_conf_inf, index = [CI_MN_KY, CI_MX_KY])
        
        #boot interval
        >>> bt_conf_int = snorm.conf_int(statistic = 'mean', ci = ci)
        >>> m=bt_conf_int.pop(EST_KEY)
        >>> (numpy.abs(bt_conf_int - th_conf_int) < 0.1).all()
        True
        
        >>> snorm = Sample(dnorm.random(100))
        >>> conf_int = snorm.conf_int(statistic = 'mean', ci = 0.95)
        >>> conf_int[CI_MN_KY] < 0
        True
        >>> conf_int[CI_MX_KY] > 0
        True
        """
        est = self.get_statistic(key=statistic, **kwarg)

        qmnmx = q_mn_mx_for_ci(ci=ci)
        boot_ss = [self.boot() for boot in range(nboot)]
        boot_vals = [sboot.get_statistic(key=statistic, **kwarg) for sboot in boot_ss]
        #~ print(boot_vals)
        boot_vals = Sample(boot_vals)
        
        #compute ci on boot values
        cis = boot_vals.quantile(qmnmx)
        cis = {k:v for k, v in zip([CI_MN_KY, CI_MX_KY], cis)}
        cis[EST_KEY] = est
        
        
        #~ print(res)
        res = pandas.Series(cis, index=[CI_MN_KY, EST_KEY, CI_MX_KY])
        #check result
        
        if est < res[CI_MN_KY] or est > res[CI_MX_KY]:
            print("warning, conf int is probably too narrow\nest={0}\n{1}".format(est, res))
        
        return res

        
    def summary(self, kys=None, pprint=False):
        """
        #not run 
        r = Sample(numpy.arange(10)).summary()
        
        dates = pandas.date_range('2000-01-01', '2010-01-01', freq='M')
        x = numpy.arange(dates.size)
        Sample(x, index=dates).summary()
        
        """
        if kys is None:
            kys = ['size', 'median', 'min', 'max', 'mean', 'sem', 'std', 'cv', 'skew', 'kurtosis']
            if isinstance(self.index, pandas.DatetimeIndex):
                kys = kys + ['npy']
        else:
            pass
        
        res = pandas.Series({ky : self.get_statistic(ky) for ky in kys}, index=kys)
        
        if pprint:
            for ky in kys:
                txt = "{0:15} = {1}".format(ky, res[ky])
                print(txt)

        return res
        
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='Samle module')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        import doctest
        test_result = doctest.testmod()
        print(test_result)

    #+++++++++++++++++++++++++++++++#
    #    EXAMPLES                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.examples:
        # ~ from japet_misc  import close_all, show_all

        pyplot.close('all')

        s = Sample(numpy.arange(100))
        s.cdf(50)
        xx = [scipy.stats.norm(loc=10, scale=1).ppf(i) for i in numpy.arange(0.01, 1, 0.01)]
        test_sample = Sample(xx)
        
        fg = pyplot.figure()
        l = test_sample.pdf_plot()
        fg.show()
        
        fg = pyplot.figure()
        l = test_sample.hist_pdf_plot()
        fg.show()
        
        fg = pyplot.figure()
        l = test_sample.cdf_plot()
        yy = [scipy.stats.genextreme(0, loc=10, scale=1).ppf(i) for i in numpy.arange(0.01, 1, 0.01)]
        fg.show()
        
        fg = pyplot.figure()
        l = Sample(yy).return_level_plot(npy=1, a=0.)
        fg.show()

        #~ show_and_close()
        
        d0 = datetime.datetime(year=random.randrange(2010, 2012), month=random.randrange(1, 13), day=random.randrange(1, 28))
        ns = 100
        numpy.arange(ns)
        dates = [d0 + i * datetime.timedelta(365/2) for i in range(ns)]
        s = Sample(numpy.arange(ns), index=dates)
        s = Sample(numpy.arange(ns), index=dates)[1:10]
        s = Sample(numpy.arange(ns), index=dates)[1:9]
        s = Sample(Sample(numpy.arange(ns), index=dates)[1:9])
        s = Sample(Sample(numpy.arange(ns), index=dates)[1:9])
        
        s = Sample(numpy.arange(ns), index=dates)


        fg = pyplot.figure()
        l = Sample(yy).neg_ln_sf_plot(a=0.)
        fg.show()
        #
        
        

        m1 = -1
        m2 = 1
        sd = 0.5
        
        n = 1000
        hn = n // 2
        
        x1 = numpy.random.normal(m1, sd, hn)
        x2 = numpy.random.normal(m2, sd, hn)
        xx = numpy.concatenate([x1, x2])        


        ss = Sample(xx)

        
        fg = pyplot.figure()
        l = ss.pdf_plot()
        l1 = ss.kde().plot()
        fg.show()

