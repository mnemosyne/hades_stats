"""
Module python : 
 - basic utilitairies
 - likelihood
 - Score classes
"""


##======================================================================================================================##
##                                PACKAGES IMPORT                                                                       ##
##======================================================================================================================##


#packages python
from collections.abc import Iterable
# ~ import math
import random


import numpy
import scipy.stats
# ~ import scipy.integrate
# ~ import scipy.fftpack
from matplotlib import pyplot
import pandas

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##


#~ COR_FUN = {"pearson" : scipy.stats.pearsonr, "spearman" : scipy.stats.spearmanr, "kendall" : scipy.stats.kendalltau}
                
QMN_KY = "qmn"
QMX_KY = "qmx"

_rank = "rank"
_ols = "pearson"

##======================================================================================================================##
##                                FUNCTIONS                                                                             ##
##======================================================================================================================##

def neg_log_likelihood(probs, force0=True):
    """
    Calcul la negative log likelihood dune liste de proabilitee
    """
    probs = numpy.array(probs)
    if force0:
        lim = 1e-100
        f = numpy.where(probs <= lim)[0]
        probs[f] = lim

    res = numpy.log(probs)
    res = - res.sum()

    return res
    
def bootstrap(size, newsize="same", seed=None):
    """
    Performs resampling from size with replacement
    chaque index est tire aleatoirement avec remise
    Return index
    >>> i = bootstrap(10)
    >>> len(i)
    10
    >>> len(set(i)) < 10
    True
    >>> v = []
    >>> for i in range(10): v += list(bootstrap(10))
    >>> numpy.sort(list(set(v)))
    array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
    >>> v = []
    >>> for i in range(20): v += list(bootstrap(10, newsize = 5))
    >>> numpy.sort(list(set(v)))
    array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
    >>> v = []
    >>> for i in range(5): v += list(bootstrap(10, newsize = 15))
    >>> numpy.sort(list(set(v)))
    array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
    """
    if seed is not None:
        numpy.random.seed(seed)
    
    if newsize == "same":
        newsize = size
    else:
        assert isinstance(newsize, int) or newsize.is_integer(), 'pb entree demande {0} got {1.__class__} : {1}'.format("nombre", newsize)
    res = scipy.stats.randint(0, size).rvs(size=newsize) #tirage aleatoire avec remise
    return res

def montecarlo(size, newsize="same", seed=None):
    """
    Random drawing without replacement
    Tirage aleatoire sans remise
    >>> i = montecarlo(10)
    >>> all(i == numpy.arange(10))
    False
    >>> len(i)
    10
    >>> len(set(i)) == 10
    True
    >>> all(numpy.sort(i) == numpy.arange(10))
    True
    >>> len(montecarlo(10, newsize = 2))
    2
    """
    if seed is not None:
        random.seed(seed)
    
    if newsize == "same":
        newsize = size
    else:
        assert isinstance(newsize, int) or newsize.is_integer(), 'pb entree demande {0} got {1.__class__} : {1}'.format("nombre", newsize)

    res = random.sample(range(size), newsize)# juste changer de place : tirage aleatoire sans remise
    res = numpy.array(res, dtype=int)

    return res

def montecarlovec(vec, newsize="same", seed=None):
    """
    Doc
    >>> a= numpy.arange(10)
    >>> boots = [montecarlovec(a) for i in range(10)]
    >>> all([i.size ==10 for i in boots])
    True
    >>> any([(i == a).all() for i in boots])
    False
    >>> all([(numpy.sort(i) == a).all() for i in boots])
    True
    """
    vec = numpy.array(vec)
    size = vec.size
    inds = montecarlo(size=size, newsize=newsize, seed=seed)
    res = vec[inds]
    return res

def indexes_segment_curve(vec, tol=2, maxvec=2):
    """
    >>> vec = list(range(5)) + list(range(4,-5,-1)) + list(range(3))
    >>> all(indexes_segment_curve(vec)['xe'] == numpy.array([ 4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16]))
    True
    >>> all(indexes_segment_curve(vec, maxvec = 3)["xn"] == numpy.array([ 4,  5,  6,  7,  8,  9, 10, 11, 12, 13]))
    True
    """
    f_ind = 0
    l_ind = len(vec) - 1
    
    indmin = numpy.argmin(vec)
    indmax = numpy.argmax(vec)
    
    m_inds = [indmin, indmax]
    m_names = ["n", "x"]
    sortind = numpy.argsort(m_inds)
    m_inds = [m_inds[i] for i in sortind]
    m_names = [m_names[i] for i in sortind]
    
    breaks = [f_ind] + m_inds  + [l_ind]
    b_names = ["f"]   + m_names + ['e']
    
    #~ print(breaks)
    #~ print(b_names)
    
    for indbreak in [1, -2]:
        b = breaks[indbreak]
        diffs = [abs(b - breaks[i]) for i in (0, -1)]
        diffs = numpy.array(diffs)
        if any(diffs < tol):
            del breaks[indbreak]
            del b_names[indbreak]
            #~ print(breaks, b_names)

    if len(breaks) == 4: #verif mid breaks
        #~ print(breaks)
        i1 = breaks[1]
        i2 = breaks[2]
        dif = i2 - i1
        if dif < tol or maxvec == 2:
            if i1 - f_ind > l_ind - i2:
                todel = 2
            else:
                todel = 1
            
            del breaks[todel]
            del b_names[todel]
    
    #~ print(breaks)
    ind_bks = range(len(breaks))
    
    res = {}

    for i0, i1 in zip(ind_bks[:-1], ind_bks[1:]):
        k = b_names[i0] + b_names[i1]
        f = breaks[i0]
        e = breaks[i1]
        
        v = numpy.arange(f, e + 1, 1)
        v = numpy.array(v, dtype=int)
        res[k] = v
            
    return res
    
def segment_vec(vec, **kwarg):
    """
    Vector segmentation
    
    >>> vec = list(range(5)) + list(range(4,-5,-1)) + list(range(3))
    >>> segs=segment_vec(vec, maxvec = 3)
    >>> all(segs["fx"] == numpy.array([0, 1, 2, 3, 4]))
    True
    >>> all(segs["xn"] == numpy.array([ 4,  4,  3,  2,  1,  0, -1, -2, -3, -4]))
    True
    >>> all(segs["ne"] == numpy.array([-4,  0,  1,  2]))
    True
    """
    vec = numpy.array(vec)
    indexes = indexes_segment_curve(vec, **kwarg)
    
    res = {k : vec[inds] for k, inds in indexes.items()}
    
    return res

def segment_multi_vec(dicvec, key, **kwarg):
    """
    Vector segmentatoin
    
    >>> vec = list(range(5)) + list(range(4,-5,-1)) + list(range(3))
    >>> x = list(range(len(vec)))
    >>> dic = {'x':x,'vec': vec}
    >>> segs = segment_multi_vec(dic, 'vec', maxvec = 3)
    >>> segs = {seg : {k : list(v) for k,v in dic.items()} for seg, dic in segs.items()}
    >>> segs["fx"] == {'x': [0, 1, 2, 3, 4], 'vec': [0, 1, 2, 3, 4]}
    True
    >>> segs["xn"] == {'x': [4, 5, 6, 7, 8, 9, 10, 11, 12, 13], 'vec': [4, 4, 3, 2, 1, 0, -1, -2, -3, -4]}
    True
    >>> segs["ne"] == {'x': [13, 14, 15, 16], 'vec': [-4, 0, 1, 2]}
    True
    """
    dic = {}

    for k, v in dicvec.items():
        dic[k] = numpy.array(v)

    vec = dic[key]
    indexes = indexes_segment_curve(vec, **kwarg)
    
    for k, v in dic.items():
        assert v.size == vec.size, f"uncoherent size : {key}, {k}"
    
    
    res = {seg : {k: v[inds] for k, v in dic.items()} for seg, inds in indexes.items()}

    return res
        

def q_mn_mx_for_ci(ci):
    """
    Function doc
    >>> q_mn_mx_for_ci(0.5)
    qmn    0.25
    qmx    0.75
    dtype: float64
    >>> q_mn_mx_for_ci(0.6)
    qmn    0.2
    qmx    0.8
    dtype: float64

    >>> q_mn_mx_for_ci(0.9)
    qmn    0.05
    qmx    0.95
    dtype: float64

    """
    assert 0 < ci < 1
    d = (1 - ci) / 2
    qmn = d
    qmx = 1 - d
    res = pandas.Series([qmn, qmx], index=[QMN_KY, QMX_KY])
    #~ {"qmn" : qmn, 'qmx' : qmx}
    return res


        

def corfun(name="pearson"):
    """ Class doc
    >>> x1 = numpy.array([0, 5, 8, 9, 25, 6])
    >>> x2 = numpy.array([0, 5, 8, 9, 6, 25])
    >>> round(corfun()(x1, -x2)[0], 3)
    -0.005
    >>> round(corfun("kendall")(x1, x2).correlation, 3)
    0.333
    >>> round(corfun("spearman")(x1, x2).correlation, 3)
    0.486
    
    >>> x1m = numpy.array([0, 5, 8, 9, 25, 6])
    >>> x2m = numpy.array([0, 5, 8, 9, 6, 25])

    >>> c = corfun()
    >>> r = c(x1m, x2m)
    
    """
    name = name.lower()
    if name in ("pearson", "ols"):
        name = "pearsonr"
    elif name in ("kendall", "mann-kendall"):
        name = "kendalltau"
    elif name in ("spearman", "spear"):
        name = "spearmanr"
    else:
        pass

    res = getattr(scipy.stats, name)
    return res
    

##======================================================================================================================##
##                                SCORES FUNCTIONS                                                                      ##
##======================================================================================================================##

def mse_compute(x1, x2):
    """
    Compute Mean square Error
    >>> s = Score([1,3.,4.,5.], [2.,3.,1.,6.])
    >>> s["mse"]
    2.75
    >>> s["relerrmse"] ==0.400625
    True
    """
    assert isinstance(x1, Iterable), 'pb entree demande {0} got {1.__class__} : {1}'.format("list", x1)
    assert isinstance(x1, Iterable), 'pb entree demande {0} got {1.__class__} : {1}'.format("list", x2)
    assert len(x1) == len(x2), 'dim error'
    x1 = numpy.array(x1, dtype=float)
    x2 = numpy.array(x2, dtype=float)
    #compute
    errors = x1 - x2 
    sq_errs = errors ** 2
    res = sq_errs.mean()
    return res

def rmse_compute(x1, x2):
    """
    Compute the Root Mean Square Error
    >>> s = Score([1,3.,4.,5.], [2.,3.,1.,6.])
    >>> s["rmse"] == 2.75 **0.5
    True
    >>> s["relerrrmse"] == 0.400625 ** 0.5
    True
    """
    mse = mse_compute(x1=x1, x2=x2)
    res = mse ** 0.5
    return res



##======================================================================================================================##
##                                CLASSES                                                                               ##
##======================================================================================================================##


class Simple_Score:
    """ Class doc 
    >>> s = Simple_Score(1,2)
    >>> ["{0:.2f}".format(s[i]) for i in ('bias', 'relative_bias', 'ratio')]
    ['1.00', '1.00', '2.00']
    >>> s = Simple_Score(0,5)
    >>> ["{0:.2f}".format(s[i]) for i in ('bias', 'relative_bias', 'ratio')]
    ['5.00', 'nan', 'nan']
    """
    
    def __init__(self, obsval, modval):
        """ Class initialiser """
        #~ assert type(obsval) in NUMB_TYPE, 'pb input : expected {0} got {1.__class__} : {1}'.format('numb', obsval)
        #~ assert type(modval) in NUMB_TYPE, 'pb input : expected {0} got {1.__class__} : {1}'.format('numb', modval)
        
        self.obsval = obsval
        self.modval = modval
            
    def __getitem__(self, key):
        """
        Get a score
        SEE main docstring
        """
        if key == 'bias':
            res = self._bias()
        else:
            if self.obsval == 0:
                res = numpy.nan
            elif key in ("rel_bias", "relative_bias", "relbias"):
                res = self._rel_bias()
            elif key == "ratio":
                res = self._ratio()
            else:
                assert 0, "{0} not implemented".format(key)
        return res
            
            
    def _bias(self):
        """
        Bias compute
        """
        res = self.modval - self.obsval
        return res
            
            
    def _rel_bias(self):
        """
        Rel bias compute
        """
        res = self._bias() / abs(self.obsval)
        
        return res
            
    def _ratio(self):
        """
        Ratio
        """
        res = self.modval / self.obsval
        return res


class Score:
    """
    Class calculant different score entere des observation et des simulation, interpolation
    entree : un vecteur observation, un vecteur model
    sortie : calcul dififerent score
    >>> s = Score(scipy.stats.norm().rvs(100), scipy.stats.norm().rvs(100))
    """
    ERROR_SCORES = ("mse", 'rmse', "mean_error", "mae", "std_error")
    OTHERS_SCORES = ("correlation", "rsquared", "nash", 'ratio_std', "bias")
    SCORE_NAMES = tuple(list(ERROR_SCORES) + list(OTHERS_SCORES))
    KINDS = ("abs_error", "rel_error")
    

    def __init__(self, x_obs, x_model):
        """
        >>> list(sorted(Score(scipy.stats.norm().rvs(100), scipy.stats.norm().rvs(100)).__dict__.keys()))
        ['errors', 'rel_errors', 'x_model', 'x_obs']
        """
        
        assert isinstance(x_obs, Iterable), 'pb entree demande {0} got {1.__class__} : {1}'.format("list", x_obs)
        assert isinstance(x_model, Iterable), 'pb entree demande {0} got {1.__class__} : {1}'.format("list", x_model)
        assert len(x_obs) == len(x_model), 'vecteur de taille differente'
        #~ print("Score class need an update")
        self.x_obs = numpy.array(x_obs)
        self.x_model = numpy.array(x_model)
        self.errors = self.x_model - self.x_obs
        self.rel_errors = self.errors / self.x_obs


    def __getitem__(self, key):
        """
        Renvoie le scoree demande en entree. On peut ajouter:
        - "-"           :       pour le score negatif
        - 'absval'      :       valeur absolue du score
        - "relerr"      :       score calcule sur les erreur relatives

        RETURN          :       valeur du score

        >>> s = Score(scipy.stats.norm().rvs(100), scipy.stats.norm().rvs(100))
        >>> s['rmse'] > 0
        True
        >>> s['-rmse'] == - s['rmse']
        True
        >>> type(Score(scipy.stats.norm().rvs(100), scipy.stats.norm().rvs(100))['errors'])
        <class 'numpy.ndarray'>
        """
        if key[0] == "-":
            key = key.replace('-', "")
            sign = -1
        else:
            sign = 1

        if key[:6] == "absval":
            key = key.replace("absval", "")
            absval = True
        else:
            absval = False

        if key[:6] == "relerr":
            key = key.replace("relerr", "")
            kind = "rel_error"
        else:
            kind = "abs_error"

        if key in self.SCORE_NAMES:
            res = self.compute_score(score_name=key, kind=kind, absolute_value=absval)
        elif key in self.__dict__:
            res = self.__dict__[key]
        else:
            assert 0, "{0} not in {1}".format(key, list(self.__dict__.keys()) + list(self.SCORE_NAMES))

        res = sign * res

        return res

    def compute_all_scores(self, kind, absolute_value, scores="all"):
        """
        >>> s=Score([1,3.,4.,5.], [2.,3.,1.,6.])
        >>> dic = s.compute_all_scores(kind = "abs_error", absolute_value = True)
        >>> list(sorted(dic.keys()))
        ['bias', 'correlation', 'mae', 'mean_error', 'mse', 'nash', 'ratio_std', 'rmse', 'rsquared', 'std_error']
        """
        if scores == "all":
            scores = self.SCORE_NAMES
        else:
            assert isinstance(scores, Iterable), 'pb entree demande {0} got {1.__class__} : {1}'.format("list", scores)

        dic = {score_name : self.compute_score(score_name=score_name, kind=kind, absolute_value=absolute_value) for score_name in scores}

        res = pandas.Series(dic)
        return res


    def compute_score(self, score_name, kind, absolute_value):
        """
        >>> s = Score([1,3.,4.,5.], [2.,3.,1.,6.])
        >>> (s.x_obs == numpy.array([1., 3., 4., 5.])).all()
        True
        >>> (s.x_model == numpy.array([2., 3., 1., 6.])).all()
        True
        >>> s.errors
        array([ 1.,  0., -3.,  1.])
        >>> s.rel_errors
        array([ 1.  ,  0.  , -0.75,  0.2 ])

        """
        sckey = "_{0}".format(score_name)
        fun = getattr(self, sckey)
        
        if score_name in self.ERROR_SCORES:
            if kind == "abs_error":
                errors = self.errors
            elif kind == "rel_error":
                errors = self.rel_errors
            else:
                assert self.KINDS, 'pb entree demande {0} got {1.__class__} : {1}'.format(self.KINDS, kind)
            
            res = fun(errors=errors)
        elif score_name in self.OTHERS_SCORES:
            res = fun()
        else:
            assert 0, "score {0} pas implemente : dans {1}".format(score_name, self.SCORE_NAMES)

        if absolute_value:
            res = abs(res)

        return res

    def _mean_error(self, errors):
        """
        Compute mean error
        >>> s = Score([1,3.,4.,5.], [2.,3.,1.,6.])
        >>> s["mean_error"]
        -0.25
        >>> s["relerrmean_error"]
        0.1125
        >>> s["absvalmean_error"]
        0.25
        >>> s["-absvalmean_error"]
        -0.25
        """
        res = errors.mean()
        return res

    def _mae(self, errors):
        """
        Compute maen absolute error
        >>> s = Score([1,3.,4.,5.], [2.,3.,1.,6.])
        >>> s["mae"]
        1.25
        >>> s["relerrmae"] ==0.4875
        True
        """
        abs_ers = abs(errors)
        res = abs_ers.mean()
        return res

    def _mse(self, errors):
        """
        Compute Mean square Error
        >>> s = Score([1,3.,4.,5.], [2.,3.,1.,6.])
        >>> s["mse"]
        2.75
        >>> s["relerrmse"] ==0.400625
        True
        """
        sq_errs = errors ** 2
        res = sq_errs.mean()
        return res

    def _rmse(self, errors):
        """
        Compute the Root Mean Square Error
        >>> s = Score([1,3.,4.,5.], [2.,3.,1.,6.])
        >>> s["rmse"] == 2.75 **0.5
        True
        >>> s["relerrrmse"] == 0.400625 ** 0.5
        True
        """
        mse = self._mse(errors=errors)
        res = mse ** 0.5
        return res

    def _std_error(self, errors):
        """
        Compute the standart deviation of errors
        >>> s = Score([1,3.,4.,5.], [2.,3.,1.,6.])
        >>> "{0:.2f}".format(s["std_error"])
        '1.89'
        >>> "{0:.2f}".format(s["relerrstd_error"])
        '0.72'
        """
        res = errors.std(ddof=1)
        return res

    def _ratio_std(self):
        """
        Compute the ratio of the standard deviations
        >>> s = Score([1,3.,4.,5.], [2.,3.,1.,6.])
        >>> "{0:.2f}".format(s["ratio_std"])
        '0.79'
        """
        res = self.x_obs.std(ddof=1) / self.x_model.std(ddof=1)          #rapport d'eccart type
        return res

    def _bias(self):
        """
        Compute the bias
        >>> s = Score([1,3.,4.,5.], [2.,3.,1.,6.])
        >>> "{0:.2f}".format(s["bias"])
        '0.92'
        """
        res = self.x_model.sum() / self.x_obs.sum()
        return res

    def _nash(self):
        """
        Compute the Nash score
        >>> s = Score([1,3.,4.,5.], [2.,3.,1.,6.])
        >>> "{0:.2f}".format(s["nash"])
        '-0.26'
        """
        sq_errors = self.errors ** 2
        dif_mean_obs = (self.x_obs-self.x_obs.mean()) **2
        res = 1- (sq_errors.sum()/ dif_mean_obs.sum())
        return res

    def _correlation(self):
        """
        coefficient correlation
        >>> s = Score([1,3.,4.,5.], [2.,3.,1.,6.])
        >>> "{0:.2f}".format(s["correlation"])
        '0.54'
        """
        res = scipy.stats.pearsonr(self.x_obs, self.x_model)[0]
        return res

    def _rsquared(self):
        """
        coefficient determination
        >>> s = Score([1,3.,4.,5.], [2.,3.,1.,6.])
        >>> "{0:.2f}".format(s["rsquared"])
        '0.29'
        """
        res = self._correlation() ** 2
        return res

    def return_score_table(self, kind, absolute_value, scores='all', fmt='.2f'):
        """
        Renvoie un tableau contenant les score contenue dans scare_names
        >>> x = scipy.stats.norm().rvs(100)
        >>> y = x.copy()
        >>> t=Score(x, y).return_score_table(kind = "abs_error", absolute_value = True)
        >>> Score(x, y).return_score_table(kind = "abs_error", absolute_value = True, scores = ['mae', 'rsquared','nash'])
        [['mae', '0.00'], ['nash', '1.00'], ['rsquared', '1.00']]
        """
        dic = self.compute_all_scores(kind=kind, absolute_value=absolute_value, scores=scores)

        res = []

        for k in sorted(dic.keys()):
            v = dic[k]
            if fmt is None:
                add = [k, v]
            else:
                add = [k, "{0:{1}}".format(v, fmt)]
            res.append(add)

        return res

    def return_scores_text(self, kind, absolute_value, scores='all', sep=' ; ', fmt='.2f'):
        """
        >>> x = scipy.stats.norm().rvs(100)
        >>> y = x.copy()
        >>> t=Score(x, y).return_scores_text(kind = "abs_error", absolute_value = True)
        >>> Score(x, y).return_scores_text(kind = "abs_error", absolute_value = True, scores = ['nash', 'rsquared'])
        'nash : 1.00 ; rsquared : 1.00'
        """
        dic = self.compute_all_scores(kind=kind, absolute_value=absolute_value, scores=scores)

        keys = sorted(dic.keys())
        res = ["{0} : {1:{2}}".format(k, dic[k], fmt) for k in keys]
        res = sep.join(res)

        return res

##======================================================================================================================##
##                TREND CLASS                                                                                           ##
##======================================================================================================================##


class Trend(pandas.Series):
    """ Class doc
    >>> x1 = numpy.array([0, 5, 8, 9, 25, 6])
    >>> x2 = numpy.array([0, 5, 8, 9, 6, 25])
    >>> ts = pandas.Series(x2, index=x1)
    >>> round(Trend(ts, typ = "pearson").rvalue, 3)
    0.005
    >>> round(Trend(-ts, typ = "pearson").rvalue, 3)
    -0.005
    >>> round(Trend(ts, typ = "kendall").rvalue, 3)
    0.333
    >>> round(Trend(ts, typ = "spearman").rvalue, 3)
    0.486
    >>> ls = Trend(ts, typ = "spearman")
    >>> ls()
    0.0      0.0
    25.0    25.0
    dtype: float64

    >>> ls(0, 5)
    0.0    0.0
    5.0    5.0
    dtype: float64
    >>> ls((0, 5))
    0.0    0.0
    5.0    5.0
    dtype: float64
    >>> ls(numpy.arange(10))
    0.0    0.0
    1.0    1.0
    2.0    2.0
    3.0    3.0
    4.0    4.0
    5.0    5.0
    6.0    6.0
    7.0    7.0
    8.0    8.0
    9.0    9.0
    dtype: float64
    
    
    >>> fig = pyplot.figure()
    >>> l=pyplot.plot(ts.index, ts.values, marker = 'o', ls = '')
    >>> t=ls.plot()
    >>> fig.show()
    
    
    """
    
    def __init__(self, *args, **kwargs):
        """ Class initialiser """
        if "typ" in kwargs:
            typ = kwargs.pop("typ")
        else:
            typ = "spearman"
        
        typ = typ.lower()
        
        pandas.Series.__init__(self, *args, **kwargs)

        if typ in ("theilslope", "theil", "rank", "spearman", "kendall"):
            self.typ = _rank
            if typ in ("spearman", "kendall"):
                cornm = typ
            else:
                cornm = "spearman"

        elif typ in ("classic", "pearson", "ols"):
            self.typ = _ols
        else:
            raise ValueError(f"Dont know what to do with {typ}")
            
        if self.typ == _rank:
            slope, intercept, cimin, cimax = scipy.stats.theilslopes(y=self.values, x=self.index)
            corres = corfun(cornm)(self.values, self.index)
            rvalue = corres.correlation
            pvalue = corres.pvalue
            
        elif self.typ == _ols:
            slres = scipy.stats.linregress(y=self.values, x=self.index)
            slope = slres.slope
            intercept = slres.intercept
            rvalue = slres.rvalue
            pvalue = slres.pvalue
        else:
            raise ValueError("pb code")
            
        self.intercept = intercept
        self.slope = slope
        self.rvalue = rvalue
        self.pvalue = pvalue

    def __call__(self, *args):
        """
        Get the value of the linear trend at given points
        """
        #~ print(args)
        if not args:
            xn = self.index.min()
            xx = self.index.max()
            x = [xn, xx]
        elif len(args) == 1:
            x = args[0]
        else:
            x = args
            
        
        x = numpy.array(x, dtype=float)
        ytrend = self.intercept + self.slope * x
        res = pandas.Series(ytrend, index=x)
        return res
    
    def slope_percentage(self):
        """Compute the slope percentage"""
        res = (self.slope / self.median()) #%/index
        return res

    
    def plot(self, *args, **kwargs):
        """
        Plot the trend
        """
        argdic = dict(ls='-', color='k', marker='')
        argdic.update(kwargs)
        trend = self(*args)
        res = pyplot.plot(trend.index, trend.values, **argdic)[0]
        return res


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##


if __name__ == '__main__':
    
    #~ from hades_stats.r2py import sink_r
    #~ sink_r()
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    import argparse
    
    parser = argparse.ArgumentParser(description='Samle module')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    # ~ parser.add_argument('--fft_example', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        import doctest
        test_result = doctest.testmod()
        print(test_result)

    #+++++++++++++++++++++++++++++++#
    #    EXAMPLES                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.examples:
        pass
        # ~ from functions import close_all, show_all
        # ~ close_all()


        # ~ def plotSpectrum(y, Fs):
            # ~ """
            # ~ Plots a Single-Sided Amplitude Spectrum of y(t)
            # ~ """
            # ~ n = len(y) # length of the signal
            # ~ k = numpy.arange(n)
            # ~ T = n/Fs
            # ~ frq = k/T # two sides frequency range
            # ~ frq = frq[range(n/2)] # one side frequency range

            # ~ Y = scipy.fftpack.fft(y)/n # fft computing and normalization
            # ~ Y = Y[range(n/2)]

            # ~ pyplot.plot(frq, abs(Y), 'r') # plotting the spectrum
            # ~ pyplot.xlabel('Freq (Hz)')
            # ~ pyplot.ylabel('|Y(freq)|')

        # ~ Fs = 150.0  # sampling rate
        # ~ Ts = 1.0/Fs # sampling interval
        # ~ t = numpy.arange(0, 1, Ts) # time vector

        # ~ ff = 5.5   # frequency of the signal
        # ~ y = numpy.sin(2*math.pi*ff*t)

        # ~ pyplot.subplot(2, 1, 1)
        # ~ pyplot.plot(t, y)
        # ~ pyplot.xlabel('Time')
        # ~ pyplot.ylabel('Amplitude')
        # ~ pyplot.subplot(2, 1, 2)
        # ~ plotSpectrum(y, Fs)
        # ~ show_all()
