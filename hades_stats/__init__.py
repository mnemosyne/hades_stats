"""
Module stats : different statistical functions and classes
"""


##======================================================================================================================##
##                                IMPORT                                                                                ##
##======================================================================================================================##

import time


## UNIVARIATE ANALYSIS
from hades_stats.utils import *
from hades_stats.sample import *
from hades_stats.distribution import *
from hades_stats.sample_vs_distribution import *
from hades_stats.fit import *
from hades_stats.lin_regress import *
from hades_stats.tests import *
from hades_stats.ns_distribution import *

## MULTIVAIATE 
from hades_stats.covar_sample import *
## NOT IMPORTED: bug to be fixed 
# ~ from hades_stats.covar_distr import *
# ~ from hades_stats.glm_fits import *



# ~ try:
    # ~ sink_r(typ='all')
# ~ except:
    # ~ print("R sink error")

# ~ from japet_misc import todo_msg
# ~ print(todo_msg("move math function into a math module", __file__))

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    opts = parser.parse_args()
    

    
