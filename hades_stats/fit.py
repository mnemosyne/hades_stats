"""
Univariate fits
"""


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

#~ import pandas
import argparse
import doctest

import numpy


from hades_stats.distribution import Distribution, sp_dist, sp_name, lm_dist, lm_name
from hades_stats.sample_vs_distribution import SampleDistr

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

MOM = "mom"
MLE = "mle"
LMOM = "lmom"


##======================================================================================================================##
##                FIT DISTRIBUTIONS                                                                                     ##
##======================================================================================================================##

#~ import lmoments3 as lm
#~ from lmoments3 import distr
#~ 
#~ paras = distr.gam.lmom_fit(data)
#~ paras
#~ OrderedDict([('a', 2.295206110128833), ('loc', 0), ('scale', 1.4100535991436407)])



class FitDist(SampleDistr):
    """ Class doc
    >>> x_norm = Distribution('norm', loc = 0, scale = 1).random(1000)
    >>> fnorm = FitDist(x_norm, model = 'nor')
    >>> sorted(fnorm.__dict__.keys())
    ['distribution', 'sample']
    >>> s = Distribution("unif", loc = 0, scale = 1).random(1000)
    >>> for model in ('norm', 'exp', 'gamma', 'unif'):d=FitDist(s, model=model, method = 'mle')
    >>> for model in ('norm', 'exp', 'gamma'):d=FitDist(s, model=model, method = 'lmom')

    >>> s = Distribution("exp").random(1000)
    >>> for model in ('norm', 'exp', 'gamma', 'unif'):d=FitDist(s, model=model, method = 'mle')
    >>> for model in ('norm', 'exp', 'gamma'):d=FitDist(s, model=model, method = 'lmom')
    
    
    >>> x_gev = Distribution('gev', c=-0.1, loc = 0, scale = 1).random(1000, seed = 3)
    >>> fgev=FitDist(x_gev, model = "gev", method = 'lmom')
    >>> len(fgev.__dict__)
    2
    >>> print(fgev.distribution)
    name     genextreme
    a          -10.1311
    b               inf
    c        -0.0987058
    loc       0.0244167
    scale      0.980532
    
    >>> x_pot = Distribution('gpd', c=0.1, loc = 0, scale = 1).random(1000, seed = 2)
    >>> fgpd = FitDist(x_pot, model='gpd', method = 'lmom')
    >>> print(fgpd.distribution)
    name     genpareto
    a                0
    b              inf
    c         0.140283
    loc      0.0131507
    scale      0.90386
    
    >>> x_gev = Distribution('gev', c=-0.1, loc = 0, scale = 1).random(1000, seed = 5)
    >>> fgev=FitDist(x_gev, model = "gev")
    >>> print(fgev.distribution)
    name     genextreme
    a          -16.5581
    b               inf
    c        -0.0603933
    loc     -0.00140048
    scale       1.00034

    
    >>> x_pot = Distribution('gpd', c=0.1, loc = 0, scale = 1).random(1000, seed = 10)
    >>> fgpd = FitDist(x_pot, model='gpd')
    >>> print(fgpd.distribution)
    name     genpareto
    a                0
    b              inf
    c        0.0972644
    loc     0.00176135
    scale     0.973109
    
    
    
    Fix a parameter
    >>> x_gev = Distribution('gev', c=-0.1, loc = 0, scale = 1).random(1000, seed = 1)
    >>> fgev=FitDist(x_gev, model = "gev", method = "mle", fc= -0.1)
    >>> print(fgev.distribution)
    name     genextreme
    a               -10
    b               inf
    c              -0.1
    loc     -0.00626203
    scale        1.0153
    
    >>> fgev=FitDist(x_gev, model = "gev", method = "mle", fc= 0.1)
    >>> print(fgev.distribution)
    name     genextreme
    a              -inf
    b                10
    c               0.1
    loc        0.122151
    scale       1.14777

    
    >>> data = [2.0, 3.0, 4.0, 2.4, 5.5, 1.2, 5.4, 2.2, 7.1, 1.3, 1.5]
    >>> fgam = FitDist(data, 'gamma', method='lmom')
    >>> print(fgam.distribution)
    name      gamma
    a       2.29521
    b           inf
    a       2.29521
    loc           0
    scale   1.41005
    
    """
    
    def __init__(self, x, model, *arg, method=MLE, **kwargs):
        """ Class initialiser """
        model = sp_name(model)
        x = numpy.asarray(x, dtype=float).copy()
        
        
        if method == MLE:
            dist = sp_dist(model)
            paras = dist.fit(x, **kwargs)
            distr = Distribution(model, *paras)
            
        elif method == LMOM:
            lmmod = lm_name(model)
            ldist = lm_dist(lmmod)
            paras = ldist.lmom_fit(x)
            distr = Distribution(model, **paras)
            
        elif method == MOM:
            dist = sp_dist(model)
            paras = dist.fit_loc_scale(x, *arg)
            distr = Distribution(model, *paras)
        else:
            raise ValueError("method {0} not implemented")
            
        SampleDistr.__init__(self, sample_obj=x, distr_obj=distr)


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='Samle module')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
