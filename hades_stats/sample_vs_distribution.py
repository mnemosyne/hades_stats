"""
Module permettant de comparer une distribution a un echantillon : qq plot, pp plot GOF test et quantile score
"""

##======================================================================================================================##
##                                PACKAGES IMPORT                                                                       ##
##======================================================================================================================##


#packages python
#~ import datetime
#~ import os
import doctest
import math
import argparse
from collections.abc import Iterable


import numpy
import pandas
import scipy
from matplotlib import pyplot

try:
    import skgof
    SKGOF = True
    # ~ raise
except:
    print("skgof pb import")
    SKGOF = False

from hades_stats.utils import Score
from hades_stats.distribution import Distribution
from hades_stats.sample import Sample


##====================================================================================================================##
##              CONSTANTS                                                                                             ##
##====================================================================================================================##

MOD_NM = "model"
OBS_NM = "sample"


if SKGOF:
    GOF_TESTS = {tst.replace("_test", "") : getattr(skgof, tst) for tst in dir(skgof) if "test" in tst}

##====================================================================================================================##
##                        Fonction                                                                                    ##
##====================================================================================================================##

def example_sampledistr(distr=Distribution(name='exp', loc=0, scale=1), n=50, seed=1):
    """
    >>> example_sampledistr().__class__ == SampleDistr
    True
    """
    values = distr.random(n, seed=seed)
    sample = Sample(data=values)
    res = SampleDistr(sample_obj=sample, distr_obj=distr)
    return res

def example_multi_samplesistr(distr=Distribution(name='norm', loc=0, scale=1), sample_size=50, n_sample=10, seed=1):
    """
    >>> example_multi_samplesistr().__class__ == Multi_SampleDistr
    True
    """
    res = [example_sampledistr(distr=distr, n=sample_size, seed=seed + i) for i in range(n_sample)]
    res = Multi_SampleDistr(res)
    return res

def model_penalty(name='aic', npar=None, sample_size=None):
    """
    Renvoie la valeur de la penalite a affecter a un model : aic aicc bic
    """
    #nb parametres
    assert isinstance(npar, int), "pb entree entier demander'"

    # calcul de la penalite
    if name == 'aic':
        res = 2 * npar
    elif name == 'aicc':
        pen1 = 2 * npar
        num = pen1 * (npar + 1)
        den = sample_size - npar - 1
        pen2 = float(num) / float(den)
        res = pen1 + pen2
    elif name == 'bic':
        res = npar * math.log(sample_size)
    else:
        assert 0, f"critere {name} non implemente dispo 'bic', aic', 'aicc'"

    return res

##====================================================================================================================##
##                        SAMPLE DISTR                                                                                ##
##====================================================================================================================##

class SampleDistr:
    """Class permettant de comparer un echantillon avec une distribution
    Entree :
    - sample_obj:   Vecteur (list ou numpy.array) du premier echanillon
    - distr_obj :   La fonction scipy.stats
    - a         :   Coefficient pour le calcul des frerquence empirriques
    - name1 et 2:   nom des echantillon
    - cvm       :   0 ou 1: dans le cas ou le tp de calcul est trop long ou que le test de cvm bugue

    >>> loc, scale = 50, 15
    >>> func = Distribution('gumbel', c= 0, loc=loc, scale = scale)
    >>> x = func.random(100)
    >>> test_sample_vs_distr=SampleDistr(Sample(x), distr_obj= func)
    
    #~ >>> f=pyplot.figure(1)
    #~ >>> l=test_sample_vs_distr.cdf_plot()
    #~ >>> f=pyplot.figure(2)
    #~ >>> l=test_sample_vs_distr.pdf_plot()
    #~ >>> f=pyplot.figure(3)
    #~ >>> l=test_sample_vs_distr.pp_plot()
    #~ >>> f=pyplot.figure(4)
    #~ >>> l=test_sample_vs_distr.qq_plot()
    #~ >>> f=pyplot.figure(5)
    #~ >>> l=test_sample_vs_distr.return_level_plot(npy=2.)
    #~ >>> l=test_sample_vs_distr.summary_plot()
    #~ >>> from functions import show_and_close
    #~ >>> show_and_close()
    """

    def __init__(self, sample_obj, distr_obj):
        """
        Initialisation de l'objet
        >>> sorted(example_sampledistr().__dict__.keys())
        ['distribution', 'sample']
        """
        assert isinstance(distr_obj, Distribution), f'exp Distribution : got {distr_obj.__class__}\n{distr_obj}'
        if isinstance(sample_obj, Sample):
            pass
        else:
            sample_obj = Sample(numpy.array(sample_obj))

        self.sample = sample_obj
        self.distribution = distr_obj
        
    def xlim(self):
        """
        Return the limits of sample
        """
        xn = math.floor(self.sample.min())
        xx = math.ceil(self.sample.max())
        res = (xn, xx)
        return res

    def gof_test(self, test='ks'):
        """
        Renvoi un objet Test_GOF_XXXXXXX
        >>> loc, scale = 50, 15
        >>> func = Distribution('gev', c=0.01, loc=loc, scale = scale)
        >>> x = [func.quantile(i) for i in numpy.arange(0.01, 1, 0.01)]
        >>> isinstance(SampleDistr(Sample(x), distr_obj= func).gof_test('ks'), skgof.ecdfgof.GofResult)
        True
        >>> round(SampleDistr(Sample(x), distr_obj= func).gof_test('ks').pvalue, 2)
        1.0
        >>> func = Distribution('norm', loc=loc, scale = scale)
        >>> x = [func.quantile(i) for i in numpy.arange(0.01, 1, 0.01)]
        >>> isinstance(SampleDistr(Sample(x), distr_obj= func).gof_test('ad'), skgof.ecdfgof.GofResult)
        True
        """
        x_obs = self.sample
        
        if test in GOF_TESTS:
            #~ npar= self.distribution.n_par()
            fun = self.distribution
            gof = GOF_TESTS[test]
            res = gof(data=x_obs, dist=fun)
        elif self.distribution.name == 'norm':
            if test == "skew":
                res = scipy.stats.kurtosistest(x_obs)
            elif test == "kurtosis":
                res = scipy.stats.skewtest(x_obs)
            else:
                assert 0, 'pb code source ou test pas encore implemente'
        else:
            raise ValueError("{0} not yet implemented")

        return res

    def gof_tests(self, tests='all'):
        """
        >>> loc, scale = 50, 15
        >>> func = Distribution('gev', c=0.01, loc=loc, scale = scale)
        >>> x = [func.quantile(i) for i in numpy.arange(0.01, 1, 0.01)]
        >>> sorted(SampleDistr(Sample(x), distr_obj= func).gof_tests().keys())
        ['ad', 'cvm', 'ks']

        >>> func = Distribution('norm', loc=loc, scale = scale)
        >>> sorted(SampleDistr(Sample(x), distr_obj= func).gof_tests().keys())
        ['ad', 'cvm', 'ks', 'kurtosis', 'skew']
        """
        if tests == 'all':
            tests = sorted(GOF_TESTS.keys())
            if self.distribution.name == 'norm':
                tests += ["skew", "kurtosis"]
        else:
            assert isinstance(tests, Iterable), f'exp list demande got : {tests.__class__}\n{tests}'

        res = {i : self.gof_test(test=i) for i in tests}

        return res

    def compare_moments(self):
        """
        >>> loc, scale = 50, 15
        >>> func = Distribution('gumbel', c=0,loc=loc, scale = scale)
        >>> x = [func.quantile(i) for i in numpy.arange(0.01, 1, 0.01)]
        >>> sorted(SampleDistr(Sample(x), distr_obj= func).compare_moments().keys())
        ['kurtosis', 'mean', 'skew', 'var']
        """
        distr_stats = self.distribution.create_summary()

        res = {i_mom : {OBS_NM : self.sample.get_statistic(i_mom), MOD_NM : d_mom} for i_mom, d_mom in distr_stats.items()}

        return res

    def quantile_quantile(self, weighted=False, **kwargs):
        """
        >>> example_sampledistr(n = 10).quantile_quantile()
             sample     model
        0  0.000114  0.095310
        1  0.096884  0.200671
        2  0.158710  0.318454
        3  0.206115  0.451985
        4  0.360013  0.606136
        5  0.423976  0.788457
        6  0.505453  1.011601
        7  0.539606  1.299283
        8  0.773960  1.704748
        9  1.274125  2.397895
        >>> example_sampledistr(n = 10).quantile_quantile(weighted = True)
             sample     model
        0  0.000039  0.032541
        1  0.036753  0.076125
        2  0.067733  0.135907
        3  0.100530  0.220451
        4  0.204858  0.344909
        5  0.289506  0.538386
        6  0.431425  0.863445
        7  0.614102  1.478658
        8  1.321216  2.910151
        9  4.350082  8.186826
        """
        probs = self.sample.cum_frequency(**kwargs)
        q_obs = self.sample.sort_values().values
        q_func = self.distribution.quantile(probs)

        if weighted:
            weight = 1 / (1 - probs)
            weight = weight  / weight.sum() * self.sample.size
            #~ print(weight, weight.sum()
            q_obs = q_obs * weight
            q_func = q_func * weight

        res = pandas.DataFrame({OBS_NM : q_obs, MOD_NM  : q_func}, columns=[OBS_NM, MOD_NM])

        return res

    def prob_prob(self):
        """
        >>> example_sampledistr(n = 10).prob_prob()
             sample     model
        0  0.090909  0.000114
        1  0.181818  0.092339
        2  0.272727  0.146756
        3  0.363636  0.186260
        4  0.454545  0.302333
        5  0.545455  0.345561
        6  0.636364  0.396767
        7  0.727273  0.417022
        8  0.818182  0.538817
        9  0.909091  0.720324
        """
        values = self.sample.sort_values().values
        p_obs = self.sample.cum_frequency()
        p_func = self.distribution.cdf(values)

        res = pandas.DataFrame({OBS_NM : p_obs, MOD_NM  : p_func}, columns=[OBS_NM, MOD_NM])

        return res

    def quantile_verification_score(self, quantile, normalize=False):
        """
        >>> s=example_sampledistr().quantile_verification_score(0.5)
        >>> s=example_sampledistr().quantile_verification_score(0.9)
        """

        def ro(q, u):
            """needed for computatation of QVS"""
            if u >= 0:
                res = q * u
            else:
                res = (q-1) * u
            return res

        q_model = self.distribution.quantile(f=quantile)
        q_obs = self.sample.sort_values()

        us = [q_i - q_model for q_i in q_obs]
        res = [ro(q=quantile, u=u) for u in us]
        res = numpy.array(res)

        if normalize:
            res = res.mean()
        else:
            res = res.sum()

        return res

    def nllh(self):
        """
        Compute the neg log likelihood
        """
        xs = numpy.array(self.sample, dtype=float)
        res = self.distribution.nllh(xs)
        return res

    def qq_score(self, weighted=False):
        """
        Renvois un objet Score
        >>> s=example_sampledistr().qq_score()
        >>> s.__class__ == Score
        True
        """
        dic = self.quantile_quantile(weighted=weighted)
        obs = dic[OBS_NM]
        model = dic[MOD_NM]
        res = Score(x_obs=obs, x_model=model)
        return res

    def information_criterion(self, name='aic', npar=None):
        """
        Renvoie la valeur du critere d'information : aic, aicc, bic
        """
        if npar is None:
            npar = self.distribution.n_params
        #sample size
        s_size = self.sample.stats['size']

        #perfomance du model
        perf = 2 * self.nllh()
        # calcul de la penalite
        pen = model_penalty(name=name, npar=npar, sample_size=s_size)

        res = perf + pen
        return res


    def cdf_plot(self, rank=False, log_axis=0):
        """
        Graph les deux fonctions de repartition
        >>> l=example_sampledistr().cdf_plot()
        """
        if rank:
            rank = self.sample.size
            ylim = (0, rank)
            print(rank)
        else:
            ylim = (0, 1)
        l1 = self.sample.cdf_plot(rank)
        l2 = self.distribution.cdf_plot(rank)

        if log_axis:
            pyplot.semilogx()

        #~ pyplot.xlim(self.xlim)
        pyplot.ylim(ylim)
        pyplot.title("CDF")
        pyplot.xlabel("$x$")
        pyplot.ylabel("$F(x)$")

        return l1, l2

    def pdf_plot(self, log_axis=0):
        """
        Graph les deux densite de probabilite empirique
        >>> l=example_sampledistr().pdf_plot()
        """

        l1 = self.sample.pdf_plot()
        l2 = self.distribution.pdf_plot()

        for i in l1:
            i.set_facecolor('none')
            i.set_edgecolor('r')
            i.set_lw(2)

        if log_axis:
            pyplot.semilogx()

        #~ pyplot.xlim(self.xlim)
        return l1, l2

    def qq_plot(self, weighted=False, **kwarg):
        """
        Quantile Quantile plot : pour une meme probabilite on place les deux quantiles des deu echantillon
        >>> ls = example_sampledistr().qq_plot()
        """
        argdic = dict(marker='o', mfc="0.5", mec="k", ls="")
        argdic.update(kwarg)
        
        q = self.quantile_quantile(weighted=weighted)
        y = q[MOD_NM]
        x = q[OBS_NM]

        res = pyplot.plot(x, y, **argdic)[0]

        pyplot.grid(1)

        lim = self.xlim()
        ax = pyplot.gcf().gca()
        ax.set(aspect="equal", ylim=lim, xlim=lim, title="QQ plot", xlabel=OBS_NM, ylabel=MOD_NM)

        return res

    def pp_plot(self, **kwarg):
        """
        Probability plot : pour une meme valeur, trace les deux probabilites correspondantes
        >>> ls=example_sampledistr().pp_plot()
        """
        argdic = dict(marker='o', mfc="0.5", mec="k")
        argdic.update(kwarg)
        
        p = self.prob_prob()
        x = p[OBS_NM]
        y = p[MOD_NM]
        
        res = pyplot.plot(x, y, **argdic)[0]

        pyplot.grid(1)

        lim = (0., 1.)
        
        ax = pyplot.gcf().gca()
        ax.set(aspect="equal", ylim=lim, xlim=lim, title="Probability plot", xlabel=OBS_NM, ylabel=MOD_NM)

        return res

    def return_level_plot(self, **kwarg):
        """
        >>> ls = example_sampledistr().return_level_plot()
        """
        l2 = self.distribution.return_level_plot(**kwarg)
        l1 = self.sample.return_level_plot(**kwarg)

        pyplot.ylabel("$x$")
        pyplot.xlabel("$T(x)$")

        res = {OBS_NM : l1, MOD_NM : l2,}

        return res

##======================================================================================================================##
##                MULTI SAMPLE DISTR                                                                                    ##
##======================================================================================================================##

class Multi_SampleDistr(list):
    """
    >>> l_svd = [example_sampledistr() for i in range(5)]
    >>> Multi_SampleDistr(l_svd).__class__ == Multi_SampleDistr
    True
    """

    def __init__(self, list_SampleDistr):
        """
        >>> list(example_multi_samplesistr().__dict__.keys())
        []
        """
        assert isinstance(list_SampleDistr, Iterable), f"List expected {list_SampleDistr} {list_SampleDistr.__class__}"
        list.__init__(self, [])
        for isd in list_SampleDistr:
            self.append(isd)

    def append(self, isd):
        """
        Add a SampleDistr obj
        """
        assert isinstance(isd, SampleDistr), "tous les element ne sont pas de la class SampleDistr"
        list.append(self, isd)

    def gof_test(self, test):
        """
        >>> tests = example_multi_samplesistr().gof_test('ad')
        >>> all([isinstance(i, skgof.ecdfgof.GofResult)  for i in tests ])
        True
        """
        res = [isd.gof_test(test=test) for isd in self]
        return res

    def gof_values(self, test, value_name):
        """
        >>> pvals = example_multi_samplesistr().gof_values("ad", "pvalue")
        >>> stats = example_multi_samplesistr().gof_values("ad", "statistic")
        """
        assert value_name in ('pvalue', 'statistic'), f'pb entree {value_name}'

        res = self.gof_test(test=test)

        if value_name == 'pvalue':
            res = [i.pvalue for i in res]
        else:
            res = [i.statistic for i in res]

        res = Sample(res)
        return res


    def gof_plot(self, test='ad', value_name='pvalue', rank=False):
        """
        >>> l=example_multi_samplesistr().gof_plot()
        """
        sample = self.gof_values(test=test, value_name=value_name)

        res = sample.cdf_plot(rank=rank)
        pyplot.title(test)
        pyplot.xlabel('P-Value')

        return res


    def quantile_quantile(self, weighted=False):
        """
        >>> example_multi_samplesistr(n_sample = 4, sample_size = 5).quantile_quantile()
             sample     model
        0 -1.072969 -0.967422
        1 -0.611756 -0.430727
        2 -0.528172  0.000000
        3  0.865408  0.430727
        4  1.624345  0.967422
        0 -2.136196 -0.967422
        1 -1.793436 -0.430727
        2 -0.416758  0.000000
        3 -0.056267  0.430727
        4  1.640271  0.967422
        0 -1.863493 -0.967422
        1 -0.277388 -0.430727
        2  0.096497  0.000000
        3  0.436510  0.430727
        4  1.788628  0.967422
        0 -0.995909 -0.967422
        1 -0.418302 -0.430727
        2  0.050562  0.000000
        3  0.499951  0.430727
        4  0.693599  0.967422
        >>> example_multi_samplesistr(n_sample = 4, sample_size = 5).quantile_quantile(weighted = True)
             sample     model
        0 -0.469913 -0.423688
        1 -0.334903 -0.235800
        2 -0.385527  0.000000
        3  0.947527  0.471599
        4  3.556961  2.118441
        0 -0.935560 -0.423688
        1 -0.981808 -0.235800
        2 -0.304203  0.000000
        3 -0.061606  0.471599
        4  3.591834  2.118441
        0 -0.816128 -0.423688
        1 -0.151855 -0.235800
        2  0.070436  0.000000
        3  0.477930  0.471599
        4  3.916705  2.118441
        0 -0.436164 -0.423688
        1 -0.228997 -0.235800
        2  0.036906  0.000000
        3  0.547392  0.471599
        4  1.518829  2.118441
        """
        dfs = [i.quantile_quantile(weighted=weighted) for i in self]
        res = pandas.concat(dfs, axis=0)
        return res

    def qq_score(self, weighted=False):
        """
        renvois un objet score
        >>> s=example_multi_samplesistr().qq_score()
        >>> s.__class__ == Score
        True
        """
        dic = self.quantile_quantile(weighted=weighted)
        obs = dic[OBS_NM]
        model = dic[MOD_NM]
        res = Score(x_obs=obs, x_model=model)
        return res

    def nllh(self):
        """
        Renvoie la log vraisemblannce des valeurs donnee en entree
        >>> example_multi_samplesistr().nllh() > 600
        True
        >>> example_multi_samplesistr().nllh() < 800
        True
        """
        res = [i.nllh() for i in self]
        res = numpy.array(res).sum()

        return res

    def quantile_verification_score(self, quantile, normalize=False, return_sample=False):
        """
        Function doc

        PARAM   :    DESCRIPTION
        RETURN  :    DESCRIPTION

        >>> s = example_multi_samplesistr().quantile_verification_score(0.5)
        """
        if return_sample:#renvoi tous les score dans une sample
            values = [i.quantile_verification_score(quantile=quantile, normalize=normalize) for i in self]
            res = Sample(values)

        else: #renvoi un score global
            res = [i.quantile_verification_score(quantile=quantile, normalize=False) for i in self]
            res = numpy.array(res).sum()
            if normalize:
                sizes = [isvd.sample["size"] for isvd in self]
                sizes = sum(sizes)
                res = res / float(sizes)
        return res








##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='Samle module')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        
        test_result = doctest.testmod()
        print(test_result)

    #+++++++++++++++++++++++++++++++#
    #    EXAMPLES                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.examples:
        from japet_misc  import close_all, show_and_close

        close_all()

        loc = 10.
        scale = 50.
        t_distr = Distribution('norm', loc=0, scale=10)

        t_sam_dis_norm = SampleDistr(Sample(t_distr.random(100)), t_distr)
        #~ t_sam_dis_norm.summary_plot()

        msvd = []
        for iii in range(40):
            scale = 10.
            t_distr = Distribution('gev', 0.1, loc=0, scale=10)
            t_sam_dis_gev = SampleDistr(Sample(t_distr.random(100)), t_distr)
            msvd.append(t_sam_dis_gev)

        msvd = Multi_SampleDistr(msvd)
        #~ msvd.qq_plot()

        #~ f = Figure_With_ColorBar(norm = numpy.arange(1,20), under = "w")
        #~ msvd.qq_map_plot(cbar = f.cbar)
        #~ t_sam_dis_gev.summary_plot()


        show_and_close()
