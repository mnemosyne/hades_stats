"""
_DESCRIPTION
"""



##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest
import re
import inspect


from numbers import Number
from collections.abc import Iterable


from dataclasses import dataclass



import pandas
import numpy


import scipy.optimize

from hades_stats.utils import neg_log_likelihood
from hades_stats.sample import Sample
from hades_stats.distribution import sp_name, sp_dist, Distribution


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

PAR_KEY = "p"

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def norm_1delta(x):
    """
    >>> norm_1delta([0,2,6,9,8,10])
    array([0. , 0.2, 0.6, 0.9, 0.8, 1. ])
    >>> norm_1delta([-2,2,6,8])
    array([-0.2,  0.2,  0.6,  0.8])
    >>> norm_1delta([2,3,6,9,8,10])
    array([0.25 , 0.375, 0.75 , 1.125, 1.   , 1.25 ])
    """
    x = numpy.array(x, dtype=float)
    res = x/ (x.max()- x.min())
    assert abs(res.max()-res.min() - 1) < 1e-8
    return res




def norm_0to1(x):
    """
    >>> norm_0to1([0,2,6,9,8,10])
    array([0. , 0.2, 0.6, 0.9, 0.8, 1. ])
    >>> norm_0to1([-10,0,2,6,9,8,10])
    array([0.  , 0.5 , 0.6 , 0.8 , 0.95, 0.9 , 1.  ])
    """
    res0 = norm_1delta(x)
    dx = res0.min()
    res = res0 - dx
    assert abs(res.min() - 0) + abs(res.max() - 1) < 1e-8
    
    return res

def norm_1delta_0centred(x):
    """
    >>> norm_1delta_0centred([0,2,6,9,8,10])
    array([-0.5, -0.3,  0.1,  0.4,  0.3,  0.5])
    >>> norm_1delta_0centred([-10,0,2,6,9,8,10])
    array([-0.5 ,  0.  ,  0.1 ,  0.3 ,  0.45,  0.4 ,  0.5 ])
    """
    res0 = norm_1delta(x)
    dx = res0.min() + 0.5
    res = res0 - dx
    assert abs(res.min() + 0.5) + abs(res.max() - 0.5) < 1e-8
    
    return res
    
    
def is_ns_function_parname(par):
    """
    >>> ts = pandas.Series(("p1", "ep2", "p2er", "prfrfr3", "p10000", "ap1", "p_1","p9"))
    >>> ts2 = ts.map(is_ns_function_parname)
    >>> pandas.concat([ts, ts2], axis=1)
             0      1
    0       p1   True
    1      ep2  False
    2     p2er  False
    3  prfrfr3  False
    4   p10000   True
    5      ap1  False
    6      p_1  False
    7       p9   True
    """
    match = re.match(f'^{PAR_KEY}[0-9]*$', par) #^: debut de segment p minuscule chiffre pouvant etre repete * et fin de segment $
    res = match is not None
    return res
    
##======================================================================================================================##
##                NS DISTRIBUTION FUNCTIONS                                                                             ##
##======================================================================================================================##

def ns_gev_m1(x, p1, p2, p3, p4):
    """
    NS GEV distribution : loc (mu) linearly depends on x
    """
    mus = p1 + p2 * x
    sca = p3
    res = (p4,mus,sca)
    return res
    
def ns_gev_m2(x, p1, p2, p3, p4):
    """
    NS GEV distribution : scale (sigma) linearly depends on x
    """
    mu = p1 
    scas = p2 + p3 * x
    res = (p4,mu,scas)
    return res 
    
def ns_gev_m12(x, p1, p2, p3, p4):
    """
    NS GEV distribution : loc (mu) linearly depends on x, scale (sigma) linearly depends on mu parameter
    mu/sigma = cst
    """
    mus = p1 + p2 * x
    scas = p3 * mus
    res = (p4,mus,scas)
    return res
    
def ns_gev_m3(x, p1, p2, p3, p4, p5):
    """
    NS GEV distribution : loc (mu) linearly depends on x, scale (sigma) linearly depends also on x
    mu/sigma is not constat
    """
    mus = p1 + p2 * x
    scas = p3 + p4 * x
    res = (p5, mus, scas)
    return res

def ns_gev_m1bis(x, y, p1,p2,p3,p4,p5):
    """
    NS GEV distribution : loc (mu) linearly depends on x and y
    """
    mus = p1 + p2 * x + p3 * y
    sca = p4
    res = (p5, mus, sca)
    return res
    
    
    
##======================================================================================================================##
##                CLASSE ObsWithCovar                                                                                   ##
##======================================================================================================================##

@dataclass
class ObsWithCovar:
    """
    Create Observation with associated covariable and norm them
    
    >>> n=100
    >>> x = numpy.arange(n)
    >>> o = pandas.Series(x)

    >>> c = o *5
    >>> c.name="covar"


    >>> owc=ObsWithCovar(o, c)

    >>> type(owc.covar_df)

    >>> owc.covar_df_norm
    
    """
    sample: pandas.Series
    covar_df:pandas.DataFrame

    def __post_init__(self, norm_method=norm_1delta_0centred):
        """
        Check size and shape, and apply norm_method to all covar
        """
        assert isinstance(self.sample, (pandas.Series, Sample))
        
        if isinstance(self.covar_df,pandas.Series):
            assert self.covar_df.name, "Need a covar name"
            self.covar_df = pandas.DataFrame(self.covar_df)
        else:
            assert isinstance(self.covar_df,pandas.DataFrame)

        assert self.sample.size == self.covar_df.index.size
        assert all(self.sample.index == self.covar_df.index)
        
        print("in post init", self.__class__)
        
        self.norm_method = norm_method
        self.covar_df_norm = self.covar_df.apply(norm_method, axis=0)
        

    def normalize_covariables(self, norm_method, colname):
        """
        Function doc
        >>> 'afarie'
        """
        print(f"{norm_method=}")
        raise ValueError("To implement")

    def bootstrap(self, ):
        """
        Function doc
        >>> 'afarie'
        """
        # ~ print(f"{=}")
        raise ValueError("To implement")




class NsDistribution:
    """ 
    Non-Stationary Distribution
    """
    
    def __init__(self, name, ns_function):
        """ Class initialiser """
        self.name = sp_name(name)
        self.sp_dist = sp_dist(name=self.name)
        
        assert callable(ns_function)
        self.ns_function = ns_function

        sign = inspect.signature(self.ns_function)
        func_params = sign.parameters

        self.par_names = []
        self.cov_names = []

        for ipar in func_params.keys():
            if is_ns_function_parname(ipar):
                self.par_names.append(ipar)
            else:
                self.cov_names.append(ipar)

        assert len(self.par_names) > 1, f"ns_function must have parameter. Parameter name must start with {PAR_KEY} and followed by a number. Ex: {PAR_KEY}1"
        assert self.cov_names + self.par_names

        if sorted(self.par_names) != self.par_names:
            print(f"Warning: par_names are not sorted {self.par_names}")

        self.n_params = len(self.par_names)
        self.n_covar = len(self.cov_names)
        
        self.par_vals = None
        
        
    def fix_ns_params(self, **params):
        """
        Set ne function parameter.
        """
        print(params)
        assert all(k in params for k in self.par_names), f"Not all parameter are present\nNeed {[p for p in self.par_names if p not in params]}"
        res = [params[k] for k in self.par_names]
        res = tuple(res)
            
        # ~ assert len(res) == self.n_params, f"Expect all the ns function parameters\nGot{new_params=}\nNeed"
        self.par_vals = res
        
        return res
        
    def predict(self, covars):
        """
        Predict distribution
        """
        assert self.is_frozen()
            # ~ assert not theta, "Distribution is alareday frozen."
        # ~ else:
            # ~ self.fix_ns_params(**theta)
        if self.n_covar == 1:
            cov_name = self.cov_names[0]
            if isinstance(covars, Number):
                covars = [covars]
            elif isinstance(covars, pandas.DataFrame):
                covars = covars.loc[:, cov_name]
            else:
                assert isinstance(covars, Iterable)
            
            # ~ print(covars)
            params = [self.ns_function(i, *self.par_vals) for i in covars]
        
        else:
            assert isinstance(covars, pandas.DataFrame)
            assert all(icov in covars.columns for icov in self.cov_names)
            par2parse = [[covars.loc[idx, icov] for icov in self.cov_names]+list(self.par_vals) for idx in covars.index]
            # ~ print(par2parse)
            params = [self.ns_function(*iparse) for iparse in par2parse]

        # ~ print(params)
        res = [Distribution(self.name, *par) for par in params]
        if len(res) == 1:
            res = res[0]
        return res
        
        

    
        
    def is_frozen(self):
        """
        Function doc
        >>> 'afarie'
        """
        if self.par_vals:
            res = len(self.par_vals) == self.n_params
        else:
            res = False
        
        return res
        
        
    def to_params_ts(self):
        """
        Function doc
        >>> 'afarie'
        """
        dic = dict(zip(self.par_names, self.par_vals))
        res = pandas.Series(dic)
        return res





class FitNsDistribution:
    """ Class doc
    >>> 'afarie' """
    
    def __init__(self, obs_with_covar, ns_distribution, use_normalized_df=True, fit_kwargs=None):
        """ Class initialiser """
        assert isinstance(obs_with_covar, ObsWithCovar)
        assert isinstance(ns_distribution, NsDistribution)
        
        self.obs_with_covar = obs_with_covar
        self.ns_distribution = ns_distribution
        
        
        for icov in self.ns_distribution.cov_names:
            assert icov in self.obs_with_covar.covar_df, f"{icov} not in {self.obs_with_covar.covar_df.columns}"
            assert icov in self.obs_with_covar.covar_df_norm, f"{icov} not in {self.obs_with_covar.covar_df_norm.columns}"
        
        self.use_normalized_df = use_normalized_df
        
        if self.use_normalized_df:
            self._covar_df = self.obs_with_covar.covar_df_norm
        else:
            self._covar_df = self.obs_with_covar.covar_df
        
        self.fit_kwargs = {}
        
        if fit_kwargs is None:
            pass
        elif isinstance(fit_kwargs, dict):
            self.append_fit_kwargs(fit_kwargs)
        elif isinstance(fit_kwargs, Iterable):
            for ifitargs in fit_kwargs:
                self.append_fit_kwargs(ifitargs)
        else:
            raise ValueError(f"Dont know what to do with {fit_kwargs}")
            
            
        self.fit_results = {}
        
        self.best_fit = None
        self.fit_nllhs = None
        
        
    def _merge_covar_theta(self, theta):
        """
        Function doc
        >>> 'afarie'
        """
        
        res = [self._covar_df.loc[:, icov].values for icov in self.ns_distribution.cov_names]
        res = res + list(theta)
        return res
        
    def _nllh(self, theta):
        """
        Function doc
        >>> 'afarie'
        """
        dist = self.ns_distribution.sp_dist
        merged_input = self._merge_covar_theta(theta)
        dist_pars = self.ns_distribution.ns_function(*merged_input)

        probs = dist.pdf(self.obs_with_covar.sample, *dist_pars)
        res = neg_log_likelihood(probs)
        return res
        
    def append_fit_kwargs(self, fit_kwargs):
        """
        Function doc
        >>> 'afarie'
        """
        assert isinstance(fit_kwargs, dict)
        assert "x0" in fit_kwargs, "Need init parameters"
        assert len(fit_kwargs['x0']) == self.ns_distribution.n_params
        
        if not self.fit_kwargs:
            k = 0
        else:
            k0 = max(self.fit_kwargs.keys())
            k = k0 + 1
        
        assert k not in fit_kwargs, "pb code"
        
        self.fit_kwargs[k] = fit_kwargs
        
        
    def fit1(self, **kwargs):
        """
        Function doc
        >>> 'afarie'
        """
        res = scipy.optimize.minimize(self._nllh, **kwargs)
        return res
        
        
    def fit(self):
        """
        Function doc
        >>> 'afarie'
        """
        assert self.fit_kwargs, f"Need fit args in {self.fit_kwargs}\nUse {self.append_fit_kwargs}"
        for kfit, i_fit_kwargs in self.fit_kwargs.items():
            print(f"Fit {kfit} = {i_fit_kwargs}", end=":", flush=True)
            fit_result = self.fit1(**i_fit_kwargs)
            self.fit_results[kfit] = fit_result
            print(f"{fit_result.success=}")
        
        self.fit_nllhs = pandas.Series({kfit: ifit.fun for kfit, ifit in self.fit_results.items()}, dtype=float)

        best_idx = self.fit_nllhs.idxmin()
        print(f"{best_idx=}")
        self.best_fit = self.fit_results[best_idx]

        self.ns_distribution.fix_ns_params(**dict(zip(self.ns_distribution.par_names, self.best_fit.x)))
        
        assert abs(self.nllh() - self.best_fit.fun) < 1e-8
        
        return None
        
    def predict(self):
        """
        Function doc
        >>> 'afarie'
        """
        res = self.ns_distribution.predict(self._covar_df)
        return res
        
    def nllh(self):
        """
        Function doc
        >>> 'afarie'
        """
        dists = self.predict()
        probs = [idist.pdf(o) for idist, o in zip(dists, self.obs_with_covar.sample.values)]
        res = neg_log_likelihood(probs)
        return res


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTEST                    #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
        

    #+++++++++++++++++++++++++++++++#
    #    TESTS                      #
    #+++++++++++++++++++++++++++++++#
    


    n = 100
    v = numpy.arange(n) / n
    locationss = 10 + 0.5*v
    scale =0.1 * locationss
    c = -0.1

    #x1_true = 
    x_nsgev = sp_dist('genextreme').rvs(c,scale=scale, loc=locationss)


    obs = ObsWithCovar(pandas.Series(x_nsgev), pandas.Series(v, name="x"))



    ns_d = NsDistribution('gev', ns_gev_m1)
    ns_d1 = NsDistribution('gev', ns_gev_m2)
    ns_d12 = NsDistribution('gev', ns_gev_m12)


    ns = NsDistribution("gev", ns_gev_m1)
    print(ns.is_frozen())
    ns.fix_ns_params(p1=10, p2=1, p3=2, p4=-0.1)
    print(ns.is_frozen())
    print(ns.predict(3))

    for d in ns.predict(numpy.arange(10)):
        print(d)
        

    ns_d2 = NsDistribution('gev', ns_gev_m1bis)
    obs = ObsWithCovar(pandas.Series(x_nsgev), pandas.DataFrame({"x": v, "y":v-0.5}))

    ns_d2.fix_ns_params(p1=10, p2=1, p3=2, p4=2, p5=-0.1)

    for d in ns_d2.predict(obs.covar_df):
        print(d.kwds['loc'])


    optim_methods = ['Nelder-Mead' , 'Powell', 'CG', 'BFGS','COBYLA','TNC','SLSQP','L-BFGS-B' ]
    
            
    # ~ - 'L-BFGS-B'    :ref:`(see here) <optimize.minimize-lbfgsb>`
    # ~ - 'TNC'         :ref:`(see here) <optimize.minimize-tnc>`
    # ~ - 'COBYLA'      :ref:`(see here) <optimize.minimize-cobyla>`
    # ~ - 'SLSQP'       :ref:`(see here) <optimize.minimize-slsqp>`
    # ~ - 'trust-constr':ref:`(see here) <optimize.minimize-trustconstr>`
    # ~ - 'dogleg'      :ref:`(see here) <optimize.minimize-dogleg>`
    # ~ - 'trust-ncg'   :ref:`(see here) <optimize.minimize-trustncg>`
    # ~ - 'trust-exact' :ref:`(see here) <optimize.minimize-trustexact>`
    # ~ - 'trust-krylov' :ref:`(see here) <optimize.minimize-trustkrylov>`
    
    optim_x0s = ([4,0.1,1,0.1], [4,0.1,1,-0.1])

    optim_kwargs = [{"method": imeth, "x0":ix0} for imeth in optim_methods for ix0 in optim_x0s]

    fit = FitNsDistribution(obs, ns_distribution=ns_d, fit_kwargs=optim_kwargs)
    fit1 = FitNsDistribution(obs, ns_distribution=ns_d1, fit_kwargs=optim_kwargs)
    fit12 = FitNsDistribution(obs, ns_distribution=ns_d12, fit_kwargs=optim_kwargs)
    
    
    fit12.fit()
    
    
    fit12.predict()

    print(fit12.ns_distribution.to_params_ts())
    
