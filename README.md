# hades_stats library

## Description

Some python statistics functions and classes

## Installation

### from gitlab

```
pip install git+https://gricad-gitlab.univ-grenoble-alpes.fr/mnemosyne/hades_stats.git
```

Ou

```
pip install git+ssh://git@gricad-gitlab.univ-grenoble-alpes.fr/mnemosyne/hades_stats.git
```

### test install

Get install directory
```
pip show hades_stats
```

Use pytest on hades_stats path
```
pytest --doctest-modules /path_wherre_pip_install/site-packages/hades_stats
```

## Usage


```

```

## License
GNU GPL

